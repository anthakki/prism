#ifndef SRC_TSV_PARSER_H_
#define SRC_TSV_PARSER_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Opaque type for a TSV parser state */
typedef struct tsv_parser_ tsv_parser_t;

/* Default flavors */
extern const char TSV_PARSER_EXCEL_[];
#define TSV_PARSER_EXCEL (TSV_PARSER_EXCEL_) /* Excel TSV */

/* Create a new parser */
void tsv_parser_init(tsv_parser_t *self, const char *flavor);

/* Segment types */
#define TSV_PARSER_SKIP (0) /* Ignore (quotes etc.) */
#define TSV_PARSER_DATA (1) /* Field data (might be partial) */
#define TSV_PARSER_FF (2) /* Field forward */
#define TSV_PARSER_LF (3) /* Line forward */

/* Parse a segment data + [0, *tail), giving its type, for *tail up to size */
int tsv_parser_parse(tsv_parser_t *self, size_t *tail, const void *data, size_t size);

/* Check for unterminated quotes */
int tsv_parser_finish(const tsv_parser_t *self);

struct tsv_parser_ {
	unsigned state_;
	char flavor_[4];
};

#ifdef __cplusplus
}
#endif

#endif /* SRC_TSV_PARSER_H_ */
