#ifndef SRC_TSV_READER_H_
#define SRC_TSV_READER_H_

#include "tsv_parser.h"
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Opaque type for a TSV reader */
typedef struct tsv_reader_ tsv_reader_t;

/* Flags for shaping the data */
#define TSV_READER_TRANSP   (0x1)  /* Transpose data (Fortran order) */
#define TSV_READER_COLNAMES (0x2)  /* Data has column names */
#define TSV_READER_ROWNAMES (0x4)  /* Data has row names */
#define TSV_READER_SPARSE   (0x8)  /* Read to sparse internal form */

/* Create a new reader */
void tsv_reader_init(tsv_reader_t *self, const char *flavor, int flags);

/* Extract current message */
const char *tsv_reader_error(const tsv_reader_t *self);

/* Push a segment of data for parsing */
int tsv_reader_parse(tsv_reader_t *self, const void *data, size_t size);
/* Finish parsing */
int tsv_reader_finish(tsv_reader_t *self);

/* Open a file and parse it */
const char *tsv_read(tsv_reader_t *self, const char *filename);

/* NOTE: Dims always as on disk or C order, even for TSV_READER_TRANSP */

/* Get number of data rows/columns */
size_t tsv_reader_cols(const tsv_reader_t *self);
size_t tsv_reader_rows(const tsv_reader_t *self);

/* Get parsed values */
double *tsv_reader_data(const tsv_reader_t *self);
/* Move values out of the reader */
double *tsv_reader_data_release(tsv_reader_t *self);

/* Get sparse density */
double tsv_reader_sparse_density(const tsv_reader_t *self);
/* Get sparse representation */
size_t tsv_reader_sparse_data(double **x, const size_t **p, const size_t **j, const tsv_reader_t *self);

/* Get column names */
const void *tsv_reader_colnames(const tsv_reader_t *self);
/* Get row names */
const void *tsv_reader_rownames(const tsv_reader_t *self);

/* Extract next name */
char *tsv_names_next(size_t *size, const void **it);

/* Free data */
void tsv_reader_deinit(tsv_reader_t *self);

struct tsv_reader_ {
	int flags_;
	tsv_parser_t parser_;
	size_t field_, cols_;
	const char *error_;
	struct tsv_reader_scratch_ {
		char *data_;
		size_t head_, tail_, size_;
	} scratch_;
	size_t rownames_head_;
	struct tsv_reader_values_ {
		double *data_;
		size_t head_, tail_;
		size_t stripes_;
	} values_;
	struct tsv_reader_sparse_values_ {
		double *data_;
		size_t *colinds_;
		size_t *rowptrs_;
		size_t head_, tail_;
		size_t col_tail_;
		size_t row_head_, row_tail_;
	} sparse_values_;
};

#ifdef __cplusplus
}
#endif

#endif /* SRC_TSV_READER_H_ */
