
#include "xalloc.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

static
void
#if defined(__GNUC__) || defined(__clang__)
__attribute__((noreturn))
#endif
xdie(const char *message)
{
	fprintf(stderr, "%s" "\n", message);
	exit(EXIT_FAILURE);
}

void *
xalloc(size_t count, size_t size)
{
	void *data;

	/* Empty request? */
	if (!(count > 0 && size > 0))
		return NULL;

	/* Count too large? */
	if (count > (size_t)-1 / size)
		xdie("no memory");

	/* Allocate */
	data = malloc(count * size);
	if (data == NULL)
		xdie("no memory");

	return data;
}

struct xatexit_node {
	struct xatexit_node *prev, *next;
	void (*callback)(void *);
	void *cookie;
};

static struct xatexit_node xatexit_root = { &xatexit_root, &xatexit_root, NULL, NULL };
static int xatexit_engaged = 0;

static
void
xatexit_pop(struct xatexit_node *node)
{
	int free_node;

	assert(node != NULL);

	/* Unlink */
	node->prev->next = node->next;
	node->next->prev = node->prev;

	/* Figure out if we need to free the node */
	free_node = node != node->cookie;

	/* Invoke the callback */
	(*node->callback)(node->cookie);
	/* Free the node, unless we just did */
	if (free_node)
		free(node);
}

static
void
xatexit_callback(void)
{
	/* Free up stuff */
	while (xatexit_root.prev != &xatexit_root)
		xatexit_pop(xatexit_root.prev);
}

static
void
xatexit_push(struct xatexit_node *node)
{
	assert(node != NULL);

	/* Boot? */
	if (xatexit_engaged == 0)
	{
		xatexit_engaged = 1;
		atexit(&xatexit_callback);
	}

	/* Get neighbors */
	node->prev = xatexit_root.prev;
	node->next = &xatexit_root;

	/* Link up */
	node->prev->next = node;
	node->next->prev = node;
}

void
xatexit(void (*callback)(void *), void *cookie)
{
	struct xatexit_node *node;

	/* Allocate a node */
	node = (struct xatexit_node *)malloc(sizeof(*node));
	if (node == NULL)
	{
		/* Clean up the item we were trying to schedule */
		(*callback)(cookie);

		/* We're ready to die */
		xdie("no memory");
	}

	/* Set up the node */
	node->callback = callback;
	node->cookie = cookie;

	/* Push the node */
	xatexit_push(node);
}

void *
xalloca(size_t count, size_t size)
{
	struct xatexit_node *node;
	void *data;

	/* Empty? */
	if (!(count > 0 && size > 0))
		return NULL;

	/* TODO: handle weird alignment */

	/* Allocate */
	node = (struct xatexit_node *)malloc(sizeof(*node) + count * size);
	if (node == NULL)
		xdie("no memory");

	/* Get data pointer */
	data = &node[1];

	/* Set up the node */
	node->callback = &free;
	node->cookie = node;

	/* Push the node */
	xatexit_push(node);

	return data;
}

static
struct xatexit_node *
xalloca_node(void *data)
{
	assert(data != NULL);
	
	/* Get node */
	return &((struct xatexit_node *)data)[-1];
}

void
xfree(void *data)
{
	/* Get node & free it */
	if (data != NULL)
		xatexit_pop(xalloca_node(data));
}
