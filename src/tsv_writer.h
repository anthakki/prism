#ifndef SRC_TSV_WRITE_H_
#define SRC_TSV_WRITE_H_

#include "tsv_reader.h"
#include <stddef.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Write a field of character data */
int tsv_write_field(FILE *stream, size_t *col, const void *data, size_t size);
/* Wriet a field in pieces */
int tsv_write_fieldv(FILE *stream, size_t *col, const void *data, size_t size, ...);
/* Write a numeric value */
int tsv_write_number(FILE *stream, size_t *col, double value);

/* Write a newline & reset column counter */
int tsv_write_newline(FILE *stream, size_t *col);

/* Dump results in a TSV file */
const char *tsv_write(const char *filename, const double *X, size_t m, size_t n, int flags,
	const void *colnames, const void *rownames);

/* Make a custom name for TSV dumping */
const void *tsv_names_put(const void *it, const char *name);
/* Allocate sequential names */
const void *tsv_names_putseq(const char *prefix, size_t n);

#ifdef __cplusplus
}
#endif

#endif /* SRC_TSV_WRITE_H_ */
