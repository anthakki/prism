#ifndef RGETOPT_H_
#define RGETOPT_H_

#include <limits.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Context for option parsing */
typedef struct rgetopt_ rgetopt_t;

/* Use self->optind, self->optopt, and self->optarg 
 * for current option index, flag, & argument
 */

/* Set up a new parser */
void rgetopt_init(rgetopt_t *self, int argc, char **argv, const char *optstr);
/* Sort non-option arguments into the end */ 
void rgetopt_sort(const rgetopt_t *self);

/* Get next option */
int rgetopt_next(rgetopt_t *self);

struct rgetopt_ {
	char flags_[(size_t)CHAR_MAX + 1u];
	char **argv;
	int optind, optopt;
	size_t used_;
	char *optarg;
};

#ifdef __cplusplus
}
#endif

#endif /* RGETOPT_H_ */
