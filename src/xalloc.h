#ifndef SRC_XALLOC_H_
#define SRC_XALLOC_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Allocate memory or die */
void *xalloc(size_t count, size_t size);

/* Schedule a free at program exit */
void xatexit(void (*callback)(void *), void *cookie);

/* This combines the above */
void *xalloca(size_t count, size_t size);
/* Free a xautoalloc() block before exit */
void xfree(void *data);

#ifdef __cplusplus
}
#endif

#endif /* SRC_XALLOC_H_ */
