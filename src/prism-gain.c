
#include "libprism/gain.h"
#include "rgetopt.h"
#include "tsv_reader.h"
#include "tsv_writer.h"
#include "xalloc.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef NAN
#	define NAN (__builtin_nan(""))
#endif

static
void
poi_estimator(double *u, double *v, const double *X, size_t m, size_t n, double trim)
{
	if (trim == 1.)
		prism_gain_poi(u, v, X, m, n);
	else
		prism_gain_trimmed_poi(u, v, X, m, n, trim);
}

static
void
spoi_estimator(double *u, double *v, const double *X, size_t m, size_t n, double trim)
{
	if (trim == 1.)
		prism_gain_spoi(u, v, X, m, n);
	else
		prism_gain_trimmed_spoi(u, v, X, m, n, trim);
}

static
void
l2_estimator(double *u, double *v, const double *X, size_t m, size_t n, double trim)
{
	if (trim == 1.)
		prism_gain_l2(u, v, X, m, n);
	else
		prism_gain_trimmed_l2(u, v, X, m, n, trim);
}

static
double
z_or_div(double x, double y)
{
	/* Compute x/y with 0./0. -> 0. for x >= 0. */
	if (x > 0.)
		return x / y;
	else
		return 0.;
}

static
void
normalize_factors(double *u, double *v, size_t m, size_t n, double unity)
{
	size_t j, i;
	double v_sum, s;

	assert(u != NULL || m < 1);
	assert(v != NULL || n < 1);

	/* Empty? */
	if (!(n > 1))
		return;

	/* Compute current scale */
	v_sum = 0.;
	for (j = 0; j < n; ++j)
		v_sum += v[j];

	/* If all v:s are zero, so are all u:s */
	if (!(v_sum > 0.))
		return;

	/* Scale u */
	s = v_sum / (n * unity);
	for (i = 0; i < m; ++i)
		u[i] *= s;
	/* Scale v */
	for (j = 0; j < n; ++j)
		v[j] = z_or_div(v[j], s);
}

static
void
tag_zeros(double *x, size_t n, double tag)
{
	size_t i;

	assert(x != NULL || n < 1);

	/* Fill zeros */
	for (i = 0; i < n; ++i)
		if (x[i] == 0.)
			x[i] = tag;
}

struct opts {
	/* Method options */
	void (*estimator)(double *u, double *v, const double *X, size_t m, size_t n, double trim);
	double trim;
	/* Normalization */
	double unity;
	/* File format options */
	unsigned headers:1;
	unsigned zero_is_miss:1;
	/* Output files */
	const char *dest_expr_fn, *dest_gains_fn;
	/* Input files */
	const char *src_expr_fn;
};

static
void
default_args(struct opts *opts) 
{
	assert(opts != NULL);

	/* Method defaults */
	opts->estimator = &spoi_estimator;
	opts->trim = 1.;
	/* Normalization */
	opts->unity = 0.;
	/* File format options */
	opts->headers = 0;
	opts->zero_is_miss = 0;
	/* Output files */
	opts->dest_expr_fn = NULL;
	opts->dest_gains_fn = NULL;
	/* Input files */
	opts->src_expr_fn = NULL;
}

static
int
parse_args(struct opts *opts, int argc, char **argv)
{
	rgetopt_t getopt;
	int opt;

	assert(opts != NULL);
	assert(argc > 0);
	assert(argv != NULL);

	/* Set up the parser */
	rgetopt_init(&getopt, argc, argv, ": m:a: n: HM Z:G:");
	rgetopt_sort(&getopt);

	/* Set defaults */
	default_args(opts);

	/* Parse */
	while ((opt = rgetopt_next(&getopt)) != -1)
		switch (opt)
		{
			/* Method options */
			case 'm':
				if      (strcmp(getopt.optarg, "poi") == 0)
					opts->estimator = &poi_estimator;
				else if (strcmp(getopt.optarg, "spoi") == 0)
					opts->estimator = &spoi_estimator;
				else if (strcmp(getopt.optarg, "l2") == 0)
					opts->estimator = &l2_estimator;
				else if (strcmp(getopt.optarg, "sum") == 0)
				{
					opts->estimator = &poi_estimator;
					opts->trim = 1.;
				}
				else if (strcmp(getopt.optarg, "pca") == 0)
				{
					opts->estimator = &l2_estimator;
					opts->trim = 1.;
				}
				else
					goto bad_arg;
				break;
			case 'a':
			{{
				char stop;

				if (sscanf(getopt.optarg, "%lf%c", &opts->trim, &stop) != 1)
					goto bad_arg;
				if (!(0. <= opts->trim && opts->trim <= 1.))
					goto bad_arg;

				break;
			}}

			/* Normalization */
			case 'n':
			{{
				char stop;

				if (sscanf(getopt.optarg, "%lf%c", &opts->unity, &stop) != 1)
					goto bad_arg;
				if (!(opts->unity > 0.))
					goto bad_arg;
				break;
			}}

			/* File format options */
			case 'H':
				opts->headers = 1;
				break;
			case 'M':
				opts->zero_is_miss = 1;
				break;

			/* Output file names */
			case 'Z':
				opts->dest_expr_fn = getopt.optarg;
				break;
			case 'G':
				opts->dest_gains_fn = getopt.optarg;
				break;

			/* Invalid option */
			default: /* '?' */
				fprintf(stderr, "%s: invalid option -%c" "\n", argv[0], getopt.optopt);
				return -1;

			/* Missing argument */ 
			case ':':
				fprintf(stderr, "%s: missing argument for option -%c" "\n", argv[0], getopt.optopt);
				return -1;

			/* Invalid option argument */
			bad_arg:
				fprintf(stderr, "%s: invalid argument for option -%c: %s" "\n", argv[0], getopt.optopt, getopt.optarg);
				return -1;
		}

	/* Get positional arguments */
	if (argc - getopt.optind >= 1)
		opts->src_expr_fn = (&argv[getopt.optind])[0];
	if (!(argc - getopt.optind <= 1))
	{
		fprintf(stderr, "%s: too many input arguments" "\n", argv[0]);
		return -1;
	}

	/* Check for required filenames */
	if (!(opts->src_expr_fn != NULL))
	{
		fprintf(stderr, "%s: missing input files" "\n", argv[0]);
		return -1;
	}

	return 0;
}

static
void
usage(const char *argv0)
{
	fprintf(stderr, "Usage: %s [options] expr.tsv" "\n"
"Options:" "\n"
"  -m model  -n unity  -H  -G gains.tsv  " "\n"
"  -a trim             -M                " "\n"
		, argv0);
}

int
main(int argc, char **argv)
{
	struct opts opts;
	int file_flags;
	tsv_reader_t X_reader;
	const char *err_msg;
	double *X;
	size_t m, n;
	double *u, *v;

	/* Parse arguments */
	if (parse_args(&opts, argc, argv) != 0)
	{
		usage(argv[0]);
		return EXIT_FAILURE;
	}

	/* Set up input options */
	file_flags = 0;
	if (opts.headers)
		file_flags = TSV_READER_COLNAMES | TSV_READER_ROWNAMES;

	/* Read input file */
	tsv_reader_init(&X_reader, NULL, TSV_READER_TRANSP | file_flags);
	if ((err_msg = tsv_read(&X_reader, opts.src_expr_fn)) != 0)
	{
		tsv_reader_deinit(&X_reader);

		fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.src_expr_fn, err_msg);
		return EXIT_FAILURE;
	}

	/* Get data & dimensions */
	X = tsv_reader_data(&X_reader);
	m = tsv_reader_rows(&X_reader);
	n = tsv_reader_cols(&X_reader);

	/* Mark zeros as missing */
	if (opts.zero_is_miss)
		tag_zeros(X, m*n, NAN);

	/* Allocate output arrays */
	u = (double *)xalloca(m + n, sizeof(*u));
	v = &u[m];

	/* Run the estimator */
	(*opts.estimator)(u, v, X, m, n, opts.trim);

	/* Normalize */
	if (opts.unity != 0.)
		normalize_factors(u, v, m, n, opts.unity);

	/* Dump profiles */
	if (opts.dest_expr_fn != NULL)
		if ((err_msg = tsv_write(opts.dest_expr_fn, u, m, 1, file_flags, tsv_names_put(NULL, "expr"), tsv_reader_rownames(&X_reader))) != NULL)
			fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.dest_expr_fn, err_msg);
	/* Dump gains */
	if (opts.dest_gains_fn != NULL)
		if ((err_msg = tsv_write(opts.dest_gains_fn, v, 1, n, file_flags, tsv_reader_colnames(&X_reader), tsv_names_put(NULL, "gain"))) != NULL)
			fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.dest_gains_fn, err_msg);

	/* Free */ 
	tsv_reader_deinit(&X_reader);

	return EXIT_SUCCESS;
}
