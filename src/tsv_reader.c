
#include "tsv_reader.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>

static
void *
malloc_grow(void **data, size_t width, size_t *size, size_t want)
{
	size_t new_size;
	void *new_data;

	/* Already satisfied? */
	if (!(*size < want))
		return *data;

	/* Figure out a new size */
	new_size = *size;
	if (new_size < 4096)
		new_size = 4096;
	else if (new_size < 1048576lu)
		new_size += new_size * 4;  /* For small, grow by 5x */
	else 
		new_size += new_size / 2;  /* For large, grow by 1.5x */
	if (new_size < want)
		new_size = want;

	/* Allocate */
	new_data = realloc(*data, new_size * width);
	if (new_data == NULL)
		return NULL;

	/* Update */
	*data = new_data;
	*size = new_size;

	return new_data;
}

static
void *
malloc_shrink(void **data, size_t width, size_t *size, size_t want)
{
	void *new_data;

	/* Already satisfied */
	if (*size == want)
		return *data;

	/* Allocate */
	if (want > 0)
	{
		new_data = realloc(*data, want * width);
		if (new_data == NULL)
			return NULL;
	}
	else
	{
		free(*data);
		new_data = NULL;
	}

	/* Update */
	*data = new_data;
	*size = want;

	return new_data;
}

/*
 * The scratch buffer is used for holding payload data, both temporarily and
 * for the stuff that we need to keep around like row & column names:
 *
 *   data
 *   v     
 *   |=========)-----------)xxxxxxxxxxxxxx)
 *        ^   head        tail           size
 *        rownames.head
 *
 *   data + [0, head) ~ column & row name data
 *     rownames.head points to the split of column/row names
 *   data + [head, tail) ~ working (current field) data
 *   data + [tail, size) ~ allocated but free
 */

static
void *
scratch_push_(struct tsv_reader_scratch_ *self, const void *data, size_t size)
{
	assert(self != NULL);

	/* NOTE: we allocate +1 here for a '\0' terminator for strtod() */

	/* Grow the array */
	if (malloc_grow((void **)&self->data_, sizeof(*self->data_), &self->size_, self->tail_ + size + 1) == NULL)
		return NULL;

	/* Append data */
	memcpy(&self->data_[self->tail_], data, size);
	self->tail_ += size;

	return &self->data_[self->tail_ - size];
}

static
size_t
scratch_used_(struct tsv_reader_scratch_ *self)
{
	assert(self != NULL);

	/* Get current field size */
	return self->tail_ - self->head_;
}

/* 
 * The value buffer holds the parsed numerical values. In case we need to fill
 * in the matrix transposed, this is slighly tricky:
 *
 *   0 3 6 . .   head ~ used elements (here 8)
 *   1 4 7 . .   stripes ~ interleaving stride (here 3) 
 *   2 5 . . .   size ~ allocated elements (here 15)
 * 
 * When the array is resized, we must permute the data such that they are in
 * correct position regarding the striping and new size:
 *
 *   0 3 6 . . . . . . .
 *   1 4 7 . . . . . . .
 *   2 5 . . . . . . . .
 */

static
void
restripe_array_d(double *data, size_t w0, size_t w1, size_t n)
{
	size_t i;

	assert(data != NULL || (w0 < 1 && w1 < 1) || n < 1);

	/* Move the stuff around */
	if (w1 > w0)
		for (i = n; i-- > 1;)
		{
			memmove(&data[w1*i], &data[w0*i], w0 * sizeof(*data));
#ifndef NDEBUG
			memset(&data[w1*i+w0], -1, (w1-w0) * sizeof(*data));
#endif

		}
	else if (w1 < w0)
	{
		for (i = 1; i < n; ++i)
			memmove(&data[w1*i], &data[w0*i], w1 * sizeof(*data));
#ifndef NDEBUG
		memset(&data[w1*n], -1, (w0-w1)*n * sizeof(*data));
#endif
	}
}

static
void
values_setstripes_(struct tsv_reader_values_ *self, size_t stripes)
{
	assert(self != NULL);
	assert(self->stripes_ == 1);

	/* NOTE: this function must be called either before any data or after the
	 * first row has been received, and can only be called once */

	/* Apply new striping */
	if (self->head_ > 0)
	{
		assert(stripes == self->head_);
		self->stripes_ = self->head_;

		/* Restripe */
		restripe_array_d(self->data_, 1, self->tail_ / self->stripes_, self->stripes_);
	}
	else
		self->stripes_ = stripes;
}

static
int
values_push_(struct tsv_reader_values_ *self, double value)
{
	assert(self != NULL);

	/* Append */
	if (!(self->head_ + (self->stripes_ - 1) < self->tail_))
		/* NOTE: we have room if h < s*(t/s) which has LB s*(t/s) >= t-(s-1) */
	{
		size_t old_tail;

		/* Grow the array */
		old_tail = self->tail_;
		if (malloc_grow((void **)&self->data_, sizeof(*self->data_), &self->tail_, (self->head_ + self->stripes_)) == NULL)
			return -1;

		/* Restripe data */
		restripe_array_d(self->data_, old_tail / self->stripes_, self->tail_ / self->stripes_, self->stripes_);
	}

	/* Store */
		/* NOTE: the coordinate is (h/s)*1 + (h%s)*(t/s) */
	if (self->stripes_ > 1)
	{
		self->data_[ (( self->head_ / self->stripes_ )) + (( self->head_ % self->stripes_ )) * (( self->tail_ / self->stripes_ )) ] = value;
		++self->head_;
	}
	else 
		self->data_[self->head_++] = value; 
	return 0;
}

static
int
values_compact_(struct tsv_reader_values_ *self)
{
	assert(self != NULL);

	/* Compact data */
	if (self->stripes_ > 0)
		restripe_array_d(self->data_, self->tail_ / self->stripes_, self->head_ / self->stripes_, self->stripes_);

	/* Give up memory */
	if (malloc_shrink((void **)&self->data_, sizeof(*self->data_), &self->tail_, self->head_) == NULL && self->head_ > 0)
		return -1;

	return 0;
}

static
int
sparse_values_commit_(struct tsv_reader_sparse_values_ *self, size_t row)
{
	assert(self != NULL);

	/* Grow the array */
	if (malloc_grow((void **)&self->rowptrs_, sizeof(*self->rowptrs_), &self->row_tail_, (row + 1) + 1) == NULL)
		return -1;

	/* Lock previous rows */
	for (; self->row_head_ < row + 1; ++self->row_head_)
		self->rowptrs_[self->row_head_] = self->head_;

	return 0;
}

static
int
sparse_values_push_(struct tsv_reader_sparse_values_ *self, double value, size_t row, size_t col)
{
	assert(self != NULL);

	/* Grow the arrays */
	if (malloc_grow((void **)&self->data_, sizeof(*self->data_), &self->tail_, self->head_ + 1) == NULL)
		return -1;
	if (malloc_grow((void **)&self->colinds_, sizeof(*self->colinds_), &self->col_tail_, self->head_ + 1) == NULL)
		return -1;

	/* Lock past positions */
	if (sparse_values_commit_(self, row) != 0)
		return -1;

	/* Store value & column coordinate */
	self->data_[self->head_] = value;
	self->colinds_[self->head_] = col;
	++self->head_;

	return 0;
}

static
int
sparse_values_compact_(struct tsv_reader_sparse_values_ *self, size_t rows)
{
	assert(self != NULL);

	/* Give up extra memory */
	(void)malloc_shrink((void **)&self->data_, sizeof(*self->data_), &self->tail_, self->head_);
	(void)malloc_shrink((void **)&self->colinds_, sizeof(*self->colinds_), &self->col_tail_, self->head_);
	(void)malloc_shrink((void **)&self->rowptrs_, sizeof(*self->rowptrs_), &self->row_tail_, rows + 1);

	return 0;
}

static
void
sparse_values_to_dense_(double *data, const struct tsv_reader_sparse_values_ *self, size_t rows, size_t cols, int fortran)
{
	size_t i, k;

	/* Copy data over */
	if (!fortran)
		for (k = i = 0; i < rows; ++i)
			for (; k < self->rowptrs_[i + 1]; ++k)
				data[ self->colinds_[k] + i * cols ] = self->data_[k];
	else
		for (k = i = 0; i < rows; ++i)
			for (; k < self->rowptrs_[i + 1]; ++k)
				data[ i + self->colinds_[k] * rows ] = self->data_[k];
}

static
void
sparse_values_transpose_(double *Z, size_t *I, size_t *Q, const double *X, const size_t *P, const size_t *J, size_t m, size_t n)
{
	size_t i, k, j, s, q, tmp;

	/* Clear accumulators */
	for (j = 0; j < n; ++j)
		Q[j] = 0;

	/* Count */
	for (k = i = 0; i < m; ++i)
		for (; k < P[i + 1]; ++k)
			Q[J[k]]++;

	/* Make cumulative */
	s = 0;
	for (j = 0; j < n; ++j)
		Q[j] = ( s += Q[j] ) - Q[j];

	/* Bucket */
	for (k = i = 0; i < m; ++i)
		for (; k < P[i + 1]; ++k)
		{
			q = Q[J[k]]++;
			Z[q] = X[k];
			I[q] = i;
		}

	/* Rotate index to correct place */
	s = 0;
	for (j = 0; j < n; ++j)
	{
		tmp = Q[j];
		Q[j] = s;
		s = tmp;
	}
	{
		Q[n] = s;
	}
}

void
tsv_reader_init(tsv_reader_t *self, const char *flavor, int flags)
{
	assert(self != NULL);

	/* Store flags */
	self->flags_ = flags;

	/* Reset the parser */
	tsv_parser_init(&self->parser_, flavor);
	/* Reset position in the TSV grid */
	self->field_ = 0;   /* NB. running field number */
	self->cols_ = 0;   /* NB. number of columns, or if unknown */

	/* Clear error */
	self->error_ = NULL;

	/* Set up scratch buffer */
	memset(&self->scratch_, 0, sizeof(self->scratch_));
	self->rownames_head_ = 0;

	/* Reset buffers */
	memset(&self->values_, 0, sizeof(self->values_));
	self->values_.stripes_ = 1;
	memset(&self->sparse_values_, 0, sizeof(self->sparse_values_));
}

const char *
tsv_reader_error(const tsv_reader_t *self)
{
	assert(self != NULL);

	/* Get error message */
	return self->error_;
}

static
int
tsv_reader_seterror_(tsv_reader_t *self, const char *error)
{
	assert(self != NULL);
	assert(error != NULL);

	/* Set error message */
	self->error_ = error;

	return -1;   /* Convenience return value */
}

static
int
tsv_reader_next_(tsv_reader_t *self)
{
#	define COLNAME_FLAG (0x1)
#	define ROWNAME_FLAG (0x2)
	int type;

	assert(self != NULL);

	/* Classify field coordinate */
	type = 0;
	if ((self->flags_ & TSV_READER_COLNAMES) != 0)
		if (!(self->cols_ > 0))
			type |= COLNAME_FLAG;
	if ((self->flags_ & TSV_READER_ROWNAMES) != 0)
		if (!((self->cols_ > 0 ? self->field_ % self->cols_ : self->field_) > 0))
			type |= ROWNAME_FLAG;

	/* Handle */
	switch (type)
	{
		case 0:
		{{
			char *endp;
			double value;

			/* Is non-empty? If it's empty we might not have space for '\0' */
			if (scratch_used_(&self->scratch_) > 0)
			{
				/* Zero terminate data */
				self->scratch_.data_[self->scratch_.tail_] = '\0';

				/* Parse value */
				value = strtod(&self->scratch_.data_[self->scratch_.head_], &endp);
				if (!(endp == &self->scratch_.data_[self->scratch_.tail_]))
					return tsv_reader_seterror_(self, "value is not numeric");
			}
			else
				/* PArse empty */
				return tsv_reader_seterror_(self, "value is not numeric");

			/* Store value */
			if ((self->flags_ & TSV_READER_SPARSE) == 0)
			{
				if (values_push_(&self->values_, value) != 0)
					return tsv_reader_seterror_(self, "no memory");
			}
			else
				if (value != 0. && sparse_values_push_(&self->sparse_values_, value,
						self->field_ / self->cols_ - ( (self->flags_ & TSV_READER_COLNAMES) != 0 ),
						self->field_ % self->cols_ - ( (self->flags_ & TSV_READER_ROWNAMES) != 0 )) != 0)
					return tsv_reader_seterror_(self, "no memory");

			break;
		}}

		case COLNAME_FLAG:
		case ROWNAME_FLAG:
		{{
			size_t size;

			/* Get field size */
			size = self->scratch_.tail_ - self->scratch_.head_;

			/* Add the field size */
			if (scratch_push_(&self->scratch_, &size, sizeof(size)) == NULL)
				return tsv_reader_seterror_(self, "no memory");

			/* Swap the field size & field data */
			memmove(&self->scratch_.data_[self->scratch_.head_ + sizeof(size)], &self->scratch_.data_[self->scratch_.head_], size);
			memcpy(&self->scratch_.data_[self->scratch_.head_], &size, sizeof(size));

			/* Leave stuff committed in the buffer */
			self->scratch_.head_ = self->scratch_.tail_;

			break;
		}}
	}

	/* Rotate field */
	++self->field_;
	self->scratch_.tail_ = self->scratch_.head_;

	return 0;
}

static
int
tsv_reader_ff_(tsv_reader_t *self)
{
	assert(self != NULL);
	
	/* Move forward the field pointer */
	if (tsv_reader_next_(self) != 0)
		return -1;

	/* Check for excess columns */
	if (self->cols_ > 0)
		if (self->field_ % self->cols_ == 0)
			return tsv_reader_seterror_(self, "excess columns found");

	return 0;
}

static
int
tsv_reader_lf_(tsv_reader_t *self)
{
	assert(self != NULL);

	/* Move forward the field pointer */
	if (tsv_reader_next_(self) != 0)
		return -1;

	/* Check for too few columns */
	if (self->cols_ > 0)
	{
		if (self->field_ % self->cols_ != 0)
			return tsv_reader_seterror_(self, "missing columns found");
	}
	else
	{
		/* First row complete-- store width & split pointer */
		self->cols_ = self->field_;
		self->rownames_head_ = self->scratch_.tail_;

		/* If we are going to transpose the data, it's time to set the stripe
		 * width, so the data can scatter into right place */
		if ((self->flags_ & TSV_READER_TRANSP) != 0)
		{
			size_t cols;

			/* Figure out number of data columns */
			cols = self->cols_;
			if ((self->flags_ & TSV_READER_COLNAMES) != 0 && cols > 0)
				--cols;

			/* Apply the striping */
			if ((self->flags_ & TSV_READER_SPARSE) == 0)
				values_setstripes_(&self->values_, cols);
		}
	}

	return 0;
}

int
tsv_reader_parse(tsv_reader_t *self, const void *data, size_t size)
{
	size_t head, tail;

	assert(self != NULL);
	assert(data != NULL || size < 1);

	/* Parse the segment */
	for (head = 0; head < size; head += tail)
		switch (tsv_parser_parse(&self->parser_, &tail, &((const char *)data)[head], size - head))
		{
			case TSV_PARSER_DATA:
				if (scratch_push_(&self->scratch_, &((const char *)data)[head], tail) == NULL)
					return tsv_reader_seterror_(self, "no memory");
				break;

			case TSV_PARSER_FF:
				if (tsv_reader_ff_(self) != 0)
					return -1;
				break;

			case TSV_PARSER_LF:
				if (tsv_reader_lf_(self) != 0)
					return -1;
				break;

			case -1:
				return tsv_reader_seterror_(self, "parse error");
		}

	return 0;
}

int
tsv_reader_finish(tsv_reader_t *self)
{
	assert(self != NULL);

	/* Finish parsing */
	if (tsv_parser_finish(&self->parser_) != 0)
		return tsv_reader_seterror_(self, "parse error");

	/* Terminate last line */
	if (scratch_used_(&self->scratch_) > 0)
		if (tsv_reader_lf_(self) != 0)
			return -1;

	/* Add the index leader in the sparse matrix
	 * need to do it here bc sparse_values_push_() won't be called for empty matrix
	 * and init() can't allocate */
	if ((self->flags_ & TSV_READER_SPARSE) != 0)
		if (sparse_values_commit_(&self->sparse_values_, tsv_reader_rows(self)) != 0)
			return tsv_reader_seterror_(self, "no memory");

	/* Give up extra memory */
	if ((self->flags_ & TSV_READER_SPARSE) == 0)
	{
		if (values_compact_(&self->values_) != 0)
			return tsv_reader_seterror_(self, "no memory");
	}
	else
		if (sparse_values_compact_(&self->sparse_values_, tsv_reader_rows(self)) != 0)
			return tsv_reader_seterror_(self, "no memory");

	/* Transpose sparse data */
	if ((self->flags_ & TSV_READER_SPARSE) != 0)
		if ((self->flags_ & TSV_READER_TRANSP) != 0)
		{
			double *data;
			size_t *rowinds;
			size_t *colptrs;

			data = (double *)malloc(self->sparse_values_.head_ * sizeof(*data));
			if (data == NULL && self->sparse_values_.head_ > 0)
				return tsv_reader_seterror_(self, "no memory");
			rowinds = (size_t *)malloc(self->sparse_values_.head_ * sizeof(*rowinds));
			if (rowinds == NULL && self->sparse_values_.head_ > 0)
			{
				free(data);
				return tsv_reader_seterror_(self, "no memory");
			}
			colptrs = (size_t *)malloc((tsv_reader_cols(self) + 1) * sizeof(*colptrs));
			if (colptrs == NULL)
			{
				free(data);
				free(rowinds);
				return tsv_reader_seterror_(self, "no memory");
			}

			sparse_values_transpose_(data, rowinds, colptrs,
				self->sparse_values_.data_, self->sparse_values_.rowptrs_, self->sparse_values_.colinds_,
				tsv_reader_rows(self), tsv_reader_cols(self));

			self->sparse_values_.data_ = data;
			self->sparse_values_.tail_ = self->sparse_values_.head_;
			self->sparse_values_.colinds_ = rowinds;
			self->sparse_values_.col_tail_ = self->sparse_values_.head_;
			self->sparse_values_.rowptrs_ = colptrs;
			self->sparse_values_.row_tail_ = tsv_reader_cols(self);
		}

	/* Allocate dense image for the sparse data
	 * need to do it here because tsv_reader_data() can't fail */
	if ((self->flags_ & TSV_READER_SPARSE) != 0)
	{
		self->values_.data_ = (double *)calloc(tsv_reader_rows(self) * tsv_reader_cols(self), sizeof(*self->values_.data_));

		/* NOTE: !!! don't populate so we don't use memory !!! */

		if (self->values_.data_ == NULL && self->sparse_values_.head_ > 0)
			return tsv_reader_seterror_(self, "no memory");
	}

	return 0;
}

const char *
tsv_read(tsv_reader_t *self, const char *filename)
{
	gzFile file;
	int result;
	char data[4096];

	assert(self != NULL);
	assert(filename != NULL);

	/* Open the file */
	file = gzopen(filename, "rb");
	if (file == NULL)
	{
		tsv_reader_seterror_(self, "failed to open for read");
		return tsv_reader_error(self);
	}

	/* Parse */
	while ((result = gzread(file, data, sizeof(data))) > 0)
		if (tsv_reader_parse(self, data, (size_t)result) != 0)
		{
			(void)gzclose_r(file);
			return tsv_reader_error(self);
		}

	/* Check for read errors */
	if (result == 0)
		result = gzclose_r(file);
	else
		(void)gzclose_r(file);
	if (result != 0)
	{
		tsv_reader_seterror_(self, "read error");
		return tsv_reader_error(self);
	}

	/* Finish parse */
	if (tsv_reader_finish(self) != 0)
		return tsv_reader_error(self);

	return NULL;
}

size_t
tsv_reader_cols(const tsv_reader_t *self)
{
	size_t cols;

	assert(self != NULL);
	
	/* Get number of columns */
	cols = self->cols_;

	/* Adjust for headers */
	if ((self->flags_ & TSV_READER_COLNAMES) != 0 && cols > 0)
		--cols;

	return cols;
}

size_t
tsv_reader_rows(const tsv_reader_t *self)
{
	size_t rows;

	assert(self != NULL);

	/* Get number of rows */
	if (self->cols_ > 0)
	{
		/* Compute dimension */
		rows = self->field_ / self->cols_;

		/* Adjust for headers */
		if ((self->flags_ & TSV_READER_ROWNAMES) != 0)
			--rows;
	}
	else
		rows = 0;

	return rows;
}

double *
tsv_reader_data(const tsv_reader_t *self)
{
	assert(self != NULL);

	/* TODO: now will copy data again if tsv_reader_data() is called twice
	 * but we can't do much about it here */

	/* NOTE: transpose moved to tsv_reader_finish() */

	/* Populate sparse values */
	if ((self->flags_ & TSV_READER_SPARSE) != 0)
		if (self->values_.data_ != NULL)
		{
			if ((self->flags_ & TSV_READER_TRANSP) == 0)
				sparse_values_to_dense_(self->values_.data_, &self->sparse_values_, tsv_reader_rows(self), tsv_reader_cols(self), 0);
			else
				sparse_values_to_dense_(self->values_.data_, &self->sparse_values_, tsv_reader_cols(self), tsv_reader_rows(self), 0);
		}

	/* Get data */
	return self->values_.data_;
}

double *
tsv_reader_data_release(tsv_reader_t *self)
{
	double *data;

	assert(self != NULL);

	/* Populate sparse values */
	if ((self->flags_ & TSV_READER_SPARSE) != 0)
		if (self->values_.data_ != NULL)
		{
			if ((self->flags_ & TSV_READER_TRANSP) == 0)
				sparse_values_to_dense_(self->values_.data_, &self->sparse_values_, tsv_reader_rows(self), tsv_reader_cols(self), 0);
			else
				sparse_values_to_dense_(self->values_.data_, &self->sparse_values_, tsv_reader_cols(self), tsv_reader_rows(self), 0);
		}

	/* Get data */
	data = self->values_.data_;

	/* Kill */
	self->values_.data_ = NULL;
	self->values_.head_ = 0;

	return data;
}

double
tsv_reader_sparse_density(const tsv_reader_t *self)
{
	assert(self != NULL);
	assert((self->flags_ & TSV_READER_SPARSE) != 0);

	if ((self->flags_ & TSV_READER_SPARSE) == 0)
		return 1.;
	else
		return (double)self->sparse_values_.head_ / ( tsv_reader_rows(self) * tsv_reader_cols(self) );
}

size_t
tsv_reader_sparse_data(double **x, const size_t **p, const size_t **j, const tsv_reader_t *self)
{
	assert(self != NULL);
	assert(x != NULL);
	assert(p != NULL);
	assert(j != NULL);
	assert((self->flags_ & TSV_READER_SPARSE) != 0);

	/* Get data */
	*x = self->sparse_values_.data_;
	*j = self->sparse_values_.colinds_;
	*p = self->sparse_values_.rowptrs_;

	return self->sparse_values_.head_;
}

const void *
tsv_reader_colnames(const tsv_reader_t *self)
{
	assert(self != NULL);

	/* Get list head */
	if ((self->flags_ & TSV_READER_COLNAMES) != 0)
		return &self->scratch_.data_[0];

	return NULL;
}

const void *
tsv_reader_rownames(const tsv_reader_t *self)
{
	assert(self != NULL);

	/* Get list head */
	if ((self->flags_ & TSV_READER_ROWNAMES) != 0)
		return &self->scratch_.data_[self->rownames_head_];

	return NULL;
}

char *
tsv_names_next(size_t *size, const void **it)
{
	char *name;

	assert(size != NULL);
	assert(it != NULL && *it != NULL);

	/* Extract size */
	memcpy(size, *it, sizeof(*size));
	/* Extract name */
	name = (char *)&((const char *)*it)[sizeof(*size)];

	/* Move the iterator forward */
	*it = &((const char *)*it)[sizeof(*size) + *size];

	return name;
}

void
tsv_reader_deinit(tsv_reader_t *self)
{
	assert(self != NULL);

	/* Free buffers */
	free(self->scratch_.data_);
	free(self->values_.data_);
	free(self->sparse_values_.data_);
	free(self->sparse_values_.colinds_);
	free(self->sparse_values_.rowptrs_);
}
