
#include "tsv_parser.h"
#include <assert.h>

enum Token {
	T_o = 0, /* Other */
	T_q = 1, /* Quote */
	T_t = 2, /* Field separator, '\t' */ 
	T_n = 3, /* Line separator, '\n' */
	T_r = 4, /* Line separator leader, '\r' */
	T_TOP
};

struct Action {
	/* Kind is the segment kind,
	 * state is the next state,
	 * chain is the scan chain-- this could be computed from the rest
	 */
#ifdef __GNUC__
	__extension__ unsigned char kind:2, state:2, chain:2;
#else
	unsigned kind:2, state:2, chain:2;
#endif
};

/* Finite automaton for the parser */
static const struct Action fsa[][T_TOP] = {
/*  T_o    , T_q    , T_t    , T_n    , T_q     */
	{ {1,0,1}, {0,1,0}, {2,0,0}, {3,0,0}, {3,3,0} }, /* state=0 */
	{ {1,1,2}, {0,2,0}, {1,1,2}, {1,1,2}, {1,1,2} }, /* state=1 */
	{ {1,0,1}, {1,1,2}, {2,0,0}, {3,0,0}, {3,3,0} }, /* state=2 */
	{ {1,0,1}, {0,1,0}, {2,0,0}, {0,0,0}, {3,3,0} }, /* state=3 */
};

/* Scan chains-- there is a chain if fsa[s][t] = {.kind=k, .state=s} for some k
 * and for all t in some nonempty token set T, which is its accept set. The
 * chain (k,s,T) is entered from any state s0 with token t0 iff fsa[s0][t1] =
 * {k,s}, even if s0 != s. Here only k=0 and k=1 are considered, as we don't
 * want to coalesce the field/line separators. */
static const unsigned char fsa_chain[] = {
	(unsigned char)0,           /* Empty chain */
	(unsigned char) (1 << T_o), /* Unquoted chain: /[^\"\t\n\r]/ */
	(unsigned char)~(1 << T_q), /* Quoted chain: /[^\"]/ */
};

static
enum Token
tokenize(char ch, const char *flavor)
{
	size_t i;

	assert(flavor != NULL);

	/* Check special characters */
	for (i = 1; i < T_TOP; ++i)
		if (ch == flavor[i-1])
			return (enum Token)i;

	return (enum Token)0;
}

static
size_t
scan_token(const char *data, size_t size, const char *flavor, unsigned accept)
{
	size_t i;

	assert(data != NULL || size < 1);
	assert(flavor != NULL);

	for (i = 0; i < size; ++i)
		if (((accept >> tokenize(data[i], flavor)) & 0x1u) == 0)
			break;

	return i;
}

static
void
copy_flavor(char *dest, const char *src)
{
	size_t i;
	const char *s;

	assert(dest != NULL);

	/* Put in the default */
	for (i = 1; i < T_TOP; ++i)
		dest[i-1] = TSV_PARSER_EXCEL[i-1];

	/* Check syntax */
	if (src == NULL || src[0] == '\0')
		/* NULL or "", use default */ ;
	else if (src[1] == '\0')
		/* Single char is field separator */   
		dest[T_t-1] = src[0];
	else if (src[2] == '\0')
	{
		/* Two are { quote, field sep } */
		dest[T_q-1] = src[0];
		dest[T_t-1] = src[1];
	}
	else
		/* Remaining fill-in order */
		for (i = 1, s = src; i < T_TOP; ++i, s += s[1] != '\0')
			dest[i-1] = *s;
}

const char TSV_PARSER_EXCEL_[4] = "\"\t\n\r";

void
tsv_parser_init(tsv_parser_t *self, const char *flavor)
{
	assert(self != NULL);

	/* Go to first state */
	self->state_ = 0;
	copy_flavor(self->flavor_, flavor);
}

int
tsv_parser_parse(tsv_parser_t *self, size_t *tail, const void *data, size_t size)
{
	struct Action action;

	assert(self != NULL);
	assert(tail != NULL);
	assert(data != NULL || size < 1);

	/* Empty? */
	if (!(size > 0))
		return TSV_PARSER_SKIP;

	/* Process token */
	action = fsa[self->state_][tokenize(*(const char *)data, self->flavor_)];
	self->state_ = action.state;

	/* Coalesce trailing tokens */
	*tail = scan_token( &((const char *)data)[1], size - 1, self->flavor_, fsa_chain[action.chain] ) + 1; 

	return action.kind;
}

int
tsv_parser_finish(const tsv_parser_t *self)
{
	assert(self != NULL);

	/* Check for quotes */
	if (fsa[self->state_][T_o].state != 0)
		return -1;

	return 0;
}
