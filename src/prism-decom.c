
#include "libprism/decom.h"
#include "rgetopt.h"
#include "tsv_reader.h"
#include "tsv_writer.h"
#include "xalloc.h"
#include <alloca.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

static
void
decom_spoi_sparse(double *t_Z, double *W, const double *t_X, prism_opt_sparse_t t_Y, prism_opt_sparse_t w, size_t m, size_t k, size_t r, size_t n, size_t max_iters, double min_delta)
{
	double *t_T;

	/* TODO: T could be large, don't stack allocate, but is small in practice */

	/* Get memory for the noise parameter */
	if (k*m*r <= 250000ul)
		t_T = (double *)alloca(k*m*r * sizeof(*t_T));
	else
		t_T = (double *)xalloca(k*m*r, sizeof(*t_T));

	/* Decompose */
	prism_decom_spoi_sparse(t_Z, t_T, W, t_X, t_Y, w, m, k, r, n, max_iters, min_delta);
}

static
void
poi_condition(double *t_Z, const double *w, const double *x, size_t k, size_t m)
{
	size_t i, l;
	double lam, coe;

	assert(t_Z != NULL || k*m < 1);
	assert(w != NULL || k < 1);
	assert(x != NULL || m < 1);

	/* Loop through genes */
	for (i = 0; i < m; ++i)
	{
		/* Compute effective rates */
		lam = 0.;
#pragma omp simd
		for (l = 0; l < k; ++l)
		{
			t_Z[l + i*k] *= w[l];
			lam += t_Z[l + i*k];
		}

		/* Scale */
		coe = 0.;
		if (x[i] > 0. && lam > 0.)
			coe = x[i] / lam;

		/* Condition */
#pragma omp simd
		for (l = 0; l < k; ++l)
			t_Z[l + i*k] *= coe;
	}
}

static
void
l2_condition(double *t_Z, const double *w, const double *x, size_t k, size_t m)
{
	size_t i, l;
	double lam, bias;

	assert(t_Z != NULL || k*m < 1);
	assert(w != NULL || k < 1);
	assert(x != NULL || m < 1);

	/* Loop through genes */
	for (i = 0; i < m; ++i)
	{
		/* Compute effective rates */
		lam = 0.;
#pragma omp simd
		for (l = 0; l < k; ++l)
		{
			t_Z[l + i*k] *= w[l];
			lam += t_Z[l + i*k];
		}

		/* Adjust */
		bias = (( x[i] - lam )) / (double)k;

		/* Condition */
#pragma omp simd
		for (l = 0; l < k; ++l)
			t_Z[l + i*k] += bias;
	}
}

static
double
estimate_bulk_bias(const double *t_X, prism_opt_sparse_t t_Y, size_t m, size_t r, size_t n)
{
	double sx, sy;
	size_t ij;

	assert(t_X != NULL || r*m < 1);
	assert(prism_assert_sparse(t_Y, n, m));

	/* Sum up X */
	sx = 0.;
#pragma omp simd reduction(+: sx)
	for (ij = 0; ij < r*m; ++ij)
		sx += t_X[ij];

	/* Sum up Y */
	sy = 0.;
	if (!prism_is_sparse(t_Y))
#pragma omp simd reduction(+: sy)
		for (ij = 0; ij < n*m; ++ij)
			sy += t_Y._x[ij];
	else
	{
		size_t i, q;

		for (i = 0; i < m; ++i)
#pragma omp simd reduction(+: sy)
			for (q = t_Y._p[i]; q < t_Y._p[i + 1]; ++q)
				sy += t_Y._x[q];
	}

	return ( sy / n ) / ( sx / r + sy / n );
}
	
static
void
apply_bulk_bias(double *x, size_t m, double bulk_bias)
{
	double scale;
	size_t i;

	assert(x != NULL || m < 1);

	/* Get scaling factor */
	scale = bulk_bias / (1. - bulk_bias);

	/* Scale */
#pragma omp simd
	for (i = 0; i < m; ++i)
		x[i] *= scale;
}

static
void
unapply_bulk_bias(double *w, size_t k, double bulk_bias)
{
	double scale;
	size_t l;

	assert(w != NULL || k < 1);

	/* Get scaling factor */
	scale = (1. - bulk_bias) / bulk_bias;

	/* Scale */
#pragma omp simd
	for (l = 0; l < k; ++l)
		w[l] *= scale;
}

struct decomposer {
	void (*decompose)(double *t_Z, double *W, const double *t_X, prism_opt_sparse_t t_Y, prism_opt_sparse_t w, size_t m, size_t k, size_t r, size_t n, size_t max_iters, double min_delta);
	void (*condition)(double *t_Z, const double *w, const double *x, size_t k, size_t m);
};

static const struct decomposer poi_decomposer = { &prism_decom_poi_sparse, &poi_condition };
static const struct decomposer spoi_decomposer = { &decom_spoi_sparse, &poi_condition };
static const struct decomposer l2_decomposer = { &prism_decom_l2_sparse, &l2_condition };

struct opts {
	/* Model */
	const struct decomposer *decomposer;
	/* Fixups */
	double bulk_bias;
	/* Optimizer parameters */
	size_t max_iters;
	double min_delta;
	double sparse_density;
	/* File format options */
	unsigned headers:1;
	/* Output files */
	const char *dest_expr_fn, *dest_gains_fn, *dest_weights_fn;
	/* Input files */
	const char *src_bulk_expr_fn, *src_sc_expr_fn, *src_sc_gains_fn, *src_sc_weights_fn;
};

static
void
default_args(struct opts *opts)
{
	assert(opts != NULL);

	/* Model */
	opts->decomposer = &spoi_decomposer;
	/* Optimization options */
	opts->max_iters = 1000;
	opts->min_delta = 1e-10;
	opts->sparse_density = .5;
	/* Fixups */
	opts->bulk_bias = .5;
	/* File format */
	opts->headers = 0;
	/* Output files */
	opts->dest_expr_fn = NULL;
	opts->dest_gains_fn = NULL;
	opts->dest_weights_fn = NULL;
	/* Input files */
	opts->src_bulk_expr_fn = NULL;
	opts->src_sc_expr_fn = NULL;
	opts->src_sc_gains_fn = NULL;
	opts->src_sc_weights_fn = NULL;
}

static
int
parse_args(struct opts *opts, int argc, char **argv)
{
	rgetopt_t getopt;
	int opt;

	assert(opts != NULL);
	assert(argc > 0);
	assert(argv != NULL);

	/* Set up the parser */
	rgetopt_init(&getopt, argc, argv, ": m: b: k:t: d: H g:w: Z:G:W:");
	rgetopt_sort(&getopt);

	/* Set defaults */
	default_args(opts);

	/* Parse */
	while ((opt = rgetopt_next(&getopt)) != -1)
		switch (opt)
		{
			/* Model */
			case 'm':
				if      (strcmp(getopt.optarg, "poi") == 0)
					opts->decomposer = &poi_decomposer;
				else if (strcmp(getopt.optarg, "spoi") == 0)
					opts->decomposer = &spoi_decomposer;
				else if (strcmp(getopt.optarg, "l2") == 0)
					opts->decomposer = &l2_decomposer;
				else
					goto bad_arg;
				break;

			/* Fixups */
			case 'b':
			{{
				char stop;

				if (strcmp(getopt.optarg, "auto") == 0)
					opts->bulk_bias = -1.;
				else
				{
					if (sscanf(getopt.optarg, "%lf%c", &opts->bulk_bias, &stop) != 1)
						goto bad_arg;
					if (!(0. < opts->bulk_bias && opts->bulk_bias < 1.))
						goto bad_arg;
				}

				break;
			}}

			/* Optimizer parameters */
			case 'k':
			{{
				unsigned long max_iters;
				char stop;

				if (sscanf(getopt.optarg, "%lu%c", &max_iters, &stop) != 1)
					goto bad_arg;
				if (!(max_iters > 0))
					goto bad_arg;
				opts->max_iters = (size_t)max_iters;

				break;
			}}
			case 't':
			{{
				char stop;

				if (sscanf(getopt.optarg, "%lf%c", &opts->min_delta, &stop) != 1)
					goto bad_arg;

				break;
			}}
			case 'd':
			{{
				char stop;

				if (sscanf(getopt.optarg, "%lf%c", &opts->sparse_density, &stop) != 1)
					goto bad_arg;

				break;
			}}
				
			/* File format options */
			case 'H':
				opts->headers = 1;
				break;

			/* Output file names */
			case 'Z':
				opts->dest_expr_fn = getopt.optarg;
				break;
			case 'G':
				opts->dest_gains_fn = getopt.optarg;
				break;
			case 'W':
				opts->dest_weights_fn = getopt.optarg;
				break;

			/* Supplemental input file names */
			case 'g':
				opts->src_sc_gains_fn = getopt.optarg;
				break;
			case 'w':
				opts->src_sc_weights_fn = getopt.optarg;
				break;

			/* Invalid option */
			default: /* '?' */
				fprintf(stderr, "%s: invalid option -%c" "\n", argv[0], getopt.optopt);
				return -1;

			/* Missing argument */ 
			case ':':
				fprintf(stderr, "%s: missing argument for option -%c" "\n", argv[0], getopt.optopt);
				return -1;

			/* Invalid option argument */
			bad_arg:
				fprintf(stderr, "%s: invalid argument for option -%c: %s" "\n", argv[0], getopt.optopt, getopt.optarg);
				return -1;
		}

	/* Get positional arguments */
	if (argc - getopt.optind >= 1)
		opts->src_bulk_expr_fn = (&argv[getopt.optind])[0];
	if (argc - getopt.optind >= 2)
		opts->src_sc_expr_fn = (&argv[getopt.optind])[1];
	if (!(argc - getopt.optind <= 2))
	{
		fprintf(stderr, "%s: too many input arguments" "\n", argv[0]);
		return -1;
	}

	/* Check for required filenames */
	if (!(opts->src_bulk_expr_fn != NULL && opts->src_sc_expr_fn != NULL && opts->src_sc_weights_fn != NULL))
	{
		fprintf(stderr, "%s: missing input files" "\n", argv[0]);
		return -1;
	}

	return 0;
}

static
void
usage(const char *argv0)
{
	fprintf(stderr, "Usage: %s [options] bulk_expr.tsv sc_expr.tsv [-g sc_gains.tsv] -w sc_labels.tsv" "\n"
"Options:" "\n"
"  -m model  -b bias  -H  -g sc_gains.tsv    -Z decom_bulk_expr.tsv  " "\n"
"                         -w sc_weights.tsv  -G bulk_gains.tsv       " "\n"
"                                            -W bulk_weights.tsv     " "\n"
		, argv0);
}

static
double
safe_div(double x, double y)
{
	/* Compute x/y with 0./0. -> 0. for x >= 0. */
	if (x > 0.)
		return x / y;
	else
		return 0.;
}

static
void
apply_gain(double *w, double g, size_t k)
{
	size_t l;

	/* Factor the scale in */
#pragma omp simd
	for (l = 0; l < k; ++l)
		w[l] *= g;
}

static
double
split_gain(double *w, size_t k)
{
	size_t l;
	double sum_w;

	/* Get gain */
	sum_w = 0.;
#pragma omp simd reduction(+: sum_w)
	for (l = 0; l < k; ++l)
		sum_w += w[l];

	/* Normalize */
#pragma omp simd
	for (l = 0; l < k; ++l)
		w[l] = safe_div(w[l], sum_w);

	return sum_w;
}

static
const char *
x_tsv_read(tsv_reader_t *reader, const char *filename, const char *flavor, int flags)
{
	const char *err_msg;

	/* Set up */
	tsv_reader_init(reader, flavor, flags);

	/* Read */
	err_msg = tsv_read(reader, filename);
	if (err_msg != NULL)
	{
		tsv_reader_deinit(reader);
		return err_msg;
	}

	/* Schedule memory to be freed */
	xatexit(&free, reader->scratch_.data_);
	xatexit(&free, reader->values_.data_);

	return NULL;
}

static
const char *
tsv_write_Z(const char *filename, const double *t_Z, size_t k, size_t m, size_t r, int file_flags, const void *k_names, const void *m_names, const void *r_names)
{
	FILE *file;
	size_t col;
	size_t i, j, l;
	const void *it;

	/* Open the file */
	file = fopen(filename, "w");
	if (file == NULL)
		return "failed to open for write";

	/* Set up column counter */
	col = 0;

	/* Dump header */
	if ((file_flags & TSV_READER_COLNAMES) != 0)
	{
		const void *it1, *it2;
		const char *name1, *name2;
		size_t size1, size2;

		/* Dump pivot */
		if ((file_flags & TSV_READER_ROWNAMES) != 0)
			if (tsv_write_field(file, &col, "", 0) != 0)
				goto write_error;

		/* Dump column names */
		it1 = r_names;
		for (j = 0; j < r; ++j)
		{
			name1 = tsv_names_next(&size1, &it1);
			it2 = k_names;
			for (l = 0; l < k; ++l)
			{
				name2 = tsv_names_next(&size2, &it2);
				if (tsv_write_fieldv(file, &col, name1, size1, ".", 1, name2, size2, NULL) != 0)
					goto write_error;
			}
		}

		/* Next line */
		if (tsv_write_newline(file, &col) != 0)
			goto write_error;
	}

	/* Write data */
	{{
		const char *name;
		size_t size;

		/* Get row names */
		it = NULL;
		if ((file_flags & TSV_READER_ROWNAMES) != 0)
			it = m_names;

		/* Loop */
		for (i = 0; i < m; ++i)
		{
			/* Write header */
			if (it != NULL)
			{
				name = tsv_names_next(&size, &it);
				if (tsv_write_field(file, &col, name, size) != 0)
					goto write_error;
			}

			/* Write data */
			for (j = 0; j < r; ++j)
				for (l = 0; l < k; ++l)
					if (tsv_write_number(file, &col, t_Z[l + i*k + j*k*m]) != 0)
						goto write_error;

			/* Next line */
			if (tsv_write_newline(file, &col) != 0)
			{
write_error:
				(void)fclose(file);

				return "write error";
			}
		}
	}}

	/* Close the file */
	if (fclose(file) != 0)
		return "write error";

	return NULL;
}

static
prism_opt_sparse_t
tsv_make_sparse(const tsv_reader_t *reader, double sparse_density)
{
	if (tsv_reader_sparse_density(reader) < sparse_density)
	{
		double *x;
		const size_t *i, *p;

		(void)tsv_reader_sparse_data(&x, &p, &i, reader);

		return prism_sparse(x, i, p);
	}
	else
		return prism_dense(tsv_reader_data(reader));
}

int
main(int argc, char **argv)
{
	struct opts opts;
	int file_flags;
	tsv_reader_t X_reader, Y_reader, w_reader, g_reader;
	const char *err_msg;
	double *X, *g, *t_Z, *W, *G;
	prism_opt_sparse_t t_Y, w;
	size_t m, r, n, k, j;

	/* Parse arguments */
	if (parse_args(&opts, argc, argv) != 0)
	{
		usage(argv[0]);
		return EXIT_FAILURE;
	}

	/* Set up input options */
	file_flags = 0;
	if (opts.headers)
		file_flags = TSV_READER_COLNAMES | TSV_READER_ROWNAMES;

	/* Read bulk data */
	if ((err_msg = x_tsv_read(&X_reader, opts.src_bulk_expr_fn, NULL, TSV_READER_TRANSP | file_flags)) != NULL)
	{
		fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.src_bulk_expr_fn, err_msg);
		return EXIT_FAILURE;
	}

	/* Read single-cell data */
	if ((err_msg = x_tsv_read(&Y_reader, opts.src_sc_expr_fn, NULL, file_flags | TSV_READER_SPARSE)) != NULL)
	{
		fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.src_sc_expr_fn, err_msg);
		return EXIT_FAILURE;
	}
	/* Check dimensions */
	if (!(tsv_reader_rows(&Y_reader) == tsv_reader_rows(&X_reader)))
	{
		fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.src_sc_expr_fn, "invalid dimensions");
		return EXIT_FAILURE;
	}

	/* REad weights */
	if ((err_msg = x_tsv_read(&w_reader, opts.src_sc_weights_fn, NULL, file_flags | TSV_READER_SPARSE)) != NULL)
	{
		fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.src_sc_weights_fn, err_msg);
		return EXIT_FAILURE;
	}
	/* Check dimensions */
	if (!(tsv_reader_rows(&w_reader) == tsv_reader_cols(&Y_reader)))
	{
		fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.src_sc_weights_fn, "invalid dimensions");
		return EXIT_FAILURE;

	}

	/* Get data & dimensions */
	X = tsv_reader_data(&X_reader);
	m = tsv_reader_rows(&X_reader);
	r = tsv_reader_cols(&X_reader);
	t_Y = tsv_make_sparse(&Y_reader, opts.sparse_density);
	n = tsv_reader_cols(&Y_reader);
	w = tsv_make_sparse(&w_reader, opts.sparse_density);
	k = tsv_reader_cols(&w_reader);

	/* Read gains */
	if (opts.src_sc_gains_fn != NULL)
	{
		if ((err_msg = x_tsv_read(&g_reader, opts.src_sc_gains_fn, NULL, file_flags)) != NULL)
		{
			fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.src_sc_gains_fn, err_msg);
			return EXIT_FAILURE;
		}
		/* Check dimensions */
		if (!(tsv_reader_rows(&g_reader) == 1 && tsv_reader_cols(&g_reader) == tsv_reader_cols(&Y_reader)))
		{
			fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.src_sc_gains_fn, "invalid dimensions");
			return EXIT_FAILURE;
		}

		/* Get gains */
		g = tsv_reader_data_release(&g_reader);
	}
	else
	{
		/* Make empty gains */
		g = (double *)xalloca(n, sizeof(*g));

		/* Fill in */
		for (j = 0; j < n; ++j)
			g[j] = 1.;
	}

	/* Allocate output */
	t_Z = (double *)xalloca(k*m*r, sizeof(*t_Z));
	W = (double *)xalloca(k*r, sizeof(*W));
	G = (double *)xalloca(r, sizeof(*G));

	/* Fuse in the gains */
	if (!prism_is_sparse(w))
#pragma omp parallel for schedule(static)
		for (j = 0; j < n; ++j)
			apply_gain(&w._x[j*k], g[j], k);
	else
#pragma omp parallel for schedule(static)
		for (j = 0; j < n; ++j)
			apply_gain(&w._x[w._p[j]], g[j], w._p[j+1]-w._p[j]);

	/* Perform decomposition */
#pragma omp parallel for schedule(dynamic)
	for (j = 0; j < r; ++j)
	{
		double bulk_bias;

		/* Apply bias */
		if ((bulk_bias = opts.bulk_bias) != .5)
		{
			if (opts.bulk_bias == -1.)
				apply_bulk_bias(&X[j*m], m, bulk_bias = estimate_bulk_bias(&X[j*m], t_Y, m, 1, n));
			else
				apply_bulk_bias(&X[j*m], m, bulk_bias);
		}

		/* Optimize parameters */
		(*opts.decomposer->decompose)(&t_Z[j*k*m], &W[j*k], &X[j*m], t_Y, w, m, k, 1, n, opts.max_iters, opts.min_delta);

		/* Apply bias on parameters */
		if (opts.bulk_bias != .5)
			unapply_bulk_bias(&W[j*k], k, bulk_bias);

		/* Convert Z from rates to reads */
		(*opts.decomposer->condition)(&t_Z[j*k*m], &W[j*k], &X[j*m], k, m);
	}

	/* Split out the gains */
#pragma omp parallel for schedule(static)
	for (j = 0; j < r; ++j)
		G[j] = split_gain(&W[j*k], k);

	/* Dump profiles */
	if (opts.dest_expr_fn != NULL)
		if ((err_msg = tsv_write_Z(opts.dest_expr_fn, t_Z, k, m, r, file_flags, tsv_reader_colnames(&w_reader), tsv_reader_rownames(&X_reader), tsv_reader_colnames(&X_reader))) != NULL)
			fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.dest_expr_fn, err_msg);

	/* Dump gains */
	if (opts.dest_gains_fn != NULL)
		if ((err_msg = tsv_write(opts.dest_gains_fn, G, 1, r, file_flags, tsv_reader_colnames(&X_reader), tsv_names_put(NULL, "gain"))) != NULL)
			fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.dest_gains_fn, err_msg);

	/* Dump weights */
	if (opts.dest_weights_fn != NULL)
		if ((err_msg = tsv_write(opts.dest_weights_fn, W, r, k, file_flags, tsv_reader_colnames(&w_reader), tsv_reader_colnames(&X_reader))) != NULL)
			fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.dest_weights_fn, err_msg);

	return EXIT_SUCCESS;
}
