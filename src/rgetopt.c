
#include "rgetopt.h"
#include <assert.h>
#include <stddef.h>
#include <string.h>

static
int
isvalidopt(int opt)
{
	/* TODO: extend option set, but don't allow '-', ':', '?', and friends */

	/* Validate option */
	return strchr("abcdefghijklmnopqrstuvwxyz" "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"0123456789", opt) != NULL;
}

static
void
parseoptstr(char *flags, const char *optstr)
{
	int lastopt;
	size_t i;

	assert(flags != NULL && optstr != NULL);

	/* Zero flags */
	memset(flags, '?', (size_t)CHAR_MAX + 1u);
	
	/* Parse */
	lastopt = -1;
	for (i = 0; optstr[i] != '\0'; ++i)
		if (isvalidopt(optstr[i]))
		{
			/* Flag option */
			lastopt = optstr[i];
			flags[lastopt] = '.';
		}
		else if (optstr[i] == ':' && lastopt != -1)
		{
			/* Flag for argument */
			flags[lastopt] = ':';
		}
}

static
void
rgetopt_reset(rgetopt_t *self)
{
	assert(self != NULL);

	/* Reset parser */
	self->optind = 1;
	self->optopt = '?';
	self->used_ = 1;
	self->optarg = NULL;
}

void
rgetopt_init(rgetopt_t *self, int argc, char **argv, const char *optstr)
{
	assert(self != NULL && argv != NULL && optstr != NULL);

	/* Parse option string */
	parseoptstr(self->flags_, optstr);

	/* Set up argument vector */
	(void)argc;
	self->argv = argv;

	/* Reset parser */
	rgetopt_reset(self);
}

static
int
rgetopt_skip_all_(rgetopt_t *self)
{
	/* Skip all options */
	while (self->argv[self->optind] != NULL)
		++self->optind;

	return self->optind;
}

static
int
rgetopt_skip_opts_(rgetopt_t *self)
{
	assert(self != NULL);

	/* Parse options */
	for (;;)
		switch (rgetopt_next(self))
		{
			default:
				/* Options, skip them */
				break;

			case '?':
			case ':':
				/* Parse error, skip to end */ 
				return rgetopt_skip_all_(self);

			case -1:
				/* Non-option */
				return self->optind;
		}
}

static
int
rgetopt_skip_nonopts_(rgetopt_t *self)
{
	assert(self != NULL);

	/* If we have --, we're finished */
	if (self->argv[self->optind][0] == '-' && 
			self->argv[self->optind][1] == '-' &&
			self->argv[self->optind][2] == '\0')
		return rgetopt_skip_all_(self);

	/* Seek */
	while (self->argv[self->optind] != NULL && self->argv[self->optind][0] != '-')
		++self->optind;

	return self->optind;
}

static
void
reverse(char **argv, int n)
{
	int i;
	char *tmp;

	/* Reverse */
	for (i = 0; i < (n-1)-i; ++i)
	{
		tmp = argv[i];
		argv[i] = argv[(n-1)-i];
		argv[(n-1)-i] = tmp;
	}
}

static
void
rotate(char **argv, int left, int mid, int right)
{
	/* Rotate */
	reverse(&argv[left], mid - left);
	reverse(&argv[mid], right - mid);
	reverse(&argv[left], right - left);
}

void
rgetopt_sort(const rgetopt_t *self)
{
	rgetopt_t copy;
	int left, mid, right;

	assert(self != NULL);

	/* Create a copy */
	memcpy(&copy, self, sizeof(*self));

	/* Parse */
	left = rgetopt_skip_opts_(&copy);
	while (copy.argv[left] != NULL)
	{
		/* Parse */
		copy.optind = left;
		mid = rgetopt_skip_nonopts_(&copy);
		if (!(copy.argv[mid] != NULL))
			break;
		right = rgetopt_skip_opts_(&copy);

		/* Rotate */
		rotate((char **)copy.argv, left, mid, right);
		left = right - (mid - left);
	}
}

static
void
rgetopt_shift_(rgetopt_t *self)
{
	assert(self != NULL);
	
	/* Point to the beginning of the next argument */
	++self->optind;
	self->used_ = 1;
}

int
rgetopt_next(rgetopt_t *self)
{
	assert(self != NULL);

	/* Check halting conditions */
	if (self->argv[self->optind] == NULL) /* out of arguments */
		return -1;
	if (self->argv[self->optind][0] != '-')  /* non-option */
		return -1;
	if (self->argv[self->optind][1] == '\0') /* "-" */
		return -1;

	/* Check for "--" */
	if (self->argv[self->optind][1] == '-' &&
			self->argv[self->optind][2] == '\0')
		/* Skip it & we're done */
	{
		rgetopt_shift_(self);
		return -1;
	}

	/* Get option */
	self->optopt = self->argv[self->optind][self->used_++];

	/* Check validity */
	switch (self->flags_[self->optopt])
	{
		case '.':
			/* No option argument, shift in next arg if out of options */
			if (self->argv[self->optind][self->used_] == '\0')
				rgetopt_shift_(self);
			break;

		case ':':
			/* Pull option argument */
			if (self->argv[self->optind][self->used_] == '\0')
			{
				/* Missing option argument */
				if (self->argv[++self->optind] == NULL)
					return ':';

				/* Get option */
				self->optarg = &self->argv[self->optind][0];
			}
			else
				self->optarg = &self->argv[self->optind][self->used_];

			/* Shift in next argument */
			rgetopt_shift_(self);

			break;

		default:
			return '?';
	}

	return self->optopt;
}
