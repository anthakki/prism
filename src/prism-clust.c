
#include "libprism/clust.h"
#include "libprism/hierarch.h"
#include "rgetopt.h"
#include "tsv_reader.h"
#include "tsv_writer.h"
#include "xalloc.h"
#include <alloca.h>
#include <assert.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct clustering {
	size_t (*scratch)(size_t m, size_t n);
	void (*cluster)(double *T, void *scratch, const double *X, const double *G, size_t m, size_t n);
	size_t (*complexity)(size_t m, size_t n);
	void (*centroids)(double *Z, const double *X, const double *G, size_t m, size_t n, const size_t *L, size_t k);
};

static
size_t
poi_complexity(size_t m, size_t n)
{
	(void)m, (void)n;
	return 1; /* Mean */
}

static
double
z_or_div(double x, double y)
{
	/* Compute x/y with 0./0. -> 0. for x >= 0. */
	if (x > 0.)
		return x / y;
	else
		return 0.;
}

static
void
poi_centroids(double *Z, const double *X, const double *G, size_t m, size_t n, const size_t *L, size_t k)
{
	double *S;
	size_t j, i, l;

	assert(Z != NULL || m*k < 1);
	assert(X != NULL || m*n < 1);
	assert(G != NULL || n < 1);
	assert(L != NULL || n < 1);

	/* Get memory */
	S = (double *)alloca(n * sizeof(*S));

	/* Zero */
	memset(Z, 0, m*k * sizeof(*Z));

	/* Scatter data */
	for (j = 0; j < n; ++j)
		for (i = 0; i < m; ++i)
			Z[i + L[j]*m] += X[i + j*m];

	/* Compute normalizers */
	for (j = 0; j < n; ++j)
		S[L[j]] += G[j];

	/* Normalize */
	for (l = 0; l < k; ++l)
		for (i = 0; i < m; ++i)
			Z[i + l*m] = z_or_div(Z[i + l*m], S[l]);
}

static
size_t
spoi_complexity(size_t m, size_t n)
{
	(void)m, (void)n;
	return 2; /* Mean & precision */
}

static
size_t
l2_complexity(size_t m, size_t n)
{
	(void)m, (void)n;
	return 1; /* Mean */
}

static
void
l2_centroids(double *Z, const double *X, const double *G, size_t m, size_t n, const size_t *L, size_t k)
{
	double *S;
	size_t j, i, l;

	assert(Z != NULL || m*k < 1);
	assert(X != NULL || m*n < 1);
	assert(G != NULL || n < 1);
	assert(L != NULL || n < 1);

	/* Get memory */
	S = (double *)alloca(n * sizeof(*S));

	/* Zero */
	memset(Z, 0, m*k * sizeof(*Z));

	/* Scatter data */
	for (j = 0; j < n; ++j)
		for (i = 0; i < m; ++i)
			Z[i + L[j]*m] += G[j] * X[i + j*m];

	/* Compute normalizers */
	for (j = 0; j < n; ++j)
		S[L[j]] += G[j] * G[j];

	/* Normalize */
	for (l = 0; l < k; ++l)
		for (i = 0; i < m; ++i)
			Z[i + l*m] = z_or_div(Z[i + l*m], S[l]);
}

static const struct clustering poi_clustering = { &prism_clust_poi_scratch, &prism_clust_poi, &poi_complexity, &poi_centroids };
static const struct clustering spoi_clustering = { &prism_clust_spoi_scratch, &prism_clust_spoi, &spoi_complexity, &poi_centroids };
static const struct clustering l2_clustering = { &prism_clust_l2_scratch, &prism_clust_l2, &l2_complexity, &l2_centroids };

static
double
bic_penalty(size_t m, size_t n, double pvalue)
{
	(void)pvalue;
	return prism_clust_bic_penalty(m, n);
}

static
double
aic_penalty(size_t m, size_t n, double pvalue)
{
	(void)pvalue;
	return prism_clust_aic_penalty(m, n);
}

struct opts {
	/* Model options */
	const struct clustering *clustering;
	/* Model selection */
	double (*penalty)(size_t m, size_t n, double pvalue);
	size_t clusts;
	double pvalue;
	/* File format options */
	unsigned headers:1;
	/* Output files */
	const char *dest_tree_fn, *dest_expr_fn, *dest_weights_fn;
	/* Input files */
	const char *src_expr_fn, *src_gains_fn, *src_tree_fn;
};

static
void
default_args(struct opts *opts)
{
	assert(opts != NULL);

	/* Model defaults */
	opts->clustering = &spoi_clustering;
	/* Model selection */
	opts->penalty = &bic_penalty;
	opts->clusts = 0;
	opts->pvalue = 0.05;
	/* File format */
	opts->headers = 0;
	/* Output files */
	opts->dest_tree_fn = NULL;
	opts->dest_expr_fn = NULL;
	opts->dest_weights_fn = NULL;
	/* Input files */
	opts->src_expr_fn = NULL;
	opts->src_gains_fn = NULL;
	opts->src_tree_fn = NULL;
}

static
int
parse_args(struct opts *opts, int argc, char **argv)
{
	rgetopt_t getopt;
	int opt;

	assert(opts != NULL);
	assert(argc > 0);
	assert(argv != NULL);

	/* Set up the parser */
	rgetopt_init(&getopt, argc, argv, ": m: k:s:p: H g:t: T:S:Z:W:");
	rgetopt_sort(&getopt);

	/* Set defaults */
	default_args(opts);

	/* Parse */
	while ((opt = rgetopt_next(&getopt)) != -1)
		switch (opt)
		{
			/* Method options */
			case 'm':
				if      (strcmp(getopt.optarg, "poi") == 0)
					opts->clustering = &poi_clustering;
				else if (strcmp(getopt.optarg, "spoi") == 0)
					opts->clustering = &spoi_clustering;
				else if (strcmp(getopt.optarg, "l2") == 0)
					opts->clustering = &l2_clustering;
				else
					goto bad_arg;
				break;

			/* Model selection options */
			case 'k':
			{{
				unsigned long lu;
				char stop;

				if (sscanf(getopt.optarg, "%lu%c", &lu, &stop) != 1)
					goto bad_arg;
				opts->clusts = lu;
				break;
			}}
			case 's':
			{{
				if      (strcmp(getopt.optarg, "bic") == 0)
					opts->penalty = &bic_penalty;
				else if (strcmp(getopt.optarg, "aic") == 0)
					opts->penalty = &aic_penalty;
				else if (strcmp(getopt.optarg, "lr") == 0)
					opts->penalty = &prism_clust_lr_penalty;
				else
					goto bad_arg;
				break;
			}}
			case 'p':
			{{
				char stop;

				if (sscanf(getopt.optarg, "%lf%c", &opts->pvalue, &stop) != 1)
					goto bad_arg;
				opts->penalty = &prism_clust_lr_penalty;
				break;
			}}

			/* File format options */
			case 'H':
				opts->headers = 1;
				break;

			/* Output file names */
			case 'T':
				opts->dest_tree_fn = getopt.optarg;
				break;
			case 'Z':
				opts->dest_expr_fn = getopt.optarg;
				break;
			case 'W':
				opts->dest_weights_fn = getopt.optarg;
				break;

			/* Supplemental input file names */
			case 'g':
				opts->src_gains_fn = getopt.optarg;
				break;
			case 't':
				opts->src_tree_fn = getopt.optarg;
				break;

			/* Invalid option */
			default: /* '?' */
				fprintf(stderr, "%s: invalid option -%c" "\n", argv[0], getopt.optopt);
				return -1;

			/* Missing argument */ 
			case ':':
				fprintf(stderr, "%s: missing argument for option -%c" "\n", argv[0], getopt.optopt);
				return -1;

			/* Invalid option argument */
			bad_arg:
				fprintf(stderr, "%s: invalid argument for option -%c: %s" "\n", argv[0], getopt.optopt, getopt.optarg);
				return -1;
		}

	/* Get positional arguments */
	if (argc - getopt.optind >= 1)
		opts->src_expr_fn = (&argv[getopt.optind])[0];
	if (!(argc - getopt.optind <= 1))
	{
		fprintf(stderr, "%s: too many input arguments" "\n", argv[0]);
		return -1;
	}

	/* Check for required filenames */
	if (!(opts->src_expr_fn != NULL))
	{
		fprintf(stderr, "%s: missing input files" "\n", argv[0]);
		return -1;
	}

	return 0;
}

static
void
usage(const char *argv0)
{
	fprintf(stderr, "Usage: %s [options] expr.tsv [-g gains.tsv]" "\n"
"Options:" "\n"
"  -m model  -k clusts  -H  -g gains.tsv  -T tree.tsv    " "\n"
"            -s bic         -t tree.tsv   -W labels.tsv  " "\n"
"            -s aic                                      " "\n"
"            -p pvalue                                   " "\n"
		, argv0);
}

static
const char *
read_gains(double **G, size_t n, const char *filename, int file_flags)
{
	tsv_reader_t G_reader;

	/* Slurp in the file */
	tsv_reader_init(&G_reader, NULL, file_flags);
	if (tsv_read(&G_reader, filename) != NULL)
	{
		tsv_reader_deinit(&G_reader);
		return tsv_reader_error(&G_reader);
	}

	/* Check for dimensions */
	if (!(tsv_reader_rows(&G_reader) == 1 && tsv_reader_cols(&G_reader) == n))
	{
		tsv_reader_deinit(&G_reader);
		return "invalid dimensions";
	}

	/* Get data & clean up */
	*G = tsv_reader_data_release(&G_reader);
	xatexit(&free, *G);
	tsv_reader_deinit(&G_reader);

	return NULL;
}

static
const char *
default_gains(double **G, size_t n)
{
	size_t i;

	assert(G != NULL);

	/* Allocate */
	*G = (double *)xalloca(n, sizeof(**G));

	/* Put in unity */
	for (i = 0; i < n; ++i)
		(*G)[i] = 1.;

	return NULL;
}

static
int
check_label(double label, size_t n)
{
	size_t k;

	/* Get label & check range */
	k = (size_t)label;
	if (!(k < n))
		return -1;
	/* Check that it was not truncated */
	if (!((double)k == label))
		return -1;

	return 0;
}

static
int
check_tree(const double *T, size_t n)
{
	size_t n1, k;

	/* Get number of splits */
	n1 = n > 0 ? n - 1 : 0;

	/* Loop through */
	for (k = 0; k < n1; ++k)
	{
		/* Check labels */
		if (check_label(T[k + 0*n1], n+k) != 0)
			return -1;
		if (check_label(T[k + 1*n1], n+k) != 0)
			return -1;
	}

	return 0;
}

static
const char *
read_tree(double **T, size_t n, const char *filename, int file_flags)
{
	tsv_reader_t T_reader;
	size_t n1;

	/* Slurp in the file */
	tsv_reader_init(&T_reader, NULL, file_flags & ~(TSV_READER_COLNAMES | TSV_READER_ROWNAMES));
	if (tsv_read(&T_reader, filename) != NULL)
	{
		tsv_reader_deinit(&T_reader);
		return tsv_reader_error(&T_reader);
	}

	/* Get number of splits */
	n1 = n > 0 ? n - 1 : 0;

	/* Check dimensions */
	if (!(tsv_reader_rows(&T_reader) == n1 && tsv_reader_cols(&T_reader) == 3))
	{
		tsv_reader_deinit(&T_reader);
		return "invalid dimensions";
	}
	/* Check contents */
	if (check_tree(tsv_reader_data(&T_reader), n) != 0)
	{
		tsv_reader_deinit(&T_reader);
		return "invalid values";
	}

	/* Get data & clean up */
	*T = tsv_reader_data_release(&T_reader);
	xatexit(&free, *T);
	tsv_reader_deinit(&T_reader);

	return NULL;
}

static
void
compute_tree(double **T, const double *X, const double *G, size_t m, size_t n, const struct clustering *clustering)
{
	double *scratch;

	/* Allocate tree & scratch */
	*T = (double *)xalloca(n*3, sizeof(**T));
	scratch = (double *)xalloca((*clustering->scratch)(m, n), sizeof(*scratch));

	/* Cluster */
	(*clustering->cluster)(*T, scratch, X, G, m, n);

	/* Free scratch */
	xfree(scratch);
}

int
main(int argc, char **argv)
{
	struct opts opts;
	int file_flags;
	tsv_reader_t X_reader;
	const char *err_msg;
	const double *X;
	size_t m, n, n1, k;
	double *G, *T;
	size_t *L;

	/* Parse arguments */
	if (parse_args(&opts, argc, argv) != 0)
	{
		usage(argv[0]);
		return EXIT_FAILURE;
	}

	/* Set up input options */
	file_flags = 0;
	if (opts.headers)
		file_flags = TSV_READER_COLNAMES | TSV_READER_ROWNAMES;

	/* Read input file */
	tsv_reader_init(&X_reader, NULL, TSV_READER_TRANSP | file_flags);
	if (tsv_read(&X_reader, opts.src_expr_fn) != 0)
	{
		tsv_reader_deinit(&X_reader);

		fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.src_expr_fn, tsv_reader_error(&X_reader));
		return EXIT_FAILURE;
	}

	/* Get data & dimensions */
	X = tsv_reader_data(&X_reader);
	m = tsv_reader_rows(&X_reader);
	n = tsv_reader_cols(&X_reader);
	n1 = n > 0 ? n - 1 : 0;

	/* Zero outputs */
	G = NULL;
	T = NULL;

	/* Acquire gain factors */
	if (opts.src_gains_fn != NULL)
	{
		if ((err_msg = read_gains(&G, n, opts.src_gains_fn, file_flags)) != NULL)
		{
			tsv_reader_deinit(&X_reader);

			fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.src_gains_fn, err_msg);
			return EXIT_FAILURE;
		}
	}
	else
		if ((err_msg = default_gains(&G, n)) != NULL)
		{
			tsv_reader_deinit(&X_reader);

			fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.src_gains_fn, err_msg);
			return EXIT_FAILURE;
		}

	/* Build clustering tree */
	if (opts.src_tree_fn != NULL)
	{
		if ((err_msg = read_tree(&T, n, opts.src_tree_fn, TSV_READER_TRANSP | file_flags)) != NULL)
		{
			tsv_reader_deinit(&X_reader);

			fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.src_tree_fn, err_msg);
			return EXIT_FAILURE;
		}
	}
	else
		compute_tree(&T, X, G, m, n, opts.clustering);

	/* Dump tree */
	if (opts.dest_tree_fn != NULL)
		if ((err_msg = tsv_write(opts.dest_tree_fn, T, n1, 3, TSV_READER_TRANSP | file_flags, NULL, NULL)) != NULL)
			fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.dest_tree_fn, err_msg);

	/* Select model */
	k = opts.clusts;
	if (opts.clusts == 0)
	{
		size_t complexity;
 		complexity = (*opts.clustering->complexity)(m, n);
		k = prism_clust_select(T, m, n, complexity * (*opts.penalty)(complexity * m, n, opts.pvalue));
	}

	/* Label nodes */
	L = (size_t *)xalloca(2*n, sizeof(*L));
	cutree(L, T, k, n);

	/* Dump profiles */
	if (opts.dest_expr_fn != NULL)
	{
		double *Z;
		const void *colnames;

		/* Get space */
		Z = (double *)xalloca(m*k, sizeof(*Z));

		/* Compute profiles */
		(*opts.clustering->centroids)(Z, X, G, m, n, L, k);

		/* Dump profiles */
		colnames = tsv_names_putseq("", k);
		if (colnames == NULL)
		{
			err_msg = "no memory";
			goto err_write_Z;
		}
		if ((err_msg = tsv_write(opts.dest_expr_fn, Z, m, k, TSV_READER_TRANSP | file_flags, colnames, tsv_reader_rownames(&X_reader))) != NULL)
		{
err_write_Z:
			fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.dest_expr_fn, err_msg);
		}

		/* Free */
		xfree(Z);
		free((void *)colnames);
	}

	/* Dump labels */
	if (opts.dest_weights_fn != NULL)
	{
		double *W;
		size_t j, l;
		const void *colnames;

		/* Get space */
		W = (double *)xalloca(k*n, sizeof(*W));

		/* Fill in */
		for (j = 0; j < n; ++j)
			for (l = 0; l < k; ++l)
				W[l + j*k] = (double)(L[j] == l);

		/* Dump weights */
		colnames = tsv_names_putseq("", k);
		if (colnames == NULL)
		{
			err_msg = "no memory";
			goto err_write_W;
		}
		if ((err_msg = tsv_write(opts.dest_weights_fn, W, n, k, file_flags, colnames, tsv_reader_colnames(&X_reader))) != NULL)
		{
err_write_W:
			fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.dest_weights_fn, err_msg);
		}

		/* Free */
		xfree(W);
		free((void *)colnames);
	}


	/* Free data */
	tsv_reader_deinit(&X_reader);

	return EXIT_SUCCESS;
}
