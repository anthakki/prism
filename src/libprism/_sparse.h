#ifndef LIBPRISM_SPARSE_H_
#define LIBPRISM_SPARSE_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct prism_opt_sparse_ prism_opt_sparse_t;

/* Wrap a sparse matrix */
prism_opt_sparse_t prism_sparse(const double *x, const size_t *i, const size_t *p);
/* Wrap a dense matrix */
prism_opt_sparse_t prism_dense(const double *x);

/* Check if a matrix is sparse */
int prism_is_sparse(prism_opt_sparse_t mtx);
/* Check for consistency */
int prism_assert_sparse(prism_opt_sparse_t mtx, size_t m, size_t n);

struct prism_opt_sparse_ {
	double *_x;
	size_t *_i, *_p;
};

#define prism_is_sparse_(mtx) \
	((mtx)._p != NULL)

#define prism_assert_sparse_(mtx, m, n) \
	(prism_is_sparse_((mtx)) ? \
		(mtx)._p != NULL && ((mtx)._x != NULL || (mtx)._p[(n)] < 1) && ((mtx)._i != NULL || (mtx)._p[(n)] < 1) : \
		(mtx)._x != NULL || (m)*(n) < 1)

#define prism_is_sparse prism_is_sparse_
#define prism_assert_sparse prism_assert_sparse_

#ifdef __cplusplus
}
#endif

#endif /* LIBPRISM_SPARSE_H_ */
