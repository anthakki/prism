#ifndef LIBPRISM_CLUST_H_
#define LIBPRISM_CLUST_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Get dimensions for Poisson clustering */ 
size_t prism_clust_poi_scratch(size_t m, size_t n);
/* Hierarchical Poisson clustering */
void prism_clust_poi(double *Z, void *scratch, const double *X, const double *v, size_t m, size_t n);

/* Get dimensions for hierarchical Poisson clustering */
size_t prism_clust_spoi_scratch(size_t m, size_t n);
/* Hierarchical scaled Poisson clustering */
void prism_clust_spoi(double *Z, void *scratch, const double *X, const double *v, size_t m, size_t n);

/* Get dimensions for LSQ clustering */
size_t prism_clust_l2_scratch(size_t m, size_t n);
/* Hierarchical least-squares clustering */
void prism_clust_l2(double *Z, void *scratch, const double *X, const double *v, size_t m, size_t n);

/* Select model */
size_t prism_clust_select(const double *Z, size_t m, size_t n, double penalty);

/* Penalties for select methods */
double prism_clust_bic_penalty(size_t m, size_t n);
double prism_clust_aic_penalty(size_t m, size_t n);
double prism_clust_lr_penalty(size_t m, size_t n, double log_pv);

#ifdef __cplusplus
}
#endif

#endif /* LIBPRISM_CLUST_H_ */
