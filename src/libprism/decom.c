
#include "decom.h"
#include "solve.h"
#include <alloca.h>
#include <assert.h>
#include <string.h>

void
prism_decom_poi(double *t_Z, double *WX, const double *t_X, const double *t_Y, const double *WY, size_t m, size_t k, size_t r, size_t n, size_t max_iters, double min_delta)
{ prism_decom_poi_sparse(t_Z, WX, t_X, prism_dense(t_Y), prism_dense(WY), m, k, r, n, max_iters, min_delta); }

void
prism_decom_poi_sparse(double *t_Z, double *WX, const double *t_X, prism_opt_sparse_t t_Y, prism_opt_sparse_t WY, size_t m, size_t k, size_t r, size_t n, size_t max_iters, double min_delta)
{
	size_t iter, i, j, l, q;
	double delta, *S, *S0;

	assert(t_Z != NULL || m*k < 1);
	assert(WX != NULL || k*r < 1);
	assert(t_X != NULL || m*r < 1);
	assert(prism_assert_sparse(t_Y, n, m));
	assert(prism_assert_sparse(WY, k, n));

	/* Get scratch */
	S = (double *)alloca(3*k * sizeof(*S));
	S0 = (double *)alloca(3*k * sizeof(*S));

	/* Set up initial solution */
	for (i = 0; i < m; ++i)
		for (l = 0; l < k; ++l)
			t_Z[i*k + l] = 1.;
	for (j = 0; j < r; ++j)
		for (l = 0; l < k; ++l)
			WX[l + j*k] = 1.;

	/* Precompute basal state update */
	{
		/* Zero accumulators */
		prism_solve_poi_init(S0, k);

		/* Add data from reference */
		if (!prism_is_sparse(t_Y))
		{
			if (!prism_is_sparse(WY))
				for (j = 0; j < n; ++j)
					prism_solve_poi_upd_a(S0, &WY._x[j*k], k);
			else
				for (j = 0; j < n; ++j)
					prism_solve_poi_upd_a_sparse(S0, &WY._x[WY._p[j]], &WY._i[WY._p[j]], WY._p[j+1]-WY._p[j], k);
		}
		else
			if (!prism_is_sparse(WY))
				for (j = 0; j < n; ++j)
					prism_solve_poi_upd_a(S0, &WY._x[j*k], k);
			else
				for (j = 0; j < n; ++j)
					prism_solve_poi_upd_a_sparse(S0, &WY._x[WY._p[j]], &WY._i[WY._p[j]], WY._p[j+1]-WY._p[j], k);
	}

	/* Presolve */
	for (i = 0; i < m; ++i)
	{
		/* Zero accumulators */
		memcpy(S, S0, 2*k * sizeof(*S));

		/* Add data from reference */
		if (!prism_is_sparse(t_Y))
		{
			if (!prism_is_sparse(WY))
				for (j = 0; j < n; ++j)
					prism_solve_poi_upd_b(S, &t_Z[i*k], &WY._x[j*k], t_Y._x[i*n + j], k);
			else
				for (j = 0; j < n; ++j)
					prism_solve_poi_upd_b_sparse(S, &t_Z[i*k], &WY._x[WY._p[j]], &WY._i[WY._p[j]], WY._p[j+1]-WY._p[j], t_Y._x[i*n + j], k);
		}
		else
			if (!prism_is_sparse(WY))
				for (q = t_Y._p[i]; q < t_Y._p[i+1]; ++q)
					prism_solve_poi_upd_b(S, &t_Z[i*k], &WY._x[t_Y._i[q]*k], t_Y._x[q], k);
			else
				for (q = t_Y._p[i]; q < t_Y._p[i+1]; ++q)
					prism_solve_poi_upd_b_sparse(S, &t_Z[i*k], &WY._x[WY._p[t_Y._i[q]]], &WY._i[WY._p[t_Y._i[q]]], WY._p[t_Y._i[q]+1]-WY._p[t_Y._i[q]], t_Y._x[q], k);

		/* Solve */
		prism_solve_poi_div(&t_Z[i*k], S, k);
	}

	/* Loop */
	for (iter = 0; iter < max_iters; ++iter)
	{
		/* Zero gradient */
		delta = 0.;

		/* Update weights */
		for (j = 0; j < r; ++j)
		{
			/* Zero accumulators */
			prism_solve_poi_init(S, k);

			/* Add data from each gene */
			for (i = 0; i < m; ++i)
				prism_solve_poi_upd(S, &WX[j*k], &t_Z[i*k], t_X[i*r + j], k);

			/* Solve */
			delta += prism_solve_poi_div(&WX[j*k], S, k);
		}

		/* Update profiles */
		for (i = 0; i < m; ++i)
		{
			/* Zero accumulators */
			memcpy(S, S0, 2*k * sizeof(*S));

			/* Add data from bulk */
			for (j = 0; j < r; ++j)
				prism_solve_poi_upd(S, &t_Z[i*k], &WX[j*k], t_X[i*r + j], k);

			/* Add data from reference */
			if (!prism_is_sparse(t_Y))
			{
				if (!prism_is_sparse(WY))
					for (j = 0; j < n; ++j)
						prism_solve_poi_upd_b(S, &t_Z[i*k], &WY._x[j*k], t_Y._x[i*n + j], k);
				else
					for (j = 0; j < n; ++j)
						prism_solve_poi_upd_b_sparse(S, &t_Z[i*k], &WY._x[WY._p[j]], &WY._i[WY._p[j]], WY._p[j+1]-WY._p[j], t_Y._x[i*n + j], k);
			}
			else
				if (!prism_is_sparse(WY))
					for (q = t_Y._p[i]; q < t_Y._p[i+1]; ++q)
						prism_solve_poi_upd_b(S, &t_Z[i*k], &WY._x[t_Y._i[q]*k], t_Y._x[q], k);
				else
					for (q = t_Y._p[i]; q < t_Y._p[i+1]; ++q)
						prism_solve_poi_upd_b_sparse(S, &t_Z[i*k], &WY._x[WY._p[t_Y._i[q]]], &WY._i[WY._p[t_Y._i[q]]], WY._p[t_Y._i[q]+1]-WY._p[t_Y._i[q]], t_Y._x[q], k);

			/* Solve */
			delta += prism_solve_poi_div(&t_Z[i*k], S, k);
		}

		/* Done? */
		if (!(delta > min_delta))
			break;
	}
}

void
prism_decom_spoi(double *t_Z, double *t_T, double *WX, const double *t_X, const double *t_Y, const double *WY, size_t m, size_t k, size_t r, size_t n, size_t max_iters, double min_delta)
{ prism_decom_spoi_sparse(t_Z, t_T, WX, t_X, prism_dense(t_Y), prism_dense(WY), m, k, r, n, max_iters, min_delta); }

void
prism_decom_spoi_sparse(double *t_Z, double *t_T, double *WX, const double *t_X, prism_opt_sparse_t t_Y, prism_opt_sparse_t WY, size_t m, size_t k, size_t r, size_t n, size_t max_iters, double min_delta)
{
	size_t iter, i, j, l, q;
	double delta, *S, *S0;

	assert(t_Z != NULL || m*k < 1);
	assert(t_T != NULL || m*k < 1);
	assert(WX != NULL || k*r < 1);
	assert(t_X != NULL || m*r < 1);
	assert(prism_assert_sparse(t_Y, n, m));
	assert(prism_assert_sparse(WY, k, n));

	/* Get scratch */
	S = (double *)alloca(5*k * sizeof(*S));
	S0 = (double *)alloca(5*k * sizeof(*S0));

	/* Set up initial solution */
	for (i = 0; i < m; ++i)
		for (l = 0; l < k; ++l)
			t_Z[i*k + l] = 1.;
	for (i = 0; i < m; ++i)
		for (l = 0; l < k; ++l)
			t_T[i*k + l] = 1.;
	for (j = 0; j < r; ++j)
		for (l = 0; l < k; ++l)
			WX[l + j*k] = 1.;

	/* Precompute basal state update */
	{
		/* Zero accumulators */
		prism_solve_spoi_init(S0, k);

		/* Add data from reference */
		if (!prism_is_sparse(t_Y))
		{
			if (!prism_is_sparse(WY))
				for (j = 0; j < n; ++j)
					prism_solve_spoi_upd_a(S0, &WY._x[j*k], k);
			else
				for (j = 0; j < n; ++j)
					prism_solve_spoi_upd_a_sparse(S0, &WY._x[WY._p[j]], &WY._i[WY._p[j]], WY._p[j+1]-WY._p[j], k);
		}
		else
			if (!prism_is_sparse(WY))
				for (j = 0; j < n; ++j)
					prism_solve_spoi_upd_a(S0, &WY._x[j*k], k);
			else
				for (j = 0; j < n; ++j)
					prism_solve_spoi_upd_a_sparse(S0, &WY._x[WY._p[j]], &WY._i[WY._p[j]], WY._p[j+1]-WY._p[j], k);
	}

	/* Presolve */
	for (i = 0; i < m; ++i)
	{
		/* Reset accumulators */
		memcpy(S, S0, 4*k * sizeof(*S));

		/* Add data from reference */
		if (!prism_is_sparse(t_Y))
		{
			if (!prism_is_sparse(WY))
				for (j = 0; j < n; ++j)
					prism_solve_spoi_upd_b(S, &t_Z[i*k], &t_T[i*k], &WY._x[j*k], t_Y._x[i*n + j], k);
			else
				for (j = 0; j < n; ++j)
					prism_solve_spoi_upd_b_sparse(S, &t_Z[i*k], &t_T[i*k], &WY._x[WY._p[j]], &WY._i[WY._p[j]], WY._p[j+1]-WY._p[j], t_Y._x[i*n + j], k);
		}
		else
			if (!prism_is_sparse(WY))
				for (q = t_Y._p[i]; q < t_Y._p[i+1]; ++q)
					prism_solve_spoi_upd_b(S, &t_Z[i*k], &t_T[i*k], &WY._x[t_Y._i[q]*k], t_Y._x[q], k);
			else
				for (q = t_Y._p[i]; q < t_Y._p[i+1]; ++q)
					prism_solve_spoi_upd_b_sparse(S, &t_Z[i*k], &t_T[i*k], &WY._x[WY._p[t_Y._i[q]]], &WY._i[WY._p[t_Y._i[q]]], WY._p[t_Y._i[q]+1]-WY._p[t_Y._i[q]], t_Y._x[q], k);

		/* Solve */
		prism_solve_spoi_div(&t_Z[i*k], &t_T[i*k], S, k);
	}

	/* Loop */
	for (iter = 0; iter < max_iters; ++iter)
	{
		/* Zero gradient */
		delta = 0.;

		/* Update weights */
		for (j = 0; j < r; ++j)
		{
			/* Zero accumulators */
			prism_solve_poi_init(S, k);

			/* Add data from each gene */
			for (i = 0; i < m; ++i)
				prism_solve_wpoi_upd(S, &WX[j*k], &t_T[i*k], &t_Z[i*k], t_X[i*r + j], k);

			/* Solve */
			delta += prism_solve_poi_div(&WX[j*k], S, k);
		}

		/* Update profiles */
		for (i = 0; i < m; ++i)
		{
			/* Reset accumulators */
			memcpy(S, S0, 4*k * sizeof(*S));

			/* Add data from bulk */
			for (j = 0; j < r; ++j)
				prism_solve_spoi_upd(S, &t_Z[i*k], &t_T[i*k], &WX[j*k], t_X[i*r + j], k);

			/* Add data from reference */
			if (!prism_is_sparse(t_Y))
			{
				if (!prism_is_sparse(WY))
					for (j = 0; j < n; ++j)
						prism_solve_spoi_upd_b(S, &t_Z[i*k], &t_T[i*k], &WY._x[j*k], t_Y._x[i*n + j], k);
				else
					for (j = 0; j < n; ++j)
						prism_solve_spoi_upd_b_sparse(S, &t_Z[i*k], &t_T[i*k], &WY._x[WY._p[j]], &WY._i[WY._p[j]], WY._p[j+1]-WY._p[j], t_Y._x[i*n + j], k);
			}
			else
				if (!prism_is_sparse(WY))
					for (q = t_Y._p[i]; q < t_Y._p[i+1]; ++q)
						prism_solve_spoi_upd_b(S, &t_Z[i*k], &t_T[i*k], &WY._x[t_Y._i[q]*k], t_Y._x[q], k);
				else
					for (q = t_Y._p[i]; q < t_Y._p[i+1]; ++q)
						prism_solve_spoi_upd_b_sparse(S, &t_Z[i*k], &t_T[i*k], &WY._x[WY._p[t_Y._i[q]]], &WY._i[WY._p[t_Y._i[q]]], WY._p[t_Y._i[q]+1]-WY._p[t_Y._i[q]], t_Y._x[q], k);

			/* Solve */
			delta += prism_solve_spoi_div(&t_Z[i*k], &t_T[i*k], S, k);
		}

		/* Done? */
		if (!(delta > min_delta))
			break;
	}
}

void
prism_decom_l2(double *t_Z, double *WX, const double *t_X, const double *t_Y, const double *WY, size_t m, size_t k, size_t r, size_t n, size_t max_iters, double min_delta)
{ prism_decom_l2_sparse(t_Z, WX, t_X, prism_dense(t_Y), prism_dense(WY), m, k, r, n, max_iters, min_delta); }

void
prism_decom_l2_sparse(double *t_Z, double *WX, const double *t_X, prism_opt_sparse_t t_Y, prism_opt_sparse_t WY, size_t m, size_t k, size_t r, size_t n, size_t max_iters, double min_delta)
{
	size_t iter, i, j, l, q;
	double delta, *S;

	assert(t_Z != NULL || m*k < 1);
	assert(WX != NULL || k*r < 1);
	assert(t_X != NULL || m*r < 1);
	assert(prism_assert_sparse(t_Y, n, m));
	assert(prism_assert_sparse(WY, k, n));

	/* Get scratch */
	S = (double *)alloca(3*k * sizeof(*S));

	/* Set up initial solution */
	for (i = 0; i < m; ++i)
		for (l = 0; l < k; ++l)
			t_Z[i*k + l] = 1.;
	for (j = 0; j < r; ++j)
		for (l = 0; l < k; ++l)
			WX[l + j*k] = 1.;

	/* Presolve */
	for (i = 0; i < m; ++i)
	{
		/* Zero accumulators */
		prism_solve_l2_init(S, k);

		/* Add data from reference */
		if (!prism_is_sparse(t_Y))
		{
			if (!prism_is_sparse(WY))
				for (j = 0; j < n; ++j)
					prism_solve_l2_upd(S, &t_Z[i*k], &WY._x[j*k], t_Y._x[i*n + j], k);
			else
				for (j = 0; j < n; ++j)
				{
					prism_solve_l2_upd_a_sparse(S, &t_Z[i*k], &WY._x[WY._p[j]], &WY._i[WY._p[j]], WY._p[j+1]-WY._p[j], k);
					prism_solve_l2_upd_b_sparse(S, &t_Z[i*k], &WY._x[WY._p[j]], &WY._i[WY._p[j]], WY._p[j+1]-WY._p[j], t_Y._x[i*n + j], k);
				}
		}
		else
			if (!prism_is_sparse(WY))
			{
				for (j = 0; j < n; ++j)
					prism_solve_l2_upd_a(S, &t_Z[i*k], &WY._x[j*k], k);
				for (q = t_Y._p[i]; q < t_Y._p[i+1]; ++q)
					prism_solve_l2_upd_b(S, &t_Z[i*k], &WY._x[t_Y._i[q]*k], t_Y._x[q], k);
			}
			else
			{
				for (j = 0; j < n; ++j)
					prism_solve_l2_upd_a_sparse(S, &t_Z[i*k], &WY._x[WY._p[j]], &WY._i[WY._p[j]], WY._p[j+1]-WY._p[j], k);
				for (q = t_Y._p[i]; q < t_Y._p[i+1]; ++q)
					prism_solve_l2_upd_b_sparse(S, &t_Z[i*k], &WY._x[WY._p[t_Y._i[q]]], &WY._i[WY._p[t_Y._i[q]]], WY._p[t_Y._i[q]+1]-WY._p[t_Y._i[q]], t_Y._x[q], k);
			}

		/* Solve */
		prism_solve_l2_div(&t_Z[i*k], S, k);
	}

	/* Loop */
	for (iter = 0; iter < max_iters; ++iter)
	{
		/* Zero gradient */
		delta = 0.;

		/* Update weights */
		for (j = 0; j < r; ++j)
		{
			/* Zero accumulators */
			prism_solve_l2_init(S, k);

			/* Add data from each gene */
			for (i = 0; i < m; ++i)
				prism_solve_l2_upd(S, &WX[j*k], &t_Z[i*k], t_X[i*r + j], k);

			/* Solve */
			delta += prism_solve_l2_div(&WX[j*k], S, k);
		}

		/* Update profiles */
		for (i = 0; i < m; ++i)
		{
			/* Zero accumulators */
			prism_solve_l2_init(S, k);

			/* Add data from bulk */
			for (j = 0; j < r; ++j)
				prism_solve_l2_upd(S, &t_Z[i*k], &WX[j*k], t_X[i*r + j], k);

			/* Add data from reference */
			if (!prism_is_sparse(t_Y))
			{
				if (!prism_is_sparse(WY))
					for (j = 0; j < n; ++j)
						prism_solve_l2_upd(S, &t_Z[i*k], &WY._x[j*k], t_Y._x[i*n + j], k);
				else
					for (j = 0; j < n; ++j)
					{
						prism_solve_l2_upd_a_sparse(S, &t_Z[i*k], &WY._x[WY._p[j]], &WY._i[WY._p[j]], WY._p[j+1]-WY._p[j], k);
						prism_solve_l2_upd_b_sparse(S, &t_Z[i*k], &WY._x[WY._p[j]], &WY._i[WY._p[j]], WY._p[j+1]-WY._p[j], t_Y._x[i*n + j], k);
					}
			}
			else
				if (!prism_is_sparse(WY))
				{
					for (j = 0; j < n; ++j)
						prism_solve_l2_upd_a(S, &t_Z[i*k], &WY._x[j*k], k);
					for (q = t_Y._p[i]; q < t_Y._p[i+1]; ++q)
						prism_solve_l2_upd_b(S, &t_Z[i*k], &WY._x[t_Y._i[q]*k], t_Y._x[q], k);
				}
				else
				{
					for (j = 0; j < n; ++j)
						prism_solve_l2_upd_a_sparse(S, &t_Z[i*k], &WY._x[WY._p[j]], &WY._i[WY._p[j]], WY._p[j+1]-WY._p[j], k);
					for (q = t_Y._p[i]; q < t_Y._p[i+1]; ++q)
						prism_solve_l2_upd_b_sparse(S, &t_Z[i*k], &WY._x[WY._p[t_Y._i[q]]], &WY._i[WY._p[t_Y._i[q]]], WY._p[t_Y._i[q]+1]-WY._p[t_Y._i[q]], t_Y._x[q], k);
				}

			/* Solve */
			delta += prism_solve_l2_div(&t_Z[i*k], S, k);
		}

		/* Done? */
		if (!(delta > min_delta))
			break;
	}
}
