#ifndef LIBPRISM_SOLVE_H_
#define LIBPRISM_SOLVE_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Invert a linear Poisson model */ 
void prism_solve_poi(double *x, const double *t_A, const double *b, size_t m, size_t n);
/* Manual update step for above, for internal use */
void prism_solve_poi_init(void *scratch, size_t n);
void prism_solve_poi_upd(void *scratch, const double *x, const double *a, double b, size_t n);
void prism_solve_poi_upd_a(void *scratch, const double *a, size_t n);
void prism_solve_poi_upd_a_sparse(void *scratch, const double *a, const size_t *a_i, size_t a_nnz, size_t n);
void prism_solve_poi_upd_b(void *scratch, const double *x, const double *a, double b, size_t n);
void prism_solve_poi_upd_b_sparse(void *scratch, const double *x, const double *a, const size_t *a_i, size_t a_nnz, double b, size_t n);
double prism_solve_poi_div(double *x, void *scratch, size_t n);

/* Model inversion step for unknown precision scaled Poisson, for internal use */
void prism_solve_spoi_init(void *scratch, size_t n);
void prism_solve_spoi_upd(void *scratch, const double *x, const double *t, const double *a, double b, size_t n);
void prism_solve_spoi_upd_a(void *scratch, const double *a, size_t n);
void prism_solve_spoi_upd_a_sparse(void *scratch, const double *a, const size_t *a_i, size_t a_nnz, size_t n);
void prism_solve_spoi_upd_b(void *scratch, const double *x, const double *t, const double *a, double b, size_t n);
void prism_solve_spoi_upd_b_sparse(void *scratch, const double *x, const double *t, const double *a, const size_t *a_i, size_t a_nzz, double b, size_t n);
double prism_solve_spoi_div(double *x, double *t, void *scratch, size_t n);
/* Model inversion step for known scaled Poisson, for internal use */
void prism_solve_wpoi_upd(void *scratch, const double *x, const double *t, const double *a, double b, size_t n);

/* Invert a least-squares model */
void prism_solve_l2(double *x, const double *t_A, const double *b, size_t m, size_t n);
/* Internal use */
void prism_solve_l2_init(void *scratch, size_t n);
void prism_solve_l2_upd(void *scratch, const double *x, const double *a, double b, size_t n);
void prism_solve_l2_upd_a(void *scratch, const double *x, const double *a, size_t n);
void prism_solve_l2_upd_a_sparse(void *scratch, const double *x, const double *a, const size_t *a_i, size_t a_nnz, size_t n);
void prism_solve_l2_upd_b(void *scratch, const double *x, const double *a, double b, size_t n);
void prism_solve_l2_upd_b_sparse(void *scratch, const double *x, const double *a, const size_t *a_i, size_t a_nnz, double b, size_t n);
double prism_solve_l2_div(double *x, void *scratch, size_t n);

/* Matrix form helpers for R, MATLAB */
void prism_solve_poi_mat(double *X, const double *t_A, const double *B, size_t m, size_t n, size_t r); 
void prism_solve_poi_mat_upd(double *X, const double *t_A, const double *B, size_t m, size_t n, size_t r);
void prism_solve_l2_mat(double *X, const double *t_A, const double *B, size_t m, size_t n, size_t r);
void prism_solve_l2_mat_upd(double *X, const double *t_A, const double *B, size_t m, size_t n, size_t r);

/* Apply unity constraint on Poisson solution */
void prism_solve_poi_con_mat(double *X, const double *t_A, size_t m, size_t n, size_t r);
/* Apply unity constraint on least-squares solution */
void prism_solve_l2_con_mat(double *X, const double *t_A, size_t m, size_t n, size_t r);

#ifdef __cplusplus
}
#endif

#endif /* LIBPRISM_SOLVE_H_ */
