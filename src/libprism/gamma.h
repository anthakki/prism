#ifndef SRC_LIBPRISM_GAMMA_H_
#define SRC_LIBPRISM_GAMMA_H_

#ifdef __cplusplus
extern "C" {
#endif

/* Logarithm of the gamma function */
double gammaln(double a);

/* Lower regularized incomplete gamma function */
double gamma_p(double x, double a);
double gamma_p_log(double x, double a);
/* Upper */
double gamma_q(double x, double a);
double gamma_q_log(double x, double a);

#ifdef __cplusplus
}
#endif

#endif /* SRC_LIBPRISM_GAMMA_H_ */
