
#include "solve.h"
#include <alloca.h>
#include <assert.h>
#include <math.h>

#define MIN_DELTA (1e-10)

static
double
safe_div(double x, double y)
{
	/* Compute x/y with 0./0. -> 0. for x >= 0. */
	if (x > 0. && y > 0.)
		return x / y;
	else
		return 0.;
}

static
void
zero_d(double *x, size_t n)
{
	size_t i;

	assert(x != NULL || n < 1);

#pragma omp simd
	for (i = 0; i < n; ++i)
		x[i] = 0.;
}

static
double
update_ratio(double *x, double x_num, double x_den)
	/* Solves problems with gradient num/x - den = 0,
	 * such that the new solution is x = num/den and and the gradient
	 * bound is ( num - den * x )^2 / ( den * x )
	 */
{
	double delta;

	assert(x != NULL);

	/* Handle all zero row/column */
	if (!(x_num > 0.))
	{
		*x = 0.;
		return 0.;
	}

	/* Compute gradient bound */
	delta = ( x_num - x_den * *x ) * ( x_num - x_den * *x ) / ( x_den * *x );
	/* Update x */
	*x = x_num / x_den;

	return delta;
}

static
double
prism_solve_poi_step(double *x, const double *t_A, const double *b, size_t m, size_t n)
{
	double *S;
	size_t i;

	assert(x != NULL || n < 1);
	assert(t_A != NULL || m*n < 1);
	assert(b != NULL || m < 1);

	/* Allocate scratch */
	S = (double *)alloca(3*n * sizeof(*S));

	/* Zero accumulators */
	prism_solve_poi_init(S, n);

	/* Loop through */
	for (i = 0; i < m; ++i)
		prism_solve_poi_upd(S, x, &t_A[i*n], b[i], n);

	/* Update estimate & compute gradient bound */
	return prism_solve_poi_div(x, S, n);
}

void
prism_solve_poi(double *x, const double *t_A, const double *b, size_t m, size_t n)
{
	double *S;
	size_t j;

	assert(x != NULL || n < 1);
	assert(t_A != NULL || m*n < 1);
	assert(b != NULL || m < 1);

	/* Allocate */
	S = (double *)alloca(3*n * sizeof(*S));

	/* Set up initial solution */
#pragma omp simd
	for (j = 0; j < n; ++j)
		x[j] = 1.;

	/* Loop */
	while (prism_solve_poi_step(x, t_A, b, m, n) > MIN_DELTA)
	{}
}

void
prism_solve_poi_init(void *scratch, size_t n)
{
	double *num, *den;

	assert(scratch != NULL || n < 1);

	/* Get data */
	num = (double *)scratch;
	den = &num[n];

	/* Zero accumulators */
	zero_d(num, n);
	zero_d(den, n);
}

void
prism_solve_poi_upd(void *scratch, const double *x, const double *a, double b, size_t n)
{
	double *num, *den, *lams, lam, coe;
	size_t j;

	assert(scratch != NULL || n < 1);
	assert(x != NULL || n < 1);
	assert(a != NULL || n < 1);

	/* Get data */
	num = (double *)scratch;
	den = &num[n];
	lams = &num[2*n];

	/* Predict */
	lam = 0.;
#pragma omp simd reduction(+: lam)
	for (j = 0; j < n; ++j)
	{
		/* Predict */
		lams[j] = a[j] * x[j];
		lam += lams[j];

		/* Update denominator */
		den[j] += a[j];
	}

	/* Get scaling coefficient */
	coe = safe_div(b, lam);

	/* Update numerator */
#pragma omp simd
	for (j = 0; j < n; ++j)
		num[j] += coe * lams[j];
}

void
prism_solve_poi_upd_a(void *scratch, const double *a, size_t n)
{
	double *num, *den;
	size_t j;

	assert(scratch != NULL || n < 1);
	assert(a != NULL || n < 1);

	/* Get data */
	num = (double *)scratch;
	den = &num[n];

	/* Update denominator */
#pragma omp simd
	for (j = 0; j < n; ++j)
		den[j] += a[j];
}

void
prism_solve_poi_upd_a_sparse(void *scratch, const double *a, const size_t *a_i, size_t a_nnz, size_t n)
{
	double *num, *den;
	size_t q;

	assert(scratch != NULL || n < 1);
	assert(a != NULL || a_nnz < 1);
	assert(a_i != NULL || a_nnz < 1);

	/* Get data */
	num = (double *)scratch;
	den = &num[n];

	/* Update denominator */
#pragma omp simd
	for (q = 0; q < a_nnz; ++q)
		den[a_i[q]] += a[q];
}

void
prism_solve_poi_upd_b(void *scratch, const double *x, const double *a, double b, size_t n)
{
	double *num, *lams, lam, coe;
	size_t j;

	assert(scratch != NULL || n < 1);
	assert(x != NULL || n < 1);
	assert(a != NULL || n < 1);

	/* Get data */
	num = (double *)scratch;
	lams = &num[2*n];

	/* Predict */
	lam = 0.;
#pragma omp simd reduction(+: lam)
	for (j = 0; j < n; ++j)
	{
		/* Predict */
		lams[j] = a[j] * x[j];
		lam += lams[j];
	}

	/* Get scaling coefficient */
	coe = safe_div(b, lam);

	/* Update numerator */
#pragma omp simd
	for (j = 0; j < n; ++j)
		num[j] += coe * lams[j];
}

void
prism_solve_poi_upd_b_sparse(void *scratch, const double *x, const double *a, const size_t *a_i, size_t a_nnz, double b, size_t n)
{
	double *num, *lams, lam, coe;
	size_t q;

	assert(scratch != NULL || n < 1);
	assert(x != NULL || n < 1);
	assert(a != NULL || a_nnz < 1);
	assert(a_i != NULL || a_nnz < 1);

	/* Get data */
	num = (double *)scratch;
	lams = &num[2*n];

	/* Predict */
	lam = 0.;
#pragma omp simd reduction(+: lam)
	for (q = 0; q < a_nnz; ++q)
	{
		/* Predict */
		lams[q] = a[q] * x[a_i[q]];
		lam += lams[q];
	}

	/* Get scaling coefficient */
	coe = safe_div(b, lam);

	/* Update numerator */
#pragma omp simd
	for (q = 0; q < a_nnz; ++q)
		num[a_i[q]] += coe * lams[q];
}

double
prism_solve_poi_div(double *x, void *scratch, size_t n)
{
	const double *num, *den;
	double delta;
	size_t j;

	assert(x != NULL || n < 1);
	assert(scratch != NULL || n < 1);

	/* Get data */
	num = (double *)scratch;
	den = &num[n];

	/* Update x & compute gradient bound */
	delta = 0.;
	for (j = 0; j < n; ++j)
		delta += update_ratio(&x[j], num[j], den[j]);

	return delta;
}

void
prism_solve_spoi_init(void *scratch, size_t n)
{
	double *x_num, *x_den, *t_num, *t_den;

	assert(scratch != NULL || n < 1);

	/* Get data */
	x_num = (double *)scratch;
	x_den = &x_num[n];
	t_num = &x_den[n];
	t_den = &t_num[n];

	/* Zero accumulators */
	zero_d(x_num, n);
	zero_d(x_den, n);
	zero_d(t_num, n);
	zero_d(t_den, n);
}

void
prism_solve_spoi_upd(void *scratch, const double *x, const double *t, const double *a, double b, size_t n)
{
	double *x_num, *x_den, *t_num, *t_den, *lams, *t_den_bas, *t_num_bas, lam, lam_var, coe, coe_var_e, coe_var_v;
	size_t j;

	assert(scratch != NULL || n < 1);
	assert(x != NULL || n < 1);
	assert(t != NULL || n < 1);
	assert(a != NULL || n < 1);

	/* Get data */
	x_num = (double *)scratch;
	x_den = &x_num[n];
	t_num = &x_den[n];
	t_den = &t_num[n];
	lams = &t_den[n];
	t_den_bas = &t_den[0];
	t_num_bas = &t_den[1];

	/* Predict */
	lam = lam_var = 0.;
#pragma omp simd reduction(+: lam) reduction(+: lam_var)
	for (j = 0; j < n; ++j)
	{
		/* Predict mean */
		lams[j] = a[j] * x[j];
		lam += lams[j];

		/* Predict variance */
		lam_var += safe_div(lams[j], t[j]);

		/* Update denominator */
		x_den[j] += a[j];
	}

	/* Get scaling coefficient */
	coe = safe_div(b, lam);

	/* Update numerator */
#pragma omp simd
	for (j = 0; j < n; ++j)
		x_num[j] += coe * lams[j];

	/* Get scaling coefficients for variance */ 
	coe_var_e = safe_div(coe * (b - lam), lam);
	coe_var_v = safe_div(coe * lam_var  , lam * lam);

	/* Update t */
	{
		*t_num_bas += coe_var_e * 0. + coe_var_v * (lam - 0.) + 1./12.;
		*t_den_bas += 1.;
	}
#pragma omp simd
	for (j = 0; j < n; ++j)
		t_num[j] += coe_var_e * lams[j] + coe_var_v * (0. - lams[j]) + 1./12.;
}

void
prism_solve_spoi_upd_a(void *scratch, const double *a, size_t n)
{
	double *x_num, *x_den, *t_num, *t_den, *t_den_bas, *t_num_bas;
	size_t j;

	assert(scratch != NULL || n < 1);
	assert(a != NULL || n < 1);

	/* Get data */
	x_num = (double *)scratch;
	x_den = &x_num[n];
	t_num = &x_den[n];
	t_den = &t_num[n];
	t_den_bas = &t_den[0];
	t_num_bas = &t_den[1];

	/* Update denominator */
#pragma omp simd
	for (j = 0; j < n; ++j)
		x_den[j] += a[j];

	/* Update t */
	{
		*t_num_bas += 1./12.;
		*t_den_bas += 1.;
	}
}

void
prism_solve_spoi_upd_a_sparse(void *scratch, const double *a, const size_t *a_i, size_t a_nnz, size_t n)
{
	double *x_num, *x_den, *t_num, *t_den, *t_den_bas, *t_num_bas;
	size_t q;

	assert(scratch != NULL || n < 1);
	assert(a != NULL || a_nnz < 1);
	assert(a_i != NULL || a_nnz < 1);

	/* Get data */
	x_num = (double *)scratch;
	x_den = &x_num[n];
	t_num = &x_den[n];
	t_den = &t_num[n];
	t_den_bas = &t_den[0];
	t_num_bas = &t_den[1];

	/* Update denominator */
#pragma omp simd
	for (q = 0; q < a_nnz; ++q)
		x_den[a_i[q]] += a[q];

	/* Update t */
	{
		*t_num_bas += 1./12.;
		*t_den_bas += 1.;
	}
}

void
prism_solve_spoi_upd_b(void *scratch, const double *x, const double *t, const double *a, double b, size_t n)
{
	double *x_num, *x_den, *t_num, *t_den, *lams, *t_den_bas, *t_num_bas, lam, lam_var, coe, coe_var_e, coe_var_v;
	size_t j;

	assert(scratch != NULL || n < 1);
	assert(x != NULL || n < 1);
	assert(t != NULL || n < 1);
	assert(a != NULL || n < 1);

	/* Get data */
	x_num = (double *)scratch;
	x_den = &x_num[n];
	t_num = &x_den[n];
	t_den = &t_num[n];
	lams = &t_den[n];
	t_den_bas = &t_den[0];
	t_num_bas = &t_den[1];

	/* Predict */
	lam = lam_var = 0.;
#pragma omp simd reduction(+: lam) reduction(+: lam_var)
	for (j = 0; j < n; ++j)
	{
		/* Predict mean */
		lams[j] = a[j] * x[j];
		lam += lams[j];

		/* Predict variance */
		lam_var += safe_div(lams[j], t[j]);
	}

	/* Get scaling coefficient */
	coe = safe_div(b, lam);

	/* Update numerator */
#pragma omp simd
	for (j = 0; j < n; ++j)
		x_num[j] += coe * lams[j];

	/* Get scaling coefficients for variance */ 
	coe_var_e = safe_div(coe * (b - lam), lam);
	coe_var_v = safe_div(coe * lam_var  , lam * lam);

	/* Update t */
	{
		*t_num_bas += coe_var_e * 0. + coe_var_v * (lam - 0.);
		*t_den_bas += 0.;
	}
#pragma omp simd
	for (j = 0; j < n; ++j)
		t_num[j] += coe_var_e * lams[j] + coe_var_v * (0. - lams[j]);
}

void
prism_solve_spoi_upd_b_sparse(void *scratch, const double *x, const double *t, const double *a, const size_t *a_i, size_t a_nnz, double b, size_t n)
{
	double *x_num, *x_den, *t_num, *t_den, *lams, *t_den_bas, *t_num_bas, lam, lam_var, coe, coe_var_e, coe_var_v;
	size_t q;

	assert(scratch != NULL || n < 1);
	assert(x != NULL || n < 1);
	assert(t != NULL || n < 1);
	assert(a != NULL || a_nnz < 1);
	assert(a_i != NULL || a_nnz < 1);

	/* Get data */
	x_num = (double *)scratch;
	x_den = &x_num[n];
	t_num = &x_den[n];
	t_den = &t_num[n];
	lams = &t_den[n];
	t_den_bas = &t_den[0];
	t_num_bas = &t_den[1];

	/* Predict */
	lam = lam_var = 0.;
#pragma omp simd reduction(+: lam) reduction(+: lam_var)
	for (q = 0; q < a_nnz; ++q)
	{
		/* Predict mean */
		lams[q] = a[q] * x[a_i[q]];
		lam += lams[q];

		/* Predict variance */
		lam_var += safe_div(lams[q], t[a_i[q]]);
	}

	/* Get scaling coefficient */
	coe = safe_div(b, lam);

	/* Update numerator */
#pragma omp simd
	for (q = 0; q < a_nnz; ++q)
		x_num[a_i[q]] += coe * lams[q];

	/* Get scaling coefficients for variance */ 
	coe_var_e = safe_div(coe * (b - lam), lam);
	coe_var_v = safe_div(coe * lam_var  , lam * lam);

	/* Update t */
	{
		*t_num_bas += coe_var_e * 0. + coe_var_v * (lam - 0.);
		*t_den_bas += 0.;
	}
#pragma omp simd
	for (q = 0; q < a_nnz; ++q)
		t_num[a_i[q]] += coe_var_e * lams[q] + coe_var_v * (0. - lams[q]);
}

double
prism_solve_spoi_div(double *x, double *t, void *scratch, size_t n)
{
	const double *x_num, *x_den, *t_num, *t_den, *t_den_bas, *t_num_bas;
	double delta;
	size_t j;

	assert(x != NULL || n < 1);
	assert(t != NULL || n < 1);
	assert(scratch != NULL || n < 1);

	/* Get data */
	x_num = (double *)scratch;
	x_den = &x_num[n];
	t_num = &x_den[n];
	t_den = &t_num[n];
	t_den_bas = &t_den[0];
	t_num_bas = &t_den[1];

	/* Update x & compute gradient bound */
	delta = 0.;
	for (j = 0; j < n; ++j)
		delta += update_ratio(&x[j], x_num[j], x_den[j]);

	/* Update t */
	for (j = 0; j < n; ++j)
		update_ratio(&t[j], *t_den_bas + t_den[j], *t_num_bas + t_num[j]);

	return delta;
}

void
prism_solve_wpoi_upd(void *scratch, const double *x, const double *t, const double *a, double b, size_t n)
{
	double *num, *den, *lams, lam, coe;
	size_t j;

	assert(scratch != NULL || n < 1);
	assert(x != NULL || n < 1);
	assert(t != NULL || n < 1);
	assert(a != NULL || n < 1);

	/* Get data */
	num = (double *)scratch;
	den = &num[n];
	lams = &num[2*n];

	/* Predict */
	lam = 0.;
#pragma omp simd reduction(+: lam)
	for (j = 0; j < n; ++j)
	{
		/* Predict */
		lams[j] = a[j] * x[j];
		lam += lams[j];

		/* Update denominator */
		den[j] += t[j] * a[j];
	}

	/* Get scaling coefficient */
	coe = safe_div(b, lam);

	/* Update numerator */
#pragma omp simd
	for (j = 0; j < n; ++j)
		num[j] += t[j] * coe * lams[j];
}

static
double 
safe_div_signed(double x, double y)
{
	/* Compute x/y with 0./0. -> 0. for x >= 0. */
	if (!(x == 0.))
		return x / y;
	else
		return x;
}

static
double
prism_solve_l2_step(double *x, const double *t_A, const double *b, size_t m, size_t n)
{
	double *S;
	size_t i;

	assert(x != NULL || n < 1);
	assert(t_A != NULL || m*n < 1);
	assert(b != NULL || m < 1);

	/* Allocate */
	S = (double *)alloca(3*n * sizeof(*S));

	/* Zero accumulators */
	prism_solve_l2_init(S, n);

	/* Loop through */
	for (i = 0; i < m; ++i)
		prism_solve_l2_upd(S, x, &t_A[i*n], b[i], n);

	/* Update estimate & compute gradient bound */
	return prism_solve_l2_div(x, S, n);
}

void
prism_solve_l2(double *x, const double *t_A, const double *b, size_t m, size_t n)
{
	size_t j;

	assert(x != NULL || n < 1);
	assert(t_A != NULL || m*n < 1);
	assert(b != NULL || m < 1);

	/* Set up initial solution */
#pragma omp simd
	for (j = 0; j < n; ++j)
		x[j] = (double)(j+1);

	/* Loop */
	while (prism_solve_l2_step(x, t_A, b, m, n) > MIN_DELTA)
	{}
}

void
prism_solve_l2_init(void *scratch, size_t n)
{
	double *num, *den;

	assert(scratch != NULL || n < 1);

	/* Get data */
	num = (double *)scratch;
	den = &num[n];

	/* Zero accumulators */
	zero_d(num, n);
	zero_d(den, n);
}

void
prism_solve_l2_upd(void *scratch, const double *x, const double *a, double b, size_t n)
{
	double *num, *den, *lams, lam, bias;
	size_t j;

	assert(scratch != NULL || n < 1);
	assert(x != NULL || n < 1);
	assert(a != NULL || n < 1);

	/* Get data */
	num = (double *)scratch;
	den = &num[n];
	lams = &num[2*n];

	/* Predict */
	lam = 0.;
#pragma omp simd reduction(+: lam)
	for (j = 0; j < n; ++j)
	{
		/* Predict */
		lams[j] = a[j] * x[j];
		lam += lams[j];

		/* Update denominator */
		den[j] += a[j] * a[j];
	}

	/* Get conditioning correction */
	bias = safe_div_signed( b - lam, (double)n );

	/* Update numerator */
#pragma omp simd
	for (j = 0; j < n; ++j)
		num[j] += (lams[j] + bias) * a[j];
}

void
prism_solve_l2_upd_a(void *scratch, const double *x, const double *a, size_t n)
{
	double *num, *den, *lams, lam, bias;
	size_t j;

	assert(scratch != NULL || n < 1);
	assert(x != NULL || n < 1);
	assert(a != NULL || n < 1);

	/* Get data */
	num = (double *)scratch;
	den = &num[n];
	lams = &num[2*n];

	/* Predict */
	lam = 0.;
#pragma omp simd reduction(+: lam)
	for (j = 0; j < n; ++j)
	{
		/* Predict */
		lams[j] = a[j] * x[j];
		lam += lams[j];

		/* Update denominator */
		den[j] += a[j] * a[j];
	}

	/* Get conditioning correction */
	bias = safe_div_signed( 0. - lam, (double)n );

	/* Update numerator */
#pragma omp simd
	for (j = 0; j < n; ++j)
		num[j] += (lams[j] + bias) * a[j];
}

void
prism_solve_l2_upd_a_sparse(void *scratch, const double *x, const double *a, const size_t *a_i, size_t a_nnz, size_t n)
{
	double *num, *den, *lams, lam, bias;
	size_t q;

	assert(scratch != NULL || n < 1);
	assert(x != NULL || n < 1);
	assert(a != NULL || a_nnz < 1);
	assert(a_i != NULL || a_nnz < 1);

	/* Get data */
	num = (double *)scratch;
	den = &num[n];
	lams = &num[2*n];

	/* Predict */
	lam = 0.;
#pragma omp simd reduction(+: lam)
	for (q = 0; q < a_nnz; ++q)
	{
		/* Predict */
		lams[q] = a[q] * x[a_i[q]];
		lam += lams[q];

		/* Update denominator */
		den[a_i[q]] += a[q] * a[q];
	}

	/* Get conditioning correction */
	bias = safe_div_signed( 0. - lam, (double)n );

	/* Update numerator */
#pragma omp simd
	for (q = 0; q < a_nnz; ++q)
		num[a_i[q]] += (lams[q] + bias) * a[q];
}

void
prism_solve_l2_upd_b(void *scratch, const double *x, const double *a, double b, size_t n)
{
	double *num, lam, bias;
	size_t j;

	assert(scratch != NULL || n < 1);
	assert(x != NULL || n < 1);
	assert(a != NULL || n < 1);

	/* Get data */
	num = (double *)scratch;

	/* Predict */
	lam = 0.;
#pragma omp simd reduction(+: lam)
	for (j = 0; j < n; ++j)
		lam += a[j] * x[j];

	/* Get conditioning correction */
	bias = safe_div_signed( b - 0., (double)n );

	/* Update numerator */
#pragma omp simd
	for (j = 0; j < n; ++j)
		num[j] += (0. + bias) * a[j];
}

void
prism_solve_l2_upd_b_sparse(void *scratch, const double *x, const double *a, const size_t *a_i, size_t a_nnz, double b, size_t n)
{
	double *num, lam, bias;
	size_t q;

	assert(scratch != NULL || n < 1);
	assert(x != NULL || n < 1);
	assert(a != NULL || a_nnz < 1);
	assert(a_i != NULL || a_nnz < 1);

	/* Get data */
	num = (double *)scratch;

	/* Predict */
	lam = 0.;
#pragma omp simd reduction(+: lam)
	for (q = 0; q < a_nnz; ++q)
		lam += a[q] * x[a_i[q]];

	/* Get conditioning correction */
	bias = safe_div_signed( b - 0., (double)n );

	/* Update numerator */
#pragma omp simd
	for (q = 0; q < a_nnz; ++q)
		num[a_i[q]] += (0. + bias) * a[q];
}

double
prism_solve_l2_div(double *x, void *scratch, size_t n)
{
	const double *num, *den;
	double delta;
	size_t j;

	assert(x != NULL || n < 1);
	assert(scratch != NULL || n < 1);

	/* Get data */
	num = (double *)scratch;
	den = &num[n];

	/* Update x & compute gradient bound */
	delta = 0.;
	for (j = 0; j < n; ++j)
		delta += update_ratio(&x[j], num[j], den[j]);

	return delta;
}

void
prism_solve_poi_mat(double *X, const double *t_A, const double *B, size_t m, size_t n, size_t r)
{
	size_t j;

	assert(X != NULL || n*r < 1);
	assert(t_A != NULL || m*n < 1);
	assert(B != NULL || m*r < 1);

	/* Solve each */
#pragma omp parallel for schedule(static)
	for (j = 0; j < r; ++j)
		prism_solve_poi(&X[j*n], t_A, &B[j*m], m, n);

}

void
prism_solve_poi_mat_upd(double *X, const double *t_A, const double *B, size_t m, size_t n, size_t r)
{
	size_t j;

	assert(X != NULL || n*r < 1);
	assert(t_A != NULL || m*n < 1);
	assert(B != NULL || m*r < 1);

	/* Solve each */
#pragma omp parallel for schedule(static)
	for (j = 0; j < r; ++j)
		prism_solve_poi_step(&X[j*n], t_A, &B[j*m], m, n);
}

void
prism_solve_l2_mat(double *X, const double *t_A, const double *B, size_t m, size_t n, size_t r)
{
	size_t j;

	assert(X != NULL || n*r < 1);
	assert(t_A != NULL || m*n < 1);
	assert(B != NULL || m*r < 1);

	/* Solve each */
#pragma omp parallel for schedule(static)
	for (j = 0; j < r; ++j)
		prism_solve_l2(&X[j*n], t_A, &B[j*m], m, n);
}

void
prism_solve_l2_mat_upd(double *X, const double *t_A, const double *B, size_t m, size_t n, size_t r)
{
	size_t j;

	assert(X != NULL || n*r < 1);
	assert(t_A != NULL || m*n < 1);
	assert(B != NULL || m*r < 1);

	/* Solve each */
#pragma omp parallel for schedule(static)
	for (j = 0; j < r; ++j)
		prism_solve_l2_step(&X[j*n], t_A, &B[j*m], m, n);
}

static
void
prism_solve_poi_con(double *x, const double *a, size_t n)
{
	double L, sum_r, sum_Jr, delta;
	size_t j;

	assert(x != NULL || n < 1);
	assert(a != NULL || n < 1);

	/* Optimize Lagrangian */
	L = 0.;
	do
	{
		/* Compute residual and its Jacobian */
		sum_r = sum_Jr = 0.;
		for (j = 0; j < n; ++j)
		{
			sum_r += safe_div_signed(a[j] * x[j], a[j] + L);
			sum_Jr += safe_div_signed(a[j] * x[j], (a[j] + L) * (a[j] + L));
		}

		/* Newton step */
		delta = safe_div_signed(sum_r - 1., sum_Jr);
		L += delta;
	}
	while (fabs(delta) > MIN_DELTA);

	/* Update x */
	for (j = 0; j < n; ++j)
		x[j] = safe_div_signed(a[j] * x[j], a[j] + L);
}

void
prism_solve_poi_con_mat(double *X, const double *t_A, size_t m, size_t n, size_t r)
{
	double *den;
	size_t j, i;

	assert(X != NULL || n < 1);
	assert(t_A != NULL || m*n < 1);

	/* Allocate */
	den = (double *)alloca(n * sizeof(*den));

	/* Precompute row gain */
#pragma omp simd
	for (j = 0; j < n; ++j)
		den[j] = 0.;
#pragma omp parallel for schedule(static)
	for (i = 0; i < m; ++i)
#pragma omp simd
		for (j = 0; j < n; ++j)
			den[j] += t_A[i*n+j];

	/* Solve each */
#pragma omp parallel for schedule(static)
	for (j = 0; j < r; ++j)
		prism_solve_poi_con(&X[j*n], den, n);
}

static
void
conjgrad(double *x, const double *t_H, const double *b, size_t m, size_t n)
{
	double *r, *p, *A_p, rr_old, pHHp, dot, rr, alpha, beta;
	size_t j, k, i;

	/* Solve (H'*H)*x = b using the conjugate gradient algorithm */

	assert(x != NULL || n < 1);
	assert(t_H != NULL || m*n < 1);
	assert(b != NULL || n < 1);

	/* Allocate */
	r = (double *)alloca(3*n * sizeof(*x));
	p = &r[n];
	A_p = &p[n];

	/* Put in an initial guess */
#pragma omp simd
	for (j = 0; j < n; ++j)
		x[j] = 0.;
	/* Get initial residual */
	rr = 0.;
#pragma omp simd reduction(+: rr)
	for (j = 0; j < n; ++j)
	{
		p[j] = r[j] = b[j];
		rr += r[j] * r[j];
	}

	/* Loop */
	for (k = 0; k < n; ++k)
	{
		/* Zero accumulators */
		pHHp = 0.;
		for (j = 0; j < n; ++j)
			A_p[j] = 0.;

		/* Project residual */
		for (i = 0; i < m; ++i)
		{
			/* Compute projection */
			dot = 0.;
#pragma omp simd reduction(+: dot)
			for (j = 0; j < n; ++j)
				dot += t_H[i*n + j] * p[j];

			/* Complete */
#pragma omp simd
			for (j = 0; j < n; ++j)
				A_p[j] += t_H[i*n + j] * dot;

			/* Update gain */
			pHHp += dot * dot;
		}

		/* Update estimate */
		alpha = safe_div_signed(rr, pHHp);
#pragma omp simd
		for (j = 0; j < n; ++j)
			x[j] += alpha * p[j];

		/* Update residual */
		rr_old = rr;
		rr = 0.;
#pragma omp simd reduction(+: rr)
		for (j = 0; j < n; ++j)
		{
			r[j] -= alpha * A_p[j];
			rr += r[j] * r[j];
		}

		/* Good enough? */
		if (!(rr > MIN_DELTA))
			break;

		/* Update direction */
		beta = safe_div_signed(rr, rr_old);
#pragma omp simd
		for (j = 0; j < n; ++j)
			p[j] = r[j] + beta * p[j];
	}
}

static
void
prism_solve_l2_con(double *x, const double *p, size_t n, double sum_p)
{
	double sum_x, L;
	size_t j;

	assert(x != NULL || n < 1);
	assert(p != NULL || n < 1);

	/* Solve Lagrangian */
	sum_x = 0.;
#pragma omp simd reduction(+: sum_x)
	for (j = 0; j < n; ++j)
		sum_x += x[j];
	L = safe_div_signed(1. - sum_x, sum_p);

	/* Update x */
#pragma omp simd
	for (j = 0; j < n; ++j)
		x[j] += L * p[j];
}

void
prism_solve_l2_con_mat(double *X, const double *t_A, size_t m, size_t n, size_t r)
{
	double *p, *ones, den;
	size_t j;

	assert(X != NULL || n < 1);
	assert(t_A != NULL || m*n < 1);

	/* Allocate scratch */
	p = (double *)alloca(2*n * sizeof(*p));
	ones = &p[n];

	/* Solve direction */
#pragma omp simd
	for (j = 0; j < n; ++j)
		ones[j] = 1.;
	conjgrad(p, t_A, ones, m, n);

	/* Precompute gain */
	den = 0.;
#pragma omp simd reduction(+: den)
	for (j = 0; j < n; ++j)
		den += p[j];

	/* Solve each */
#pragma omp parallel for schedule(static)
	for (j = 0; j < r; ++j)
		prism_solve_l2_con(&X[j*n], p, n, den);
}
