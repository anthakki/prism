
#include "gain.h"
#include "math99.h"
#include "sort.h"
#include <alloca.h>
#include <assert.h>
#include <float.h>
#include <math.h>
#include <string.h>

/* Global tolerance for stopping, should be ~sqrt(eps) */
#define TOLERANCE (sqrt(DBL_EPSILON))

/* Small leakage weight for when row is trimmed empty, should be ~eps */
#define LEAKAGE (DBL_EPSILON)

static
size_t
init_uv(double *u, double *v, const double *X, size_t m, size_t n, double value)
	/* Mark u, v with value if the can be inferred, and NaN otherwise */
{
	size_t i, j, num_nans;

	assert(u != NULL || m < 1);
	assert(v != NULL || n < 1);
	assert(X != NULL || m*n < 1);
	
	/* Mark all rows & cols as dirty */
	for (i = 0; i < m; ++i)
		u[i] = (double)NAN;
	for (j = 0; j < n; ++j)
		v[j] = (double)NAN;

	/* Clear data rows & columns */
	num_nans = 0;
	for (j = 0; j < n; ++j)
		for (i = 0; i < m; ++i)
			if (!isnan(X[i + j*m]))
			{
				u[i] = value;
				v[j] = value;
			}
			else
				++num_nans;

	return num_nans;
}

static
size_t
prism_gain_poi_full(double *u, double *v, const double *X, size_t m, size_t n)
	/* Solve the Poisson problem for no missing data, 
	 * otherwise put in an approximate solution and return nonzero
	 */
{
	double s, inv_s;
	size_t num_nans, i, j;

	assert(u != NULL || m < 1);
	assert(v != NULL || n < 1);
	assert(X != NULL || m*n < 1);

	/* Set up */
	num_nans = init_uv(u, v, X, m, n, 0.);
	s = (double)NAN;
	if (num_nans < m*n)
		s = 0.;

	/* Accumulate */
	for (j = 0; j < n; ++j)
		for (i = 0; i < m; ++i)
			if (!isnan(X[i + j*m]))
			{
				u[i] += X[i + j*m];
				v[j] += X[i + j*m];
				s += X[i + j*m];
			}

	/* Scale u to match X */
	inv_s = 1. / s;
	if (num_nans < m*n)
		if (!(s > 0.))   /* NB. all zeros */
			inv_s = 0.;
	for (i = 0; i < m; ++i)
		u[i] *= inv_s;

	return num_nans;
}

static
void
prism_gain_poi_null(double *u, double *v, const double *X, size_t m, size_t n)
	/* Put in an initial solution for the trimmed models using a constant u
	 * solution */
{
	size_t j, i;
	double v_num, v_den;

	assert(u != NULL || m < 1);
	assert(v != NULL || n < 1);
	assert(X != NULL || m*n < 1);

	/* Set up */
	(void)init_uv(u, v, X, m, n, 0.);

	/* Get v */
#pragma omp parallel for private(v_num, v_den, i)
	for (j = 0; j < n; ++j)
		if (!isnan(v[j]))
		{
			v_num = v_den = 0.;
			for (i = 0; i < m; ++i)
				if (!isnan(X[i + j*m]))
				{
					v_num += X[i + j*m];
					v_den += 1.;
				}

			v[j] = (double)m * (v_num / v_den);
		}

	/* Get u */
	for (i = 0; i < m; ++i)
		if (!isnan(u[i]))
			u[i] = 1. / (double)m;
}

static
double
update_ratio(double *x, double x_num, double x_den)
	/* Solves problems with gradient num/x - den = 0,
	 * such that the new solution is x = num/den and and the gradient
	 * bound is ( num - den * x )^2 / ( den * x )
	 */
{
	double delta;

	assert(x != NULL);

	/* Handle missing row/column */
	if (isnan(*x))
		return 0.;
		
	/* Handle all zero row/column */
	if (!(x_num > 0.))
	{
		*x = 0.;
		return 0.;
	}

	/* Compute gradient bound */
	delta = ( x_num - x_den * *x ) * ( x_num - x_den * *x ) / ( x_den * *x );
	/* Update x */
	*x = x_num / x_den;

	return delta;
}

void
prism_gain_poi(double *u, double *v, const double *X, size_t m, size_t n)
{
	double *u_num, *u_den, delta, v_num, v_den;
	size_t j, i;

	assert(u != NULL || m < 1);
	assert(v != NULL || n < 1);
	assert(X != NULL || m*n < 1);

	/* Allocate */
	u_num = (double *)alloca(2*m * sizeof(*u));
	u_den = &u_num[m];

	/* Set up the initial guess */
	if (!(prism_gain_poi_full(u, v, X, m, n) > 0))
		return;

	/* Loop */
	for (;;)
	{
		/* Zero gradient bound */
		delta = 0.;

		/* Zero accumulators */
		memset(u_num, 0, m * sizeof(*u_num));
		memset(u_den, 0, m * sizeof(*u_den));

		/* Update v */
#pragma omp parallel for private(v_num, v_den, i)
		for (j = 0; j < n; ++j)
		{
			/* Zero accumulators */
			v_num = v_den = 0.;

			/* Process data */
			for (i = 0; i < m; ++i)
				if (!isnan(X[i + j*m]))
				{
					/* Accumulate for v */
					v_num += X[i + j*m];
					v_den += u[i];
				}

			/* Update v */
			delta += update_ratio(&v[j], v_num, v_den);
		}

		/* Do another pass over the data for u */
		for (j = 0; j < n; ++j)
#pragma omp parallel for schedule(static)
			for (i = 0; i < m; ++i)
				if (!isnan(X[i + j*m]))
				{
					/* Accumulate */
					u_num[i] += X[i + j*m];
					u_den[i] += v[j];
				}

		/* Update u */
		for (i = 0; i < m; ++i)
			delta += update_ratio(&u[i], u_num[i], u_den[i]);

		/* Stop? */
		if (!(delta > TOLERANCE))
			break;
	}
}

/* These could be specialized buth both need an interative algorithm. */

void
prism_gain_spoi(double *u, double *v, const double *X, size_t m, size_t n)
{ prism_gain_trimmed_spoi(u, v, X, m, n, 1.); }

void
prism_gain_l2(double *u, double *v, const double *X, size_t m, size_t n)
{ prism_gain_trimmed_l2(u, v, X, m, n, 1.); }

static
void
make_mask(double *w, const double *r, size_t m, double alpha)
{
	size_t t, i, k, n1, n2;
	double q, w2;

	assert(w != NULL || m < 1);
	assert(r != NULL || m < 1);

	/* Copy elements to scratch & strip NaNs */
	t = 0;
	for (i = 0; i < m; ++i)
		if (!isnan(r[i]))
			w[t++] = r[i];

	/* Handle empty */
	if (!(t > 0))
	{
		for (i = 0; i < m; ++i)   /* Zero w if all NaN */
			w[i] = 0.;
		return;
	}

	/* Find splitting value */
	if (!(alpha <= 1.))
		k = t;
	else if (!(alpha >= 0.))
		k = 0;
	else
		k = (size_t)(t * alpha);
	if (k < t)
	{
		nth_element_d(w, k, t);
		q = w[k];
	}
	else 
		q = HUGE_VAL;

	/* Count values less, eq, greater than q */
	n1 = n2 = 0;
	for (i = 0; i < t; ++i)
		if (w[i] < q)
			++n1;
		else if (w[i] == q)
			++n2;

	/* Distribute weights-- we have 1*n1 + w2*n2 = t*alpha */
	w2 = (( t*alpha - n1 )) / n2;
	if (!(w2 > 0.))
		w2 = 0.;
	for (i = 0; i < m; ++i)
		if (r[i] < q)
			w[i] = 1.;
		else if (r[i] == q)
			w[i] = w2;
		else 
			w[i] = LEAKAGE;
}

static
void
trimmed_gain(double *u, double *v, double *S, const double *X, size_t m, size_t n, double alpha, void (*get_dist)(double *r, const double *x, const double *u, double v, const double *S, size_t m), double (*update_v)(double *S, const double *w, const double *x, const double *u, double *v, size_t m), double (*update_u)(double *S, double *u, size_t m))
{
	double *r, *w, delta;
	size_t j;

	assert(u != NULL || m < 1);
	assert(v != NULL || n < 1);
	assert(S != NULL || m < 1);
	assert(X != NULL || m*n < 1);

	/* Get scratch */
	r = (double *)alloca(2*m * sizeof(*r));
	w = &r[m];

	/* Loop */
	for (;;)
	{
		/* Zero gradient */
		delta = 0.;

		/* Loop through the samples */
		for (j = 0; j < n; ++j)
		{
			/* Compute residuals */
			(*get_dist)(r, &X[j*m], u, v[j], S, m);

			/* Sort & weight */
			make_mask(w, r, m, alpha);

			/* This will run two-stage EM:
			 * - first update v
			 * - then update u using updated v
			 */

			/* Update v & accumulate for u */
			delta += (*update_v)(S, w, &X[j*m], u, &v[j], m);
		}

		/* Normalize u & zero accumulators */
		delta += (*update_u)(S, u, m);

		/* Stop */
		if (!(delta > TOLERANCE))
			break;
	}
}

static
double
poi_dist(double x, double lam)
{
	double lhs;

	/* Compute x*log(x)-x - ( x*log(lam)-lam ) */
	if (x > 0.)
		lhs = x * -log(lam / x);
	else
		lhs = 0.;
	return lhs - (x - lam);
}

static
void
poi_get_dist(double *r, const double *x, const double *u, double v, const double *S, size_t m)
{
	size_t i;

	assert(r != NULL || m < 1);
	assert(x != NULL || m < 1);
	assert(u != NULL || m < 1);
	assert(S != NULL || m < 1);

	/* Ignore unused */
	(void)S;

	/* Compute distances */
	for (i = 0; i < m; ++i)
		r[i] = poi_dist(x[i], u[i] * v);
}

static
double
trimmed_poi_update_v(double *S, const double *w, const double *x, const double *u, double *v, size_t m)
{
	double v_num, v_den, *u_num, *u_den, delta;
	size_t i;

	assert(w != NULL || m < 1);
	assert(x != NULL || m < 1);
	assert(u != NULL || m < 1);
	assert(S != NULL || m < 1);

	/* Get state */
	u_num = &S[0*m];
	u_den = &S[1*m];

	/* Accumulate for v */
	v_num = v_den = 0.;
	for (i = 0; i < m; ++i)
		if (!isnan(x[i]))
		{
			v_num += w[i] * x[i];
			v_den += w[i] * u[i];
		}

	/* Compute v */
	delta = update_ratio(v, v_num, v_den);

	/* Do another pass over the data for u */
	for (i = 0; i < m; ++i)
		if (!isnan(x[i]))
		{
			u_num[i] += w[i] * x[i];
			u_den[i] += w[i] * *v;
		}

	return delta;
}

static
double
trimmed_poi_update_u(double *S, double *u, size_t m)
{
	const double *u_num, *u_den;
	double delta;
	size_t i;

	assert(u != NULL || m < 1);
	assert(S != NULL || m < 1);

	/* Get state */
	u_num = &S[0*m];
	u_den = &S[1*m];

	/* Compute u */
	delta = 0.;
	for (i = 0; i < m; ++i)
		delta += update_ratio(&u[i], u_num[i], u_den[i]);

	/* Zero accumulators */
	memset(S, 0, 2*m * sizeof(*S));

	return delta;
}

void
prism_gain_trimmed_poi(double *u, double *v, const double *X, size_t m, size_t n, double alpha)
{
	double *S;

	assert(u != NULL || m < 1);
	assert(v != NULL || n < 1);
	assert(X != NULL || m*n < 1);

	/* Allocate & zero the state */
	S = (double *)alloca(2*m * sizeof(*S));
	memset(S, 0, 2*m * sizeof(*S));

	/* The state holds the numerator and denominator for u, what I call u_num and u_den */

	/* Put in an initial solution-- it's important u are tied in case of a
	 * trivial solution, so we put the usual and compute v accordingly
	 */
	prism_gain_poi_null(u, v, X, m, n);

	/* Run the estimator */
	trimmed_gain(u, v, S, X, m, n, alpha, &poi_get_dist, &trimmed_poi_update_v, &trimmed_poi_update_u);
}

static
double
spoi_dist(double x, double lam, double t)
{
	/* NOTE: this is 0 for t=1, but might be negative for other t */

	/* Get scaled Poisson "distance" */
	return -.5 * log(t) + t * poi_dist(x, lam);
}

static
void
spoi_get_dist(double *r, const double *x, const double *u, double v, const double *S, size_t m)
{
	const double *t_w;
	size_t i;

	assert(r != NULL || m < 1);
	assert(x != NULL || m < 1);
	assert(u != NULL || m < 1);
	assert(S != NULL || m < 1);

	/* Get state */
	t_w = &S[4*m];

	/* Compute distances */
	for (i = 0; i < m; ++i)
		r[i] = spoi_dist(x[i], u[i] * v, t_w[i]);
}

static
double
x_per_y(double x, double y)
{
	if (x > 0.)
		return x / y;
	else
		return 0.;
}

static
double
trimmed_spoi_update_v(double *S, const double *w, const double *x, const double *u, double *v, size_t m)
{
	double *u_num, *u_den, *t_num, *t_den;
	const double *t_w;
	double v_num, v_den, delta, lam, d;
	size_t i;

	assert(w != NULL || m < 1);
	assert(x != NULL || m < 1);
	assert(u != NULL || m < 1);
	assert(S != NULL || m < 1);

	/* Get state */
	u_num = &S[0*m];
	u_den = &S[1*m];
	t_num = &S[2*m];
	t_den = &S[3*m];
	t_w = &S[4*m];

	/* Accumulate for v */
	v_num = v_den = 0.;
	for (i = 0; i < m; ++i)
		if (!isnan(x[i]))
		{
			v_num += w[i] * ( t_w[i] * x[i] );
			v_den += w[i] * ( t_w[i] * u[i] );
		}

	/* Compute v */
	delta = update_ratio(v, v_num, v_den);

	/* Accumulate for u */
	for (i = 0; i < m; ++i)
		if (!isnan(x[i]))
		{
			/* Update moments for u */
			u_num[i] += w[i] * x[i];
			u_den[i] += w[i] * *v;

			/* Update moments for t */
			lam = u[i] * *v;
			d = x[i] - lam;
			t_den[i] += w[i] * x_per_y( d*d + 1./12., lam ); /* NB. 1/t statistic */
			t_num[i] += w[i];
		}

	return delta;
}

static
double
trimmed_spoi_update_u(double *S, double *u, size_t m)
{
	const double *u_num, *u_den, *t_num, *t_den;
	double *t_w, delta;
	size_t i;

	assert(u != NULL || m < 1);
	assert(S != NULL || m < 1);

	/* Get state */
	u_num = &S[0*m];
	u_den = &S[1*m];
	t_num = &S[2*m];
	t_den = &S[3*m];
	t_w = &S[4*m];

	/* Compute u */
	delta = 0.;
	for (i = 0; i < m; ++i)
		delta += update_ratio(&u[i], u_num[i], u_den[i]);

	/* Compute t weights */
	for (i = 0; i < m; ++i)
		update_ratio(&t_w[i], t_num[i], t_den[i]);

	/* Zero accumulators */
	memset(S, 0, 4*m * sizeof(*S));

	return delta;
}

void
prism_gain_trimmed_spoi(double *u, double *v, const double *X, size_t m, size_t n, double alpha)
{
	double *S, *t_w;
	size_t i;

	assert(u != NULL || m < 1);
	assert(v != NULL || n < 1);
	assert(X != NULL || m*n < 1);

	/* Allocate */
	S = (double *)alloca(5*m * sizeof(*S));
	memset(S, 0, 5*m * sizeof(*S));

	/* The state holds u_num, u_den, then the running (unscaled Fano factor)
	 * using numerator t_num and denominator t_den, and finally the t-weights
	 * from the previous round 
	 */

	/* Put in an initial solution */
	prism_gain_poi_null(u, v, X, m, n);

	/* Fill in initial t-weights */
	t_w = &S[4*m];
	for (i = 0; i < m; ++i)
		t_w[i] = 1.;

	/* Run the estimator */
	trimmed_gain(u, v, S, X, m, n, alpha, &spoi_get_dist, &trimmed_spoi_update_v, &trimmed_spoi_update_u);
}

static
double
l2_dist(double x, double y)
{
	double d;

	/* Get squared distance */
	d = x - y;
	return d * d;
}

static
void
l2_get_dist(double *r, const double *x, const double *u, double v, const double *S, size_t m)
{
	size_t i;

	assert(r != NULL || m < 1);
	assert(x != NULL || m < 1);
	assert(u != NULL || m < 1);
	assert(S != NULL || m < 1);

	/* Ignore unused */
	(void)S;

	/* Compute distances */
	for (i = 0; i < m; ++i)
		r[i] = l2_dist(x[i], u[i] * v);
}

static
double
trimmed_l2_update_v(double *S, const double *w, const double *x, const double *u, double *v, size_t m)
{
	double *u_num, *u_den, v_num, v_den, v1, v2, delta;
	size_t i;

	assert(w != NULL || m < 1);
	assert(x != NULL || m < 1);
	assert(u != NULL || m < 1);
	assert(S != NULL || m < 1);

	/* Get state */
	u_num = &S[0*m];
	u_den = &S[1*m];

	/* Accumulate for v */
	v_num = v_den = 0.;
	for (i = 0; i < m; ++i)
		if (!isnan(x[i]))
		{
			v_num += w[i] * ( x[i] * u[i] );
			v_den += w[i] * ( u[i] * u[i] );
		}

	/* Update v */
	delta = update_ratio(v, v_num, v_den);
	v1 = *v;
	v2 = v1 * v1;

	/* Accumulate for u */
	for (i = 0; i < m; ++i)
		if (!isnan(x[i]))
		{
			u_num[i] += w[i] * ( x[i] * v1 );
			u_den[i] += w[i] * (        v2 );
		}

	return delta;
}

static
double
trimmed_l2_update_u(double *S, double *u, size_t m)
{
	const double *u_num, *u_den;
	double delta;
	size_t i;

	assert(u != NULL || m < 1);
	assert(S != NULL || m < 1);

	/* Get state */
	u_num = &S[0*m];
	u_den = &S[1*m];

	/* Compute u */
	delta = 0.;
	for (i = 0; i < m; ++i)
		delta += update_ratio(&u[i], u_num[i], u_den[i]);

	/* Zero accumulators */
	memset(S, 0, 2*m * sizeof(*S));

	return delta;
}

static
void
prism_gain_l2_null(double *u, double *v, const double *X, size_t m, size_t n)
	/* Put in an initial solution for the trimmed models using a constant u
	 * solution */
{
	size_t j, i;
	double sqrt_m, v_num, v_den;

	assert(u != NULL || m < 1);
	assert(v != NULL || n < 1);
	assert(X != NULL || m*n < 1);

	/* Set up */
	(void)init_uv(u, v, X, m, n, 0.);

	/* Compute scaling */
	sqrt_m = sqrt( (double)m );

	/* Get v */
#pragma omp parallel for private(v_num, v_den, i)
	for (j = 0; j < n; ++j)
		if (!isnan(v[j]))
		{
			v_num = v_den = 0.;
			for (i = 0; i < m; ++i)
				if (!isnan(X[i + j*m]))
				{
					v_num += X[i + j*m];
					v_den += 1.;
				}

			v[j] = sqrt_m * (v_num / v_den);
		}

	/* Get u */
	for (i = 0; i < m; ++i)
		if (!isnan(u[i]))
			u[i] = 1. / sqrt_m;
}

void
prism_gain_trimmed_l2(double *u, double *v, const double *X, size_t m, size_t n, double alpha)
{
	double *S;

	assert(u != NULL || m < 1);
	assert(v != NULL || n < 1);
	assert(X != NULL || m*n < 1);

	/* Get & zero state */
	S = (double *)alloca(2*m * sizeof(*S));
	memset(S, 0, 2*m * sizeof(*S));

	/* The state has u_num, u_den */

	/* Put in an initial solution */
	prism_gain_l2_null(u, v, X, m, n);

	/* Run estimator */
	trimmed_gain(u, v, S, X, m, n, alpha, &l2_get_dist, &trimmed_l2_update_v, &trimmed_l2_update_u);
}
