
#include "clust.h"
#include "gamma.h"
#include "hierarch.h"
#include <assert.h>
#include <float.h>
#include <math.h>
#include <string.h>

struct poi_cluster {
	/* Row sums of the samples in X for this cluster-- this represents the
	 * centroid for the cluster */
	const double *r;

	/* Marginals for X and r*log(r)-- these are needed for the costs */
	double sum_x, sum_w, sum_rlr;
};

static
double
xlogy(double x, double y)
{
	/* Compute x*log(x) with 0*log(0) = 0 */
	if (x > 0.)
		return x*log(y);

	return 0.;
}

static
double
xlogx(double x)
{ return xlogy(x, x); }

static
void
poi_do_init(struct poi_cluster *clust, const double *x, double w, size_t m)
{
	double sum_x, sum_xlx;
	size_t i;

	assert(clust != NULL);
	assert(x != NULL || m < 1);

	/* Compute sums */
	sum_x = sum_xlx = 0.;
	for (i = 0; i < m; ++i)
	{
		sum_x += x[i];
		sum_xlx += xlogx(x[i]);
	}

	/* Store row sums */
	clust->r = x;
	
	/* Store marginals */
	clust->sum_x = sum_x;
	clust->sum_w = w;
	clust->sum_rlr = sum_xlx;   /* NB. r = x */
}

static
void
poi_do_merge(struct poi_cluster *clust1, const struct poi_cluster *clust2, double *r, size_t m)
{
	double sum_rlr;
	size_t i;

	assert(clust1 != NULL);
	assert(clust2 != NULL);
	assert(r != NULL || m < 1);

	/* Merge row sums */
	sum_rlr = 0.;
	for (i = 0; i < m; ++i)
	{
		r[i] = clust1->r[i] + clust2->r[i];
		sum_rlr += xlogx(r[i]);
	}

	/* Store row sums */
	clust1->r = r;

	/* Store marginals */
	clust1->sum_x = clust1->sum_x + clust2->sum_x;
	clust1->sum_w = clust1->sum_w + clust2->sum_w;
	clust1->sum_rlr = sum_rlr;
}

static
double
poi_merged_cost(const struct poi_cluster *clust1, const struct poi_cluster *clust2, size_t m)
{
	double sum_rlr, sum_x, sum_w;
	size_t i;

	assert(clust1 != NULL);
	assert(clust2 != NULL);

	/* Get row sum entropy after merge */
	sum_rlr = 0.;
	for (i = 0; i < m; ++i)
		sum_rlr += xlogx(clust1->r[i] + clust2->r[i]);

	/* Get marginal sums */
	sum_x = clust1->sum_x + clust2->sum_x;
	sum_w = clust1->sum_w + clust2->sum_w;

	/* Compute cost */
	return xlogy(sum_x, sum_w) - (( xlogy(clust1->sum_x, clust1->sum_w) + xlogy(clust2->sum_x, clust2->sum_w) )) +
		-sum_rlr - (( -clust1->sum_rlr + -clust2->sum_rlr ));
}

static
void
poi_do_merged_costs(double *d, const struct poi_cluster *clusters, size_t m, size_t n)
{
	size_t i;

	/* Loop through & compute costs */
#pragma omp parallel for
	for (i = 0; i < n; ++i)
		d[i] = poi_merged_cost(&clusters[i], &clusters[n], m);
}

struct poi_cookie {
	/* Clusters */
	struct poi_cluster *clusters;
	/* Sample dimension */
	size_t m;

	/* An m*(n-1) matrix for temporary row sums */
	double *Y;
	/* Tail pointer for the above */
	size_t p;

}; /* struct poi_cookie */

static
void
poi_swap(size_t i, size_t j, void *arg)
{
	const struct poi_cookie *cookie;
	struct poi_cluster *clusters, tmp;

	assert(i < j);
	assert(arg != NULL);

	/* Get data */
	cookie = (const struct poi_cookie *)arg;
	clusters = cookie->clusters;

	/* Swap clusters */
	tmp = clusters[i];
	clusters[i] = clusters[j];
	clusters[j] = tmp;
}

static
void
poi_merge(size_t i, size_t j, double *D, void *arg)
{
	struct poi_cookie *cookie;
	struct poi_cluster *clusters;
	size_t m;

	assert(i < j);
	assert(D != NULL);
	assert(arg != NULL);

	/* Get data */
	cookie = (struct poi_cookie *)arg;
	clusters = cookie->clusters;
	m = cookie->m;

	/* Merge clusters */
	poi_do_merge(&clusters[i], &clusters[j], &cookie->Y[cookie->p++ * m], m);
	/* Merge distances */
	poi_do_merged_costs(&D[i*(i-1)/2], clusters, m, i);
}

size_t
prism_clust_poi_scratch(size_t m, size_t n)
{
	/* Compute scratch: D, Y, clusters, L */
	return n*(n-1)/2 + m*n + 4*n + 2*n;
}

void
prism_clust_poi(double *Z, void *scratch, const double *X, const double *v, size_t m, size_t n)
{
	double *D, *Y;
	struct poi_cluster *clusters;
	size_t *L, j;
	struct poi_cookie cookie;

	/* Designate scratch */
	D = (double *)scratch;
	Y = &D[n*(n-1)/2];
	clusters = (struct poi_cluster *)&Y[m*n];
	L = (size_t *)&clusters[n];

	/* Create initial clusters */
	for (j = 0; j < n; ++j)
		poi_do_init(&clusters[j], &X[j*m], v[j], m);
	/* Compute initial distances */
	for (j = 1; j < n; ++j)
		poi_do_merged_costs(&D[j*(j-1)/2], clusters, m, j);

	/* Fill in the cookie */
	cookie.clusters = clusters;
	cookie.m = m;
	cookie.Y = Y;
	cookie.p = 0;

	/* Cluster */
	linkage_rnn(n, L, Z, D, &cookie, &poi_swap, &poi_merge);
}

static
double
z_or_div(double x, double y)
{
	/* Compute x/y with 0./0. -> 0. for x >= 0. */
	if (x > 0.)
		return x / y;
	else
		return 0.;
}

struct spoi_cluster {
	/* Running row sums */
	const double *r, *q, *z;
	/* Running sums */
	double sum_w, sum_1;
	/* Cached relative cost term */
	double cost;
};

struct spoi_cookie {
	/* Clusters */
	struct spoi_cluster *clusters;
	/* Sample dimension */
	size_t m;

	/* Array for temporary row sums */
	double *Y;
	/* Tail pointer for the above */
	size_t p;

}; /* struct spoi_cookie */

static
void
spoi_do_init(struct spoi_cookie *cookie, struct spoi_cluster *clust, const double *x, double w, size_t m)
{
	double *q, *z, y_per_w;
	size_t i;

	assert(cookie != NULL);
	assert(clust != NULL);
	assert(x != NULL || m < 1);

	/* Designate arrays */
	clust->r = x;
	q = &cookie->Y[cookie->p++ * m];
	clust->q = q;
	z = &cookie->Y[cookie->p++ * m];
	clust->z = z;

	/* Compute row sums */
	for (i = 0; i < m; ++i)
	{
		y_per_w = z_or_div(x[i], w);
	
		/* Accumulate */
		q[i] = x[i] * y_per_w;
		z[i] = xlogy(x[i], y_per_w);
	}

	/* Store weights */
	clust->sum_w = w;
	clust->sum_1 = 1.;

	/* Store cost */
	clust->cost = 0.;   /* NB. cost is actually -Inf, but we choose a finite basal level */
}

#include <stdio.h>
static
double
spoi_cost(double r, double q, double z, double w, double n)
{
	double t, w_per_r;

	/* Estimate t & r/w ratio */
	t = (( w * z_or_div(q, r) - r + n/12. )) / n;
	w_per_r = z_or_div(w, r);

	/* Get cost */
	return -.5*n*log(t) + r * (( xlogy(r, w_per_r) + z ));
}

static
void
spoi_do_merge(struct spoi_cookie *cookie, struct spoi_cluster *clust1, const struct spoi_cluster *clust2, size_t m)
{
	double *r, *q, *z, cost;
	size_t i;

	assert(cookie != NULL);
	assert(clust1 != NULL);
	assert(clust2 != NULL);

	/* Allocate new arrays */
	r = &cookie->Y[cookie->p++ * m];
	q = &cookie->Y[cookie->p++ * m];
	z = &cookie->Y[cookie->p++ * m];

	/* Merge row sums */
	for (i = 0; i < m; ++i)
	{
		r[i] = clust1->r[i] + clust2->r[i];
		q[i] = clust1->q[i] + clust2->q[i];
		z[i] = clust1->z[i] + clust2->z[i];
	}

	/* Store */
	clust1->r = r;
	clust1->q = q;
	clust1->z = z;

	/* Merge weights */
	clust1->sum_w = clust1->sum_w + clust2->sum_w;
	clust1->sum_1 = clust1->sum_1 + clust2->sum_1;

	/* Recompute cost */
	cost = 0.;
	for (i = 0; i < m; ++i)
		cost += spoi_cost(clust1->r[i], clust1->q[i], clust1->z[i], clust1->sum_w, clust1->sum_1);
	clust1->cost = cost;
}

static
double
spoi_merged_cost(const struct spoi_cluster *clust1, const struct spoi_cluster *clust2, size_t m)
{
	double sum_w, sum_1, cost, ref_cost;
	size_t i;

	assert(clust1 != NULL);
	assert(clust2 != NULL);

	/* Get joint parameters */
	sum_w = clust1->sum_w + clust2->sum_w;
	sum_1 = clust1->sum_1 + clust2->sum_1;

	/* Compute objective */
	cost = 0.;
	for (i = 0; i < m; ++i)
		cost += spoi_cost(clust1->r[i] + clust2->r[i], clust1->q[i] + clust2->q[i], clust1->z[i] + clust2->z[i], sum_w, sum_1);

	/* Cap costs to finite values */
	ref_cost = clust1->cost + clust2->cost;
	if (cost < ref_cost)   /* NB. can't be better than parent, parent is capped */
		cost = ref_cost;

	/* Return objective change */ 
	return cost - ref_cost;
}

static
void
spoi_do_merged_costs(double *d, const struct spoi_cluster *clusters, size_t m, size_t n)
{
	size_t i;

	/* Loop through & compute costs */
#pragma omp parallel for
	for (i = 0; i < n; ++i)
		d[i] = spoi_merged_cost(&clusters[i], &clusters[n], m);
}

static
void
spoi_swap(size_t i, size_t j, void *arg)
{
	const struct spoi_cookie *cookie;
	struct spoi_cluster *clusters, tmp;

	assert(i < j);
	assert(arg != NULL);

	/* Get data */
	cookie = (const struct spoi_cookie *)arg;
	clusters = cookie->clusters;

	/* Swap clusters */
	tmp = clusters[i];
	clusters[i] = clusters[j];
	clusters[j] = tmp;
}

static
void
spoi_merge(size_t i, size_t j, double *D, void *arg)
{
	struct spoi_cookie *cookie;
	struct spoi_cluster *clusters;
	size_t m;

	assert(i < j);
	assert(D != NULL);
	assert(arg != NULL);

	/* Get data */
	cookie = (struct spoi_cookie *)arg;
	clusters = cookie->clusters;
	m = cookie->m;

	/* Merge clusters */
	spoi_do_merge(cookie, &clusters[i], &clusters[j], m);
	/* Merge distances */
	spoi_do_merged_costs(&D[i*(i-1)/2], clusters, m, i);
}

size_t
prism_clust_spoi_scratch(size_t m, size_t n)
{
	/* Compute scratch: D, Y, clusters, L */
	return n*(n-1)/2 + 6*m*n + 6*n + 2*n;
}

void
prism_clust_spoi(double *Z, void *scratch, const double *X, const double *v, size_t m, size_t n)
{
	double *D, *Y;
	struct spoi_cluster *clusters;
	size_t *L, j;
	struct spoi_cookie cookie;

	/* Designate scratch */
	D = (double *)scratch;
	Y = &D[n*(n-1)/2];
	clusters = (struct spoi_cluster *)&Y[6*m*n];
	L = (size_t *)&clusters[n];

	/* Fill in the cookie */
	cookie.clusters = clusters;
	cookie.m = m;
	cookie.Y = Y;
	cookie.p = 0;

	/* Create initial clusters */
	for (j = 0; j < n; ++j)
		spoi_do_init(&cookie, &clusters[j], &X[j*m], v[j], m);
	/* Compute initial distances */
	for (j = 1; j < n; ++j)
		spoi_do_merged_costs(&D[j*(j-1)/2], clusters, m, j);

	/* Cluster */
	linkage_rnn(n, L, Z, D, &cookie, &spoi_swap, &spoi_merge);
}

size_t
prism_clust_l2_scratch(size_t m, size_t n)
{
	/* Need scratch for w, D, and L */
	(void)m;   /* NB. m is not relevant here */
	return n + n*(n-1)/2 + 2*n;
}

static
double
lsq_dist_with_gains(const double *x, double vx, const double *y, double vy, size_t m)
{
	double sum_r, r;
	size_t i;

	assert(x != NULL || m < 1);
	assert(y != NULL || m < 1);

	/* Compute squared distance */
	sum_r = 0.;
	for (i = 0; i < m; ++i)
	{
		r = vy * x[i] - vx * y[i];
		sum_r += r * r;
	}

	return 2. * z_or_div(sum_r, vx*vx + vy*vy);
}

static
void
ward_init_with_gains(size_t m, size_t n, double *w, double *D, const double *X, const double *v)
{
	size_t j, i;

	assert(w != NULL || n < 1);
	assert(D != NULL || n < 2);
	assert(X != NULL || m*n < 1);

	/* Put in initial weights */
	for (j = 0; j < n; ++j)
		w[j] = v[j] * v[j];

	/* Compute initial distances */
	for (j = 0; j < n; ++j)
#pragma omp parallel for
		for (i = 0; i < j; ++i)
			D[i + j*(j-1)/2] = lsq_dist_with_gains(&X[i*m], v[i], &X[j*m], v[j], m);
}

void
prism_clust_l2(double *Z, void *scratch, const double *X, const double *v, size_t m, size_t n)
{
	double *D, *w;
	void *more_scratch;

	assert(Z != NULL || m < 2);
	assert(scratch != NULL || prism_clust_l2_scratch(m, n) < 1);
	assert(X != NULL || m*n < 1);

	/* Designate scratch */
	w = (double *)scratch;
	D = &w[n];
	more_scratch = &D[n*(n-1)/2];

	/* Compute initial distances */
	ward_init_with_gains(m, n, w, D, X, v);
	/* Build tree */
	linkage_rnn(n, more_scratch, Z, D, w, &ward_swap, &ward_merge);
}

size_t
prism_clust_select(const double *Z, size_t m, size_t n, double penalty)
{
	double ress, best_cost, cost;
	size_t dofs, best_ord, k, ord;

	/* Empty? */
	if (!(n > 1))
		return n;

	/* Put in the trivial solution */
	ress = 0.;
	best_cost = HUGE_VAL;
	best_ord = 0;

#if 0
	/* Dump header & first */
__extension__ fprintf(stderr, "%2s %12s %12s %5s %5s %10s %2s" "\n", "", "heights", "costs", "dofs", "samps", "loss", "k");
__extension__ fprintf(stderr, "%2d %12.6f %12.6f %5zu %5zu %10.4f %2zu" "\n", 1, 0., ress, dofs, m*n, best_cost, best_ord);
#endif

	/* Evaluate rest */
	for (k = 0; k < n-1; ++k)
	{
		/* Update parameters */
		ress += Z[k + 2*(n-1)];
		ord = n-(k+1);
		dofs = m*ord;
		cost = penalty*dofs + m*n*log(ress > 0. ? ress : 0.);

#if 0
		/* Dump data */
__extension__ fprintf(stderr, "%2d %12.6f %12.6f %5zu %5zu %10.4f %2zu" "\n", 2+(int)k, Z[k + 2*(n-1)], ress, dofs, m*n, cost, ord);
#endif

		/* Better? */
		if (cost <= best_cost)   /* NB. tie break for smaller */
		{
			best_cost = cost;
			best_ord = ord;
		}
	}

	return best_ord;
}

double
prism_clust_bic_penalty(size_t m, size_t n)
{
	/* Return cost per excess parameter in BIC */
	return log(m*n);
}

double
prism_clust_aic_penalty(size_t m, size_t n)
{
	/* Return cost per excess parameter in AIC */
	(void)m, (void)n;
	return 2.;
}

static
double
chi2inv_upper_log(double log_pv, double df)
{
	double hi, lo, x;

	/* Find an upper bound */
	hi = 2*df;
	while (gamma_q_log(.5*hi, .5*df) > log_pv)
		hi += hi;

	/* Bisect */
	lo = 0.;
	x = lo + .5*(hi-lo);
	while (lo < x && x < hi)
	{
		/* Branch */
		if (gamma_q_log(.5*x, .5*df) > log_pv)
			lo = x;
		else
			hi = x;

		/* Update */
		x = lo + .5*(hi-lo);
	}

	return x;
}

double
prism_clust_lr_penalty(size_t m, size_t n, double log_pv)
{
	/* Return cost per excess parameter */
	(void)n;
	return 1./m * chi2inv_upper_log(log_pv, m);
}
