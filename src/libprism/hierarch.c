
#include "hierarch.h"
#include <assert.h>
#include <math.h>

static
double
get_cost(const double *D, size_t i, size_t j)
{
	assert(D != NULL);
	assert(i < j);

	/* Get element */
	return D[i + j*(j-1)/2];
}

static
void
set_cost(double *D, size_t i, size_t j, double cost)
{
	assert(D != NULL);
	assert(i < j);

	/* Set element value */
	D[i + j*(j-1)/2] = cost;
}

static
size_t
find_nearest(const size_t *L, const double *D, size_t n, size_t p)
{
	double best_dist, cost;
	size_t best_q, q;

	assert(D != NULL);
	assert(n > 1);
	assert(p < n);

	/* TODO: break ties using L */
	(void)L;

	/* Put in an empty solution */
	best_dist = HUGE_VAL;
	best_q = 0 + (p == 0);   /* NB. feasible, if costs overflow */

	/* Scan through the distances */
	for (q = 0; q < p; ++q)
		if ((cost = get_cost(D, q, p)) < best_dist)
		{
			best_dist = cost;
			best_q = q;
		}
	for (q = p+1; q < n; ++q)
		if ((cost = get_cost(D, p, q)) < best_dist)
		{
			best_dist = cost;
			best_q = q;
		}

	return best_q;
}

static
size_t
find_pair(size_t *pair, const size_t *L, const double *D, size_t n, size_t last)
{
	size_t p, q, p_cand;

	assert(pair != NULL);
	assert(D != NULL);
	assert(n > 1);
	assert(last < n);

	/* Find reciprocal nearest neighbors */
	{{
		/* Boot */
		p = find_nearest(L, D, n, last);
		q = find_nearest(L, D, n, p);

		/* Walk the NN graph */
		while ((p_cand = find_nearest(L, D, n, q)) != p)
		{
			/* Rotate */
			last = p;
			p = q;
			q = p_cand;
		}
	}}

	/* Store p, q sorted */
	if (!(p > q))
	{
		pair[0] = p;
		pair[1] = q;
	}
	else
	{
		pair[0] = q;
		pair[1] = p;
	}

	return last;
}

static
void
store_pair(double *Z, size_t ldZ, size_t label1, size_t label2, double cost)
{
	assert(Z != NULL);
	assert(ldZ > 0);

	/* Store */
	Z[0*ldZ] = (double)label1;
	Z[1*ldZ] = (double)label2;
	Z[2*ldZ] = cost;
}

static
void
swap_costs(double *D, size_t p, size_t q, size_t n)
{
	size_t i;
	double tmp;

	assert(D != NULL);
	assert(p <= q);
	assert(q < n);

	/* Done already? */
	if (p == q)
		return;

	/* Swap each block */
	for (i = 0; i < p; ++i)
	{
		tmp = get_cost(D, i, p);
		set_cost(D, i, p, get_cost(D, i, q));
		set_cost(D, i, q, tmp);
	}
	for (i = p+1; i < q; ++i)
	{
		tmp = get_cost(D, p, i);
		set_cost(D, p, i, get_cost(D, i, q));
		set_cost(D, i, q, tmp);
	}
	for (i = q+1; i  <n; ++i)
	{
		tmp = get_cost(D, p, i);
		set_cost(D, p, i, get_cost(D, q, i));
		set_cost(D, q, i, tmp);
	}
}

static
void
sort_tree(void *scratch, double *Z, size_t ldZ, size_t n)
{
	size_t *map, *perm, j, r;

	assert(Z != NULL || n < 2);
	assert(ldZ > 0);

	/* Designate scratch */
	map = (size_t *)scratch;
	perm = map;

	/* Find a permutation to sort the branches */
	{{
		size_t ja, jb, p, c;

		/* Set up a stack & push the root */ 
		ja = jb = n-1;   /* NB. [0,ja) ~ free, [ja,jb) ~ stack, [jb,n-1) ~ done */
		if (n > 2)
			perm[--ja] = n-2;

		/* Process stack */
		while (ja < jb)
		{
			/* Pop stack top */
			p = perm[--jb];

			/* Visit children */
			for (r = 0; r < 2; ++r)
				if (!(Z[p + r*ldZ] < n))
				{
					/* Get child row */
					c = (size_t)Z[p + r*ldZ] - n;

					/* Allocate a slot & sort the item in the stack */
					for (j = --ja; j+1 < jb && Z[perm[j+1] + 2*ldZ] < Z[c + 2*ldZ]; ++j)
						perm[j] = perm[j+1];

					/* Schedule child */
					perm[j] = c;
				}
		}
	}}

	/* Get new labels */
	for (j = 0; j < n-1; ++j)
		map[n + perm[j]] = n + j;
	for (j = 0; j < n; ++j)
		map[j] = j;

	/* Relabel & sort the children */
	for (j = 0; j < n-1; ++j)
	{
		size_t pp[2], tmp;

		/* Get labels */
		for (r = 0; r < 2; ++r)
			pp[r] = map[(size_t)Z[j + r*ldZ]];

		/* Sort */
		if (pp[0] > pp[1])
		{
			tmp = pp[0];
			pp[0] = pp[1];
			pp[1] = tmp;
		}

		/* Put back */
		for (r = 0; r < 2; ++r)
			Z[j + r*ldZ] = (double)pp[r];
	}

	/* Sort the rows */
	{{
		for (j = 0; j < n-1; ++j)
		{
			size_t key, i;
			double Zj[3];

			/* Get value */
			key = map[n+j];
			for (r = 0; r < 3; ++r)
				Zj[r] = Z[j + r*ldZ];

			/* Find slot */
			for (i = j; i > 0 && key < map[n+i-1]; --i)
			{
				map[n+i] = map[n+i-1];
				for (r = 0; r < 3; ++r)
					Z[i + r*ldZ] = Z[(i-1) + r*ldZ];
			}

			/* Put back */
			map[n+i] = key;
			for (r = 0; r < 3; ++r)
				Z[i + r*ldZ] = Zj[r];
		}
	}}
}

void
linkage_rnn(size_t n, void *scratch, double *Z, double *D, void *cookie, void (*swap)(size_t, size_t, void *), void (*merge)(size_t, size_t, double *, void *))
{
	size_t *L, j, last, k, pair[2];

	assert(Z != NULL || n < 2);
	assert(D != NULL || n < 1);

	/* Stop if trivial */
	if (!(n > 1))
		return;

	/* Designate scratch */
	L = (size_t *)scratch;

	/* Make initial labels */
	for (j = 0; j < n; ++j)
		L[j] = j;

	/* Aggregate */
	last = 0;
	for (k = 0; k < n-1; ++k)
	{
		/* Find next pair & save */
		last = find_pair(pair, L, D, n-k, last);
		store_pair(&Z[k], n-1, L[pair[0]], L[pair[1]], get_cost(D, pair[0], pair[1]));

		/* Update chain pointer for next round */
		if (last == pair[0] || last == pair[1])   /* NB. stale, reset */
			/* TODO: break ties with L */
			last = 0;
		else
		{
			if (last == n-k-1)
				last = pair[1];
			if (last == n-k-2)   /* NB. *no* else-- pair[1] might be n-k-2 */
				last = pair[0];
		}

		/* Swap cluster unless already in place */
		if (pair[1] < n-k-1)
			(*swap)(pair[1], n-k-1, cookie);
		if (pair[0] < n-k-2)
			(*swap)(pair[0], n-k-2, cookie);

		/* Merge costs */
		swap_costs(D, pair[1], n-k-1, n-k);
		swap_costs(D, pair[0], n-k-2, n-k);
		(*merge)(n-k-2, n-k-1, D, cookie);

		/* Update labels */
		L[pair[1]] = L[n-k-1];
		L[pair[0]] = L[n-k-2];
		L[n-k-2] = n+k;
	}

	/* Sort the branches of the resulting tree */
	sort_tree(scratch, Z, n-1, n);
}

static
double
lsq_dist(const double *x, const double *y, size_t m)
{
	double sum_r, r;
	size_t i;

	assert(x != NULL || m < 1);
	assert(y != NULL || m < 1);

	/* Compute squared distance */
	sum_r = 0.;
#pragma omp simd reduction(+:sum_r) 
	for (i = 0; i < m; ++i)
	{
		r = x[i] - y[i];
		sum_r += r * r;
	}

	return sum_r;
}

void
ward_init(size_t m, size_t n, double *w, double *D, const double *X)
{
	size_t j, i;

	assert(w != NULL || n < 1);
	assert(D != NULL || n < 2);
	assert(X != NULL || m*n < 1);

	/* Put in initial weights */
	for (j = 0; j < n; ++j)
		w[j] = 1.;

	/* Compute initial distances */
#pragma omp parallel for schedule(dynamic) private(i)
	for (j = 0; j < n; ++j)
		for (i = 0; i < j; ++i)
			D[i + j*(j-1)/2] = lsq_dist(&X[i*m], &X[j*m], m);
}

void
ward_swap(size_t i, size_t j, void *cookie)
{
	double *w, tmp;

	assert(i < j);
	assert(cookie != NULL);

	/* Get data */
	w = (double *)cookie;

	/* Swap weights */
	tmp = w[i];
	w[i] = w[j];
	w[j] = tmp;
}

void
ward_merge(size_t i, size_t j, double *D, void *cookie)
{
	double *w, wi, wj, dij, *di;
	const double *dj;
	size_t k;

	assert(i < j);
	assert(D != NULL);
	assert(cookie != NULL);

	/* Get data */
	w = (double *)cookie;

	/* Get constants */
	wi = w[i];
	wj = w[j];
	di = &D[i*(i-1)/2];
	dj = &D[j*(j-1)/2];
	dij = dj[i];

	/* Merge distances */
#pragma omp simd
	for (k = 0; k < i; ++k)
		di[k] = (( (wi+w[k])*di[k] + (wj+w[k])*dj[k] - w[k]*dij )) / (wi+wj+w[k]);

	/* Merge weights */
	w[i] = wi + wj;
}

void
cutree(size_t *L, const double *Z, size_t k, size_t n)
{
	size_t i, seen;

	assert(L != NULL || n < 1);
	assert(Z != NULL || n < 2);
	assert(1 <= k && k <= n);

	/* Label each leaf */
	for (i = 0; i < n; ++i)
		L[i] = i;

	/* Label each using the first member */
	for (i = 0; i < n-k; ++i)
	{
		L[n+i] = L[(size_t)Z[i + 0*(n-1)]];
		if (L[(size_t)Z[i + 1*(n-1)]] < L[n+i])
			L[n+i] = L[(size_t)Z[i + 1*(n-1)]];
	}

	/* Propagate labels down the tree from the cut */
	for (i = n-k; i-- > 0;)
	{
		L[(size_t)Z[i + 0*(n-1)]] = L[n+i];
		L[(size_t)Z[i + 1*(n-1)]] = L[n+i];
	}

	/* Compress labels from [0,n) to [0,k) */
	seen = 0;
	for (i = 0; i < n; ++i)
		if (L[i] == i)
			L[i] = seen++;
		else
			L[i] = L[L[i]];
}
