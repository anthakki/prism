#ifndef LIBPRISM_DECOM_H_
#define LIBPRISM_DECOM_H_

#include "_sparse.h"
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Partially blind decomposition using a Poisson model */
void prism_decom_poi(double *t_Z, double *WX, const double *t_X, const double *t_Y, const double *WY, size_t m, size_t k, size_t r, size_t n, size_t max_iters, double min_delta);
void prism_decom_poi_sparse(double *t_Z, double *WX, const double *t_X, prism_opt_sparse_t t_Y, prism_opt_sparse_t WY, size_t m, size_t k, size_t r, size_t n, size_t max_iters, double min_delta);

/* Partially blind decomposition using a scaled Poisson model */ 
void prism_decom_spoi(double *t_Z, double *t_T, double *WX, const double *t_X, const double *t_Y, const double *WY, size_t m, size_t k, size_t r, size_t n, size_t max_iters, double min_delta);
void prism_decom_spoi_sparse(double *t_Z, double *t_T, double *WX, const double *t_X, prism_opt_sparse_t t_Y, prism_opt_sparse_t WY, size_t m, size_t k, size_t r, size_t n, size_t max_iters, double min_delta);

/* Partially blind decomposition using a normal (least-squares) model */
void prism_decom_l2(double *t_Z, double *WX, const double *t_X, const double *t_Y, const double *WY, size_t m, size_t k, size_t r, size_t n, size_t max_iters, double min_delta);
void prism_decom_l2_sparse(double *t_Z, double *WX, const double *t_X, prism_opt_sparse_t t_Y, prism_opt_sparse_t WY, size_t m, size_t k, size_t r, size_t n, size_t max_iters, double min_delta);

#ifdef __cplusplus
}
#endif

#endif /* LIBPRISM_DECOM_H_ */
