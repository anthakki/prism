
#include "sort.h"
#include <assert.h>
#include <math.h>

/* Sorting predicate-- mostly to stand out */
#define LESS(x, y) ((x) < (y))

/* Threshold for switching to insertion sort */
#define INSERTION_SORT_TH (32)

static
void
insertion_sort_d(double *x, size_t n)
{
	size_t i, j;
	double xi;

	assert(x != NULL || n < 1);

	/* Pass */
	for (i = 1; i < n; ++i)
	{
		/* Lift off the pivot */
		xi = x[i];

		/* Rotate larger elements right */
		for (j = i; j > 0 && LESS(xi, x[j-1]); --j)
			x[j] = x[j-1];

		/* Put the pivot back */
		x[j] = xi;
	}
}

static
double
choosepivot_d(double *x, size_t n)
{
	double a, b, c, tmp;

	assert(n > 0);

	/* Pick a ~ first, b ~ middle, c ~ last */
	a = x[0];
	b = x[0 + ((n-1)-0)/2];
	c = x[n-1];

	/* Sort a, c */
	if (LESS(c, a))
	{
		tmp = a;
		a = c;
		c = tmp;
	}

	/* Pick median */
	if (!LESS(b, c))
		return c;
	if (!LESS(a, b))
		return a;
	else
		return b;
}

static
size_t
partition_lower_d(double *x, double v, size_t n)
{
	size_t i;
	double tmp;

	assert(x != NULL || n < 1);

	/* Partition lesser elements to the left */
	for (i = 0; i < n;)
		if (LESS(x[i], v))
			/* Move along */
			++i;
		else
		{
			/* Pop a slot */
			--n;

			/* Swap it out */
			tmp = x[i];
			x[i] = x[n];
			x[n] = tmp;
		}

	return n;
}

static
size_t
partition_upper_d(double *x, double v, size_t n)
{
	size_t i;
	double tmp;

	assert(x != NULL || n < 1);

	/* Partition less or equal elements to the left */
	for (i = 0; i < n;)
		if (!LESS(v, x[i]))
			/* Move along */
			++i;
		else
		{
			/* Pop a slot */
			--n;
			
			/* Swap our element out */
			tmp = x[i];
			x[i] = x[n];
			x[n] = tmp;
		}

	return n;
}

static
void
quicksort_d(double *x, size_t n)
{
	size_t p, q;
	double xp;

	assert(x != NULL || n < 1);

	/* Loop */
	while (n > INSERTION_SORT_TH)
	{
		/* Pick a pivot & partition the data on it */
		xp = choosepivot_d(x, n);
		q = partition_upper_d(x, xp, n);
		p = partition_lower_d(x, xp, q);

		/* Sort each partition */
		if (!(n-q > p))
		{
			/* Recurse on right partition */
			quicksort_d(&x[q], n-q);

			/* Continue work on left */
			n = p;
		}
		else
		{
			/* Recurse left */
			quicksort_d(x, p);

			/* Continue on right */
			x = &x[q];
			n = n-q;
		}
	}

#if INSERTION_SORT_TH > 1
	/* Do a final insertion sort pass */
	insertion_sort_d(x, n);
#endif
}

void
sort_d(double *x, size_t n)
{
	assert(x != NULL || n < 1);

	/* Sort data */
	quicksort_d(x, n);
}

static
void
quickselect_d(double *x, size_t k, size_t n)
{
	size_t p, q;
	double xp;

	assert(x != NULL || n < 1);
	assert(k < n);

	/* Loop */
	while (n > INSERTION_SORT_TH)
	{
		/* Pick a pivot & partition the data on it */
		xp = choosepivot_d(x, n);
		q = partition_upper_d(x, xp, n);
		p = partition_lower_d(x, xp, q);
		
		/* Branch */
		if (k < p)
			/* Continue on left */
			n = p;
		else if (!(k < q))
			/* Continue on right */
		{
			x = &x[q];
			k = k-q;
			n = n-q;
		}
		else
			/* The sought element is withing the range-- we're done */
			return;
	}

#if INSERTION_SORT_TH > 1
	/* Do a final insertion sort pass */
	insertion_sort_d(x, n);
#endif
}

void
nth_element_d(double *x, size_t p, size_t n)
{
	assert(x != NULL || n < 1);
	assert(p <= n);

	/* Locate the element */
	if (p < n)   /* NB. handle trivial case p = n */
		quickselect_d(x, p, n);
}
