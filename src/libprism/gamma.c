
#include "gamma.h"
#include "math99.h"
#include <assert.h>
#include <math.h>

#define countof(x) \
	(sizeof((x)) / sizeof(*(x)))

double
gammaln(double x)
{
	/* Coefficients from the GNU Scientific Library */

	static const double p[] = { 676.5203681218851, -1259.1392167224028,
		771.32342877765313, -176.61502916214059, 12.507343278686905,
		-0.13857109526572012, 9.9843695780195716e-6, 1.5056327351493116e-7 };

	double sum, t;
	int i;

	if (x < .5)
		return log(M_PI) - log(fabs(sin(M_PI*x))) - gammaln(1.-x);

	x -= 1;
	sum = 0.99999999999980993;
	for (i = 0; i < (int)countof(p); ++i)
		sum += p[i] / (x+i+1);
	t = x + countof(p) - 0.5;

	return .5*log(2*M_PI) + (x+.5)*log(fabs(t)) - t + log(fabs(sum));
}

#define EPSILON (0.)

double
gamma_p_log(double x, double s)
{
	double log_ft, r, c, pws;

	if (x == 0.)
		return 0.;
	if (!(x >= 0. && s > 0.))
		return (double)NAN;

	if (x > 1.1 && x > s)
		return log1p(-gamma_q(x, s));

	log_ft = s * log(x) - x - gammaln(s);
	r = s;
	c = 1.;
	pws = 1.;

	do
	{
		r += 1.;
		c *= x/r;
		pws += c;
	}
	while (c / pws > EPSILON);

	return log(pws) + log_ft - log(s);
}

double
gamma_p(double x, double s)
{ return exp(gamma_p_log(x, s)); }

double
gamma_q(double x, double s)
{ return exp(gamma_q_log(x, s)); }

double
gamma_q_log(double x, double s)
{
	double f, C, D, a, b, chg;
	int i;

	if (!(x > 1.1 && x > s))
		return log1p(-gamma_p(x, s));

	f = 1 + x - s;
	C = f;
	D = 0;
	for (i = 1; i < 10000; ++i)
	{
		a = i * (s - i);
		b = (i<<1) + 1 + x - s;
		D = b + a * D;
		C = b + a / C;
		D = 1 / D;
		chg = C * D;
		f *= chg;
		if (fabs(chg - 1.) < EPSILON)
			break;
	}

	return s * log(x) - x - gammaln(s) - log(f);
}
