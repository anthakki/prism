#ifndef SRC_LIBPRISM_HIERARCH_H_
#define SRC_LIBPRISM_HIERARCH_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Finds a linkage tree using the O(n^2) RNN algorithm */
void linkage_rnn(size_t n, void *scratch, double *Z, double *D, void *cookie,
	void (*swap)(size_t i, size_t j, void *cookie),
	void (*merge)(size_t i, size_t j, double *D, void *cookie));

/* In the above, the callback swap is used to notify the user that clusters at
 * indices i, j change their roles. The user must permute the statistics in
 * the cookie accordingly. The callbacks are only called for i < j. Scratch
 * must have room for 2*n-1 doubles and is used internally. */

/* The callback merge is used to merge the clusters i,j. The user must merge
 * the distances D[k+i*(i-1)/2] and D[k+j*(j-1)/2], for clusters i, j,
 * respectively, for all k in [0,i) into the former array, and merge the
 * statistics in the cookie accordingly (i.e. fuse j into i). The callback is
 * only called for i < j. */

/* Moreover, for the RNN algorithm to work, the distances must be reducible,
 * that is, for all i, j, k where a t exist such that d(i,j) < t and d(i,k),
 * d(j,k) > t implies d(i u j, k) > t. */ 

/* Ward's method: set up */
void ward_init(size_t m, size_t n, double *w, double *D, const double *X);
/* Ward's method: swap clusters */
void ward_swap(size_t i, size_t j, void *cookie);
/* Ward's method: merge clusters */
void ward_merge(size_t i, size_t j, double *D, void *cookie);

/* Cut the linkage tree */
void cutree(size_t *L, const double *Z, size_t k, size_t n);

#ifdef __cplusplus
}
#endif

#endif /* SRC_LIBPRISM_HIERARCH_H_ */
