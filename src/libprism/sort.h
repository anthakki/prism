#ifndef SRC_LIBPRISM_SORT_H_
#define SRC_LIBPRISM_SORT_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* NOTE: NaN behavior is undefined on these-- sort NaNs yourself */

/* Sort numeric data */
void sort_d(double *x, size_t n);

/* Partial sort */
void partial_sort_d(double *x, size_t p, size_t n);
/* Locate a single element */
void nth_element_d(double *x, size_t p, size_t n);

/* Binary search: lower bound */
size_t lower_bound_d(const double *x, double v, size_t n);
/* Binary search: upper bound */
size_t upper_bound_d(const double *x, double v, size_t n);

#ifdef __cplusplus
}
#endif

#endif /* SRC_LIBPRISM_SORT_H_ */
