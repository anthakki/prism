
#include "_sparse.h"
#include <assert.h>

prism_opt_sparse_t
prism_sparse(const double *x, const size_t *i, const size_t *p)
{
	prism_opt_sparse_t mtx;

	assert(p != NULL);

	/* Fill in the data */
	mtx._x = (double *)x;
	mtx._i = (size_t *)i;
	mtx._p = (size_t *)p;

	return mtx;
}

prism_opt_sparse_t
prism_dense(const double *x)
{
	prism_opt_sparse_t mtx;

	/* Fill in the data */
	mtx._x = (double *)x;
	mtx._i = NULL;
	mtx._p = NULL;

	return mtx;
}

#undef prism_is_sparse
int
prism_is_sparse(prism_opt_sparse_t mtx)
{ return prism_is_sparse_(mtx); }

#undef prism_assert_sparse
int
prism_assert_sparse(prism_opt_sparse_t mtx, size_t m, size_t n)
{ return prism_assert_sparse_(mtx, m, n); }
