#ifndef SRC_LIBPRISM_MATH99_H_
#define SRC_LIBPRISM_MATH99_H_

#include <math.h>

#ifdef __cplusplus
extern "C" {
#endif

/* We need NAN from ISO C99 */
#if !defined(NAN)
#	if defined(__GNUC__) || defined(__clang__)
#		define NAN __builtin_nan("")
#	else
#		define NAN sqrtf(-1.)
#	endif
#endif

/* We need isnan(x) from ISO C99 */
#if !defined(isnan)
#	if defined(__GNUC__) || defined(__clang__)
#		define isnan(x) __builtin_isnan((x))
#	else
#		define isnan(x) (!((x) == (x)))
#	endif
#endif

/* We need M_PI from ISO C99 */
#if !defined(M_PI)
#	define M_PI (4.*atan(1.))
#endif

/* We need log1p(x) from ISO C99 */
#if !defined(log1p)
#	if defined(__GNUC__) || defined(__clang__)
#		define log1p(x) __builtin_log1p((x))
#	else
#		define log1p(x) log((x) + 1.)
#	endif
#endif

#ifdef __cplusplus
}
#endif

#endif /* SRC_LIBPRISM_MATH99_H_ */
