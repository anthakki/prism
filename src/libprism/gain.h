#ifndef LIBPRISM_GAIN_H_
#define LIBPRISM_GAIN_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Rank-1 factorization using a Poisson model */
void prism_gain_poi(double *u, double *v, const double *X, size_t m, size_t n);

/* Rank-1 factorization using a scaled Poisson model */
void prism_gain_spoi(double *u, double *v, const double *X, size_t m, size_t n);

/* Rank-1 factorization using a Normal model (PCA) */
void prism_gain_l2(double *u, double *v, const double *X, size_t m, size_t n);

/* Trimmed version of the rank-1 Poisson model */
void prism_gain_trimmed_poi(double *u, double *v, const double *X, size_t m, size_t n, double alpha);

/* Trimmed version of the rank-1 above scaled Poisson model */
void prism_gain_trimmed_spoi(double *u, double *v, const double *X, size_t m, size_t n, double alpha);

/* Trimmed version of the rank-1 above Normal model */
void prism_gain_trimmed_l2(double *u, double *v, const double *X, size_t m, size_t n, double alpha);

#ifdef __cplusplus
}
#endif

#endif /* LIBPRISM_GAIN_H_ */
