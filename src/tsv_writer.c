
#include "tsv_writer.h"
#include <assert.h>
#include <float.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static
int
write_escaped(FILE *stream, const char *data, size_t size)
{
	size_t i;

	assert(stream != NULL);
	assert(data != NULL || size < 1);

	/* Write data */
	for (i = 0; i < size; ++i)
	{
		/* Escape quote */
		if (((const char *)data)[i] == '\"')
			if (fputc('\"', stream) == EOF)
				return -1;

		/* PAss through */
		if (fputc(((const char *)data)[i], stream) == EOF)
			return -1;
	}

	return 0;
}

int
tsv_write_field(FILE *stream, size_t *col, const void *data, size_t size)
{
	assert(stream != NULL);
	assert(col != NULL);
	assert(data != NULL || size < 1);

	/* Put leading separator? */
	if ((*col)++ > 0)
		if (fputc('\t', stream) == EOF)
			return -1;

	/* Quote *" */
	if (fputc('\"', stream) == EOF)
		return -1;

	/* Write value */
	if (write_escaped(stream, (const char *)data, size) != 0)
		return -1;

	/* Unquote */
	if (fputc('\"', stream) == EOF)
		return -1;

	return 0;
}

int
tsv_write_fieldv(FILE *stream, size_t *col, const void *data, size_t size, ...)
{
	va_list va;

	assert(stream != NULL);
	assert(col != NULL);
	assert(data != NULL || size < 1);

	/* Put leading separator? */
	if ((*col)++ > 0)
		if (fputc('\t', stream) == EOF)
			return -1;

	/* Quote */
	if (fputc('\"', stream) == EOF)
		return -1;

	/* Write values */
	va_start(va, size);
	for (;;)
	{
		/* Write */
		if (write_escaped(stream, data, size) != 0)
			return -1;

		/* Pull next value */
		data = va_arg(va, const char *);
		if (data == NULL)
		{
			va_end(va);
			break;
		}
		size = va_arg(va, size_t);
	}

	/* Unquote */
	if (fputc('\"', stream) == EOF)
		return -1;

	return 0;
}

static
size_t
dtoa(char *dest, double value)
{
	assert(dest != NULL);

	/* TODO: lossless print */

	return (size_t)sprintf(dest, "%.*g", DBL_DIG, value);
}

int 
tsv_write_number(FILE *stream, size_t *col, double value) 
{
	char data[128];
	size_t size;

	assert(stream != NULL);

	/* Put leading separator? */
	if ((*col)++ > 0)
		if (fputc('\t', stream) == EOF)
			return -1;

	/* Format & write */
	size = dtoa(data, value);
	if (fwrite(data, 1, size, stream) != size)
		return -1;

	return 0;
}

int
tsv_write_newline(FILE *stream, size_t *col)
{
	assert(stream != NULL);
	assert(col != NULL);

	/* Write file */
	if (fputc('\n', stream) == EOF)
		return -1;

	/* Reset column counter */
	*col = 0;

	return 0;
}

const char *
tsv_write(const char *filename, const double *X, size_t m, size_t n, int flags, const void *colnames, const void *rownames)
{
	FILE *file;
	size_t s0, s1, col, i, j, size;

	assert(filename != NULL);
	assert(X != NULL || m*n < 1);

	/* Open the file */
	file = fopen(filename, "w");
	if (file == NULL)
		return "failed to open for write";

	/* Set up strides */
	s0 = n, s1 = 1;
	if ((flags & TSV_READER_TRANSP) != 0)
		s0 = 1, s1 = m;

	/* Set up column counter */
	col = 0;

	/* Write column names */
	if (colnames != NULL && (flags & TSV_READER_COLNAMES) != 0)
	{
		const void *it;
		const char *name;

		/* Write top left element */
		if (rownames != NULL && (flags & TSV_READER_ROWNAMES) != 0)
			if (tsv_write_field(file, &col, "", 0) != 0)
				goto write_error;

		/* Set up iterator */
		it = colnames;

		/* Loop */
		for (j = 0; j < n; ++j)
		{
			name = tsv_names_next(&size, &it);
			if (tsv_write_field(file, &col, name, size) != 0)
				goto write_error;
		}

		/* Next line */
		if (tsv_write_newline(file, &col) != 0)
			goto write_error;
	}

	/* Write data rows */
	{{
		const void *it;
		const char *name;

		/* Set up row name iterator */
		it = NULL;
		if (rownames != NULL && (flags & TSV_READER_ROWNAMES) != 0)
			it = rownames;

		/* Loop */
		for (i = 0; i < m; ++i)
		{
			/* Write header */
			if (it != NULL)
			{
				name = tsv_names_next(&size, &it);
				if (tsv_write_field(file, &col, name, size) != 0)
					goto write_error;
			}

			/* Write payload */
			for (j = 0; j < n; ++j)
				if (tsv_write_number(file, &col, X[i*s0 + j*s1]) != 0)
					goto write_error;

			/* Next line */
			if (tsv_write_newline(file, &col) != 0)
			{
write_error:
				(void)fclose(file);

				return "write error";
			}
		}
	}}

	/* Close the file */
	if (fclose(file) != 0)
		return "write error";

	return NULL;
}

const void *
tsv_names_put(const void *it, const char *name)
{
	size_t size;

	assert(name != NULL);

	/* TODO: not re-entrant when it = NULL */

	/* If it is not given, use a small static buffer */
	if (it == NULL)
	{
		static char data[64];
		it = data;
	}

	/* Get size */
	size = strlen(name);

	/* Copy in the stuff */
	memcpy((void *)it, &size, sizeof(size));
	memcpy((char *)it + sizeof(size), name, size);

	return it;
}

const void *
tsv_names_putseq(const char *prefix, size_t n)
{
	size_t prefix_size, size, p, i;
	char *data;

	/* Empty prefix? */
	if (prefix == NULL)
		prefix = "";

	/* Compute an upper bound for the size */
	prefix_size = strlen(prefix);
	size = (prefix_size + 32 + sizeof(size)) * n;

	/* Allocate buffer */
	data = (char *)malloc(size);
	if (data == NULL)
		return NULL;

	/* Copy in the data */
	p = 0;
	for (i = 0; i < n; ++i)
	{
		/* Write payload */
		size = (size_t)sprintf(&data[p + sizeof(size)], "%s%lu",
			prefix, (unsigned long)(i + 1));

		/* Write size */
		memcpy(&data[p], &size, sizeof(size));

		/* Move along */
		p += sizeof(size) + size;
	}

	return data;
}
