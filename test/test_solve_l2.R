#!/usr/bin/env Rscript

#
# test the matrix solvers
#

library('prism', lib.loc = './r/library/')

### test general L2 formula

set.seed(123L)
m <- 103L
n <- 13L
r <- 53L
X <- matrix( runif(m*n), m, n )
Y <- matrix( runif(n*r), n, r )

# forward compute
Z <- X %*% Y

# test Poisson solver
Y.hat <- prism.solve.poi(X, Z)
stopifnot( isTRUE(all.equal( Y.hat, Y, tol = 1e-4 )) )

# test normal solver
Y.hat <- prism.solve.l2(X, Z)
stopifnot( isTRUE(all.equal( Y.hat, Y, tol = 1e-4 )) )

### done

cat('success', sep = '\n', file = stderr())

