#!/bin/sh

set -e

for test in test/test_*; do
	./"$test"
done

echo 'ALL OK'
