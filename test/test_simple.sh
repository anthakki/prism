#!/bin/sh

set -e

# generate data
./test/simple-generate.R

# estimate gains
./prism-gain -H test/simple-Y.tsv -G test/simple-GY.est.tsv
# cluster
./prism-clust -H test/simple-Y.tsv -g test/simple-GY.est.tsv -W test/simple-WY.est.tsv
# decompose
./prism-decom -H test/simple-X.tsv test/simple-Y.tsv -g test/simple-GY.est.tsv -w test/simple-WY.est.tsv \
	-Z test/simple-Z.est.tsv -G test/simple-GX.est.tsv -W test/simple-WX.est.tsv

# run checks
./test/simple-check.R

# win! 
echo 'success'
