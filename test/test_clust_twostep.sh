#!/bin/sh

set -e

#
# check that the two step with tree readback works..
#

# generate data
./test/simple-generate.R

# cluster
./prism-clust -H test/simple-Y.tsv -g test/simple-GY.ref.tsv -W test/simple-WY-1.est.tsv

# cluster in two steps
./prism-clust -H test/simple-Y.tsv -g test/simple-GY.ref.tsv -T test/simple-TY.est.tsv
./prism-clust -H test/simple-Y.tsv -t test/simple-TY.est.tsv -W test/simple-WY-2.est.tsv

# compare
diff -q test/simple-WY-1.est.tsv test/simple-WY-2.est.tsv || exit 1

#
# test for a non-ultrametric case in clustering
#

# cluster in two steps
./prism-clust test/nonultra.tsv -T test/nonultra-tree.tsv
./prism-clust test/nonultra.tsv -t test/nonultra-tree.tsv -W /dev/null

echo 'success'
