#!/bin/sh

# generate data
cat >test/sparse-X.tsv <<-EOF
	""	"A"	"B"
	"x"	1	2
	"y"	3	4
	"z"	5	6
EOF
cat >test/sparse-Y.tsv <<-EOF
	""	"a"	"b"	"c"	"d"	"e"
	"x"	0	0	3	5	0
	"y"	1	0	0	0	0
	"z"	0	2	0	0	5
EOF
cat >test/sparse-w-20.tsv <<-EOF
	""	"1"	"2"	"3"	"4"	"5"
	"a"	1	0	0	0	0
	"b"	0	1	0	0	0
	"c"	1	0	0	0	0
	"d"	1	0	0	0	0
	"e"	0	1	0	0	0
EOF
cat >test/sparse-w-50.tsv <<-EOF
	""	"1"	"2"
	"a"	1	0
	"b"	0	1
	"c"	1	0
	"d"	1	0
	"e"	0	1
EOF
cat >test/sparse-GY.tsv <<-EOF
	""	"a"	"b"	"c"	"d"	"e"
	"gain"	1	2	3	4	5
EOF

# write reference data
cat >test/sparse-out-20-poi.txt <<-EOF
	""	"A.1"	"A.2"	"A.3"	"A.4"	"A.5"	"B.1"	"B.2"	"B.3"	"B.4"	"B.5"
	"x"	1	0	0	0	0	2	0	0	0	0
	"y"	3	0	0	0	0	4	0	0	0	0
	"z"	0	5	0	0	0	0	6	0	0	0
	""	"A"	"B"
	"gain"	8.55555555555556	11.3333333333333
	""	"1"	"2"	"3"	"4"	"5"
	"A"	0.415584415584416	0.584415584415584	0	0	0
	"B"	0.470588235294118	0.529411764705882	0	0	0
EOF
cat >test/sparse-out-50-poi.txt <<-EOF
	""	"A.1"	"A.2"	"B.1"	"B.2"
	"x"	1	0	2	0
	"y"	3	0	4	0
	"z"	0	5	0	6
	""	"A"	"B"
	"gain"	8.55555555555556	11.3333333333333
	""	"1"	"2"
	"A"	0.415584415584416	0.584415584415584
	"B"	0.470588235294118	0.529411764705882
EOF
cat >test/sparse-out-20-spoi.txt <<-EOF
	""	"A.1"	"A.2"	"A.3"	"A.4"	"A.5"	"B.1"	"B.2"	"B.3"	"B.4"	"B.5"
	"x"	1	0	0	0	0	2	0	0	0	0
	"y"	3	0	0	0	0	4	0	0	0	0
	"z"	0	5	0	0	0	0	6	0	0	0
	""	"A"	"B"
	"gain"	13.6666666666667	18
	""	"1"	"2"	"3"	"4"	"5"
	"A"	0.634146341463415	0.365853658536585	0	0	0
	"B"	0.666666666666667	0.333333333333333	0	0	0
EOF
cat >test/sparse-out-50-spoi.txt <<-EOF
	""	"A.1"	"A.2"	"B.1"	"B.2"
	"x"	1	0	2	0
	"y"	3	0	4	0
	"z"	0	5	0	6
	""	"A"	"B"
	"gain"	13.6666666666667	18
	""	"1"	"2"
	"A"	0.634146341463415	0.365853658536585
	"B"	0.666666666666667	0.333333333333333
EOF
cat >test/sparse-out-20-l2.txt <<-EOF
	""	"A.1"	"A.2"	"A.3"	"A.4"	"A.5"	"B.1"	"B.2"	"B.3"	"B.4"	"B.5"
	"x"	1.05436640824539	0.618007034031887	-0.224124480759093	-0.224124480759093	-0.224124480759093	1.41268336979365	0.895921631909437	-0.102868333901027	-0.102868333901027	-0.102868333901027
	"y"	1.09365055651282	1.12748323887682	0.259622068203451	0.259622068203451	0.259622068203451	1.39415710600675	1.42709310303591	0.392916596985778	0.392916596985778	0.392916596985778
	"z"	1.39924057205372	1.91352293887984	0.562412163022145	0.562412163022145	0.562412163022145	1.6863662593083	2.27749505444224	0.678712895416486	0.678712895416486	0.678712895416486
	""	"A"	"B"
	"gain"	2.56369356128846	3.0167068179312
	""	"1"	"2"	"3"	"4"	"5"
	"A"	0.484865198703537	0.515134801296463	0	0	0
	"B"	0.486619667674558	0.513380332325442	0	0	0
EOF
cat >test/sparse-out-50-l2.txt <<-EOF
	""	"A.1"	"A.2"	"B.1"	"B.2"
	"x"	1.08756936541016	-0.0875693654101649	1.80435625947778	0.195643740522218
	"y"	1.38532973733729	1.61467026266271	1.86563820448908	2.13436179551092
	"z"	1.4961085520094	3.5038914479906	1.76735756238911	4.23264243761089
	""	"A"	"B"
	"gain"	4.07179197777392	5.37231868958731
	""	"1"	"2"
	"A"	0.409634341103949	0.590365658896051
	"B"	0.42845668733984	0.57154331266016
EOF

# dense, sparse w, sparse Y,w
for method in poi spoi l2; do
	for th in 0 .25 .5; do
		if ! err="$( ./prism-decom -H -k1 -d"$th" -m"$method" 'test/sparse-X.tsv' 'test/sparse-Y.tsv' -w 'test/sparse-w-20.tsv' -g 'test/sparse-GY.tsv' -Z '/dev/stdout' -G '/dev/stdout' -W '/dev/stdout' | Rscript test/diff-tables.R "test/sparse-out-20-$method.txt" /dev/stdin 2>&1 )"; then
			echo "-d$th -m$method: $err" >&2
			exit 1
		fi
	done
done

# dense, sparse Y, sparse Y,w
for method in poi spoi l2; do
	for th in 0 .375 .75; do
		if ! err="$( ./prism-decom -H -k1 -d"$th" -m"$method" 'test/sparse-X.tsv' 'test/sparse-Y.tsv' -w 'test/sparse-w-50.tsv' -g 'test/sparse-GY.tsv' -Z '/dev/stdout' -G '/dev/stdout' -W '/dev/stdout' | Rscript test/diff-tables.R "test/sparse-out-50-$method.txt" /dev/stdin 2>&1 )"; then
			echo "-d$th -m$method: $err" >&2
			exit 1
		fi
	done
done

echo 'success' >&2
