#!/bin/sh

shell() {
	echo "$@" >&2
	"$@"
}

shell rm -f test/nonultra-tree.tsv
shell rm -f test/simple-*.tsv
shell rm -f test/sparse-*.tsv test/sparse-out-*.txt
