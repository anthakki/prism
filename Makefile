
# settings
CC = cc
AR = ar -c
RM = rm -f
INSTALL = install
SED = sed
PANDOC = pandoc
prefix = /usr/local

# read configuration to override defaults
-include Makefile.config

# set up files for libprism
lib_HEADERS := $(wildcard src/libprism/*.h)
lib_SOURCES := $(wildcard src/libprism/*.c)
lib_OBJECTS = $(patsubst %.c,%.o,$(lib_SOURCES))

targets += libprism.a libprism.so libprism.h
sources += $(lib_SOURCES)
objects += $(lib_OBJECTS)

# set up files for the cli programs
progs = prism-gain prism-clust prism-decom
bin_SOURCES = $(patsubst %,src/%.c,$(progs))
bin_OBJECTS = $(patsubst %.c,%.o,$(bin_SOURCES))

supp_SOURCES = $(filter-out $(bin_SOURCES),$(wildcard src/*.c))
supp_OBJECTS = $(patsubst %.c,%.o,$(supp_SOURCES))

targets += $(progs) prism.pc
sources += $(bin_SOURCES) $(supp_SOURCES)
objects += $(bin_OBJECTS) $(supp_OBJECTS)

# set up files for man pages
mantargets = $(patsubst %,man/%.1,$(progs))

# rules

all: $(targets)

man: $(mantargets)

cleanobj:
	$(RM) $(objects)

cleanman:
	$(RM) $(mantargets)

clean: cleanobj cleanman
	$(RM) $(targets)

distclean: clean
	$(RM) Makefile.config Makefile.depends

install: all
	$(INSTALL) -m 755 -d $(prefix)/bin/
	$(INSTALL) -m 755 $(progs) $(prefix)/bin/
	$(INSTALL) -m 755 -d $(prefix)/share/pkgconfig/
	$(INSTALL) -m 644 prism.pc $(prefix)/share/pkgconfig/

src/libprism/%.o: src/libprism/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

src/%.o: src/%.c
	$(CC) $(CFLAGS) $(zlib_CFLAGS) -c -o $@ $<

%.1: %.1.md
	$(PANDOC) --standalone --to man $< -o $@

-include Makefile.depends

libprism.a: $(lib_OBJECTS)
	$(AR) -r $@ $(lib_OBJECTS)

libprism.so: $(lib_OBJECTS)
	$(CC) $(CFLAGS) -shared -o $@ $(lib_OBJECTS) $(LIBS)

libprism.h: $(lib_HEADERS)
	$(SED) '/#ifndef SRC_/,/#endif \/\* SRC_/d; /^#include "_.*[.]h"/d;' $(lib_HEADERS) >$@

prism-%: src/prism-%.o $(supp_OBJECTS) libprism.a libprism.h
	$(CC) $(CFLAGS) $(zlib_CFLAGS) -o $@ $< $(supp_OBJECTS) libprism.a $(LIBS) $(zlib_LIBS)

prism.pc: prism.pc.in
	$(SED) 's#@prefix@#$(prefix)#g; s#@Libs.private@#$(LIBS) $(openmp_LIBS)#g;' $< >$@

Makefile.config Makefile.depends &: configure
	./configure

.EXTRA_PREREQS += $(MAKEFILE_LIST)

.PHONY: all cleanobj clean distclean cleanman man install
.SUFFIXES:
