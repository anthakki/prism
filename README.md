
PRISM -- Poisson RNA-profile Identification in Scaled Mixtures
==============================================================

PRISM is statistical framework that allows simultaneous extraction of a tumor sample composition and cell type and sample specific whole-transcriptome profiles from individual bulk RNA-seq samples, by exploiting single-cell reference data. This facilitates a separate analysis of cancer cell and the microenvironment phenotypes and of the tumor composition.

Major features:

- Estimates both the sample composition (i.e. fraction of the constituent cell types) and expression profiles (i.e. RNA-seq profiles that you would get if you removed the other cell types) at individual bulk sample level

- The single-cell reference are treated as data and are subject to the statistical model, which allows the bulk specific profiles to adapt to unmatched patients

- An arbitrary number of composing cell types are supported, and the level of the decomposition can be specified by the user or the cell types can be automatically discovered through clustering 

- The model accounts for the discrete nature of RNA-seq data and models both expression and technical noise or the lack of thereof, with different genes and cell types having varying degrees of variation

For the details about the method and validation, please refer to our [publication][1] on the matter.

[1]: https://doi.org/10.1093/bioinformatics/btab178

Installation:
-------------

PRISM can be compiled into a set of standalone programs (in which case R is not needed) or used through R interfaces.

To build PRISM, you will need an ISO C90 (or ANSI C89) compiler (such as [GCC](https://gcc.gnu.org/) or [Clang](https://clang.llvm.org/)), `alloca.h` (which is nonstandard but comes with most compilers), [GNU make](https://www.gnu.org/software/make/), and [zlib](https://www.zlib.net/). There is a good chance that these are already installed in your Linux or macOS system, especially if you have developer specific tools. If you wish to exploit parallelization (optional), ensure that your C compiler supports [OpenMP](https://www.openmp.org/), and check that the corresponding flag is set in `Makefile.config`. If you want to build the documentation (optional), you will need [Pandoc](https://pandoc.org/).

PRISM has been tested on Ubuntu 16.04 with gcc 5.4.0 (with OpenMP 4.0), gmake 4.1, and zlib 1.2.8.

To build the binaries, run (this should take a few seconds):

```
	./configure && make
```

This will build a static library `libprism.a`, a shared library `libprism.so`, and the command line interface, which consist of the driver programs [**prism-gain**](man/prism-gain.1.md), [**prism-clust**](man/prism-clust.1.md) and [**prism-decom**](man/prism-decom.1.md), which link `libprism` statically. Install the binaries in your preferred location (e.g. `/usr/local/bin/`):

```
	sudo make install prefix=/usr/local 
```

Please refer to the following section and the tool specific manual pages for example usage and parameter choices.

PRISM is also available as an [Anduril component](https://www.anduril.org/), which facilitates integration with other tools.

Note for Apple users: Apple's version of Clang does _not_ support OpenMP out of the box. Easiest way to overcome this is to install LLVM's `libomp` from [Homebrew](https://brew.sh/) (`brew install libomp`), which PRISM should now detect automatically. The flags `openmp_CFLAGS` and `openmp_LIBS` can be customized if `libomp` is installed into a nonstandard location. Other options are to install a vanilla Clang or GCC, which are available from the links above or also from Homebrew.

General usage:
--------------

It is up to the user to harmonize the datasets. While headers, such as gene and sample names are allowed through the **-H** option, and will be propagated through the analysis, the rows and columns are not permuted into a matching order but are assumed to already be so.

Generally, the input data must be in the form of raw counts (i.e. not transformed, not normalized) for the models to be meaningful. This is not enforced either. Virtual fractional counts (e.g. expectations of a discrete distribution) are allowed throughoutly, as these are commonly provided by RNA quantification methods, such as [eXpress](https://dx.doi.org/10.1038/nmeth.2251), and are also output by PRISM. RNA-seq proprocessing such as sequence trimming, alignment and quality control should be done prior to running the analysis and is out of scope of this document.

The PRISM analysis requires and an `m`-by-`r` matrix of the bulk expression profiles to be decomposed, and an `m`-by-`n` matrix of the reference single-cell profiles, where `m` represents the number of genes (or RNA isoforms), `r` the number of bulk profiles, and `n` the number of single-cell profiles. Even though the bulk profiles are arranged in a single matrix, each is analyzed independently. Each file is encoded as tab separated text and can be [gzip](https://www.gzip.org/) compressed.

The PRISM analysis commonly consists of the following three steps. Alternatively, if the single-cell sample scale factors are already known or if the single-cell profiles are already labeled, steps 1 or 2 can be omitted, respectively.

**Step 1**: Estimate scale factors for the single-cell reference using [**prism-gain**](man/prism-gain.1.md) to correct for unequal total RNA. Do note that the absolute scale in general cannot be inferred, so the output magnitude is arbitrary and subject to change, and it is typical to enforce a canonical scaling using the **-n** option. This will read the expression data from the matrix `sc_expr.tsv`, factorize it, and write the scale factors to the output file `sc_gains.tsv`:

```
	prism-gain -H sc_expr.tsv -G sc_gains.tsv
```

**Step 2**: Cluster single-cell data using [**prism-clust**](man/prism-clust.1.md) in order to find the constituent phenotypes. This will build a hierarchical clustering tree `sc_tree.tsv` from the expression data `sc_expr.tsv`, and the cut the tree using [Bayesian Information Criterion](https://doi.org/10.1093/biomet/92.4.937) (**-s bic**) into an adjacency matrix suitable for `prism-decom` which is output into `sc_weights.tsv`:

```
	prism-clust -H sc_expr.tsv -g sc_gains.tsv -T sc_tree.tsv
	prism-clust -H sc_expr.tsv -t sc_tree.tsv -s bic -W sc_weights.tsv
```

**Step 3**: Decompose bulk expression profiles using [**prism-decom**](man/prism-decom.1.md). This will decompose each mixture expression profile from `bulk_expr.tsv` into the cell type specific profiles adapted for each bulk `decom_bulk_expr.tsv` using the single-cell reference. `sc_gains.tsv` are the scale factors and `sc_weights.tsv` the cell type adjacency matrix, as computed above, while the analysis outputs the scale factors for the bulk profiles in `bulk_gains.tsv`, and the estimated composition of the bulk profiles in `bulk_weights.tsv`.

```
	prism-decom -H bulk_expr.tsv sc_expr.tsv -g sc_gains.tsv -w sc_weights.tsv \
		-Z decom_bulk_expr.tsv -G bulk_gains.tsv -W bulk_weights.tsv 
```

The decomposed profiles `decom_bulk_expr.tsv` can be used e.g. for performing a cell type specific differential expression analysis instead of the raw bulk data `bulk_expr.tsv`, e.g. using [DESeq2](https://doi.org/10.1186/s13059-014-0550-8), or either the expression features or compositional features can be directly associated with clinical features. Typically, a product of the gain factors `bulk_gains.tsv` and the composition `bulk_weights.tsv` are used as scale factors in subsequent analysis. 

If you want a concrete example with numbers, please walk through [test/test_simple.sh](test/test_simple.sh). This will generate some data, and use the command line interface to perform a full analysis, and can be used to verify that PRISM is functioning correctly. To run this test, issue `./test/test_simple.sh` (this will take several seconds and print `success`).

If PRISM is compiled with OpenMP, you can control the degree of parallelization by setting the environment varible [`OMP_NUM_THREADS`](https://www.openmp.org/wp-content/uploads/openmp-4.5.pdf#section.4.2).

Using PRISM through R:
----------------------

First, build the R package from the directory [`r/`](r/) of the PRISM repository and install it in your R instance using the following commands. This requires that you have [R](https://www.r-project.org/) and the [`roxygen2`](https://cran.r-project.org/web/packages/roxygen2/index.html) package installed.

```
	./configure
	make -C r install
```

In your R script, load the PRISM package:

```
	library(prism)
```

This will make various functions `prism.*` available. Use the R help browser
(e.g. `?prism.decom`) to consult the R specific documentation. The analysis should follow the above steps using the functions [`prism.gain`](r/prism/R/gain.R), [`prism.clust`](r/prism/R/clust.R), [`prism.clust.select`](r/prism/R/clust.R), and [`prism.decom`](r/prism/R/decom.R). A general linear solver is available through [`prism.solve`](r/prism/R/solve.R).

In general, the inputs must be standard R `matrix`es or vectors (not `data.frames` or more complex structures) and `numeric`, (not `integer` or `logical`). Use [`as.matrix`](https://stat.ethz.ch/R-manual/R-patched/library/base/html/matrix.html) and [`as.numeric`](https://stat.ethz.ch/R-manual/R-patched/library/base/html/numeric.html) to cast your data into these types.

Note for Apple ARM users: if you are running emulated R built for x86-64 on an arm64 native system, then PRISM must also be cross-compiled for x86-64. Please substitute `./configure` with:
```
	./configure CC='clang -arch x86_64'
```

Using PRISM through MATLAB:
---------------------------

Rudimentary MATLAB interfaces are also available in the directory [`matlab/`](matlab/). These are mostly intended to support development, so there is minimal documentation, no convenience routines, and performance might be poor, but are suitable for analysis in terms of quality. They invoke the C routines from the shared library `libprism.so` directly, so no further build steps are needed, but you will need to have `libprism.so` in path.

Copying
-------

All files are subject to the simplified BSD license. Please refer to [`LICENSE.txt`](LICENSE.txt) for details. Copyright (c) 2018-2019 Antti Hakkinen.
