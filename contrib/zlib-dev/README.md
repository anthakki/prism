
# zlib stub header

The zlib binaries are provided in the core of many systems, but the headers are
often not included in the default install. To compile against zlib, development
packages (e.g. `zlib-dev` on RHEL derivatives or `zlib1g-dev` on Debian
derivatives) must be installed. Since the zlib ABI is quite stable and only few
features are needed, this stub header can be used instead.

Usage:
```
	./configure zlib_CFLAGS=`./contrib/zlib-dev/zlib-config --cflags` zlib_LIBS=`./contrib/zlib-dev/zlib-config --libs`
```

Use at your own risk!
