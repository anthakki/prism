#ifndef SRC_ZLIB_STUB_H_
#define SRC_ZLIB_STUB_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct gzFile_s *gzFile;

gzFile gzopen(const char *path, const char *mode);

int gzread(gzFile file, void *data, unsigned len);

int gzclose(gzFile);

int gzclose_r(gzFile);

#ifdef __cplusplus
}
#endif

#endif /* SRC_ZLIB_STUB_H_ */
