
#'
#' Invert a linear model
#'
#' @param A The m-by-n design matrix. 
#' @param b The m-by-r response vector set.
#' @param method Specifies the model for the data. Possible values are 'poi'
#'   for Poisson and 'l2' for normal (least-squares)
#'
#' @return x The n-by-r coefficients.
#'
prism.solve <- function(A, b, method = 'poi') {
	return (switch(method,
		poi = prism.solve.poi,
		l2 = prism.solve.l2)(A, b))
}

#' @rdname prism.solve
prism.solve.poi <- function(A, b)
	return (.Call('prism_solve_poi_R', t(A), b))

#' @rdname prism.solve
prism.solve.l2 <- function(A, b)
	return (.Call('prism_solve_l2_R', t(A), b))

# single step of Poisson solver, for internal use
prism.solve.poi.upd <- function(A, b, x)
	return (.Call('prism_solve_poi_upd_R', t(A), b, x))

# single step of normal solver, for internal use
prism.solve.l2.upd <- function(A, b, x)
	return (.Call('prism_solve_l2_upd_R', t(A), b, x))

# unity constrained Poisson solver
prism.solve.poi.con <- function(A, b)
	return (.Call('prism_solve_poi_con_R', t(A), b))

# unity constrained normal solver
prism.solve.l2.con <- function(A, b)
	return (.Call('prism_solve_l2_con_R', t(A), b))
