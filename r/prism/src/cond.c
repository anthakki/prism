
#include <Rinternals.h>
#include <libprism.h>

static
int
ndims(SEXP arg_R)
{
	SEXP dim_R; 

	/* Get dimension vector */
	dim_R = getAttrib(arg_R, R_DimSymbol);
	if (isNull(dim_R))
		return 1;

	return length(dim_R);
}

static
int
n3rdDim(SEXP arg_R)
{
	SEXP dim_R;

	/* Get dimension vector */
	dim_R = getAttrib(arg_R, R_DimSymbol);
	if (isNull(dim_R))
		return 1;
	if (!(length(dim_R) >= 3))
		return 1;

	return INTEGER(dim_R)[2];
}

static
double
safe_div(double x, double y)
{
	if (!(x > 0.))
		return 0.;

	return x / y;
}

static
void
prism_cond_poi(double *scratch, double *Y, const double *X, const double *Z, const double *W, const double *G, size_t m, size_t k, size_t n, size_t Zsj, size_t Wsl, size_t Wsj, size_t Gsj)
{
	double *lams, lam;
	size_t j, i, l;

	/* Designate scratch */
	lams = &scratch[0];

	/* Loop over the data */
	for (j = 0; j < n; ++j)
		for (i = 0; i < m; ++i)
		{
			/* Compute model rates */ 
			lam = 0.;
			for (l = 0; l < k; ++l)
			{
				lams[l] = Z[i + l*m + j*m*k*Zsj] * W[l*Wsl + j*k*Wsj] * G[j*Gsj];
				lam += lams[l];
			}

			/* Compute */
			for (l = 0; l < k; ++l)
				Y[i + l*m + j*m*k] = safe_div( X[i + j*m] * lams[l], lam );
		}
}

SEXP
prism_cond_poi_R(SEXP X_R, SEXP Z_R, SEXP W_R, SEXP G_R)
{
	int m, n, mZ, k, nZ, kW, nW, nG;
	SEXP Y_R;
	double *scratch;

	/* Check inputs */
	if (!(isReal(X_R) && ndims(X_R) <= 2))
		error("'%s' must be a numeric matrix", "X");
	if (!(isReal(X_R) && ndims(Z_R) <= 3))
		error("'%s' must be a numeric array", "Z");
	if (!(isReal(W_R) && ndims(W_R) <= 2))
		error("'%s' must be a numeric matrix", "W");
	if (!(isReal(G_R) && ndims(G_R) <= 2))
		error("'%s' must be a numeric matrix", "G");

	/* Get dimensions */
	m = nrows(X_R);
	n = ncols(X_R);
	mZ = nrows(Z_R);
	k = ncols(Z_R);
	nZ = n3rdDim(Z_R);
	kW = nrows(W_R);
	nW = ncols(W_R);
	nG = length(G_R);

	/* Check dimensions */
	if (!((mZ == m) && (nZ == n || nZ == 1)))
		error("'%s' must be a %d-by-%d-by-%d array", "Z", m, k, n);
	if (!((kW == k || kW == 1) && (nW == n || nW == 1)))
		error("'%s' must be a %d-by-%d matrix", "W", k, n);
	if (!((nG == n || nG == 1) && (nrows(G_R) == nG || ncols(G_R) == nG)))
		error("'%s' must be a %d-vector", "G", n);

	/* Create output */
	Y_R = PROTECT(alloc3DArray(REALSXP, m, k, n));
	scratch = (double *)R_alloc(sizeof(*scratch), k);

	/* Compute */
	prism_cond_poi(scratch, REAL(Y_R), REAL(X_R), REAL(Z_R), REAL(W_R), REAL(G_R),
		(size_t)m, (size_t)k, (size_t)n,
		(size_t)(nZ == n), (size_t)(kW == k), (size_t)(nW == n), (size_t)(nG == n));

	UNPROTECT(1);
	return Y_R;
}

static
void
prism_cond_l2(double *scratch, double *Y, const double *X, const double *Z, const double *W, const double *G, size_t m, size_t k, size_t n, size_t Zsj, size_t Wsl, size_t Wsj, size_t Gsj)
{
	double *lams, lam, bias;
	size_t j, i, l;

	/* Designate scratch */
	lams = &scratch[0];

	/* Loop over the data */
	for (j = 0; j < n; ++j)
		for (i = 0; i < m; ++i)
		{
			/* Compute model rates */ 
			lam = 0.;
			for (l = 0; l < k; ++l)
			{
				lams[l] = Z[i + l*m + j*m*k*Zsj] * W[l*Wsl + j*k*Wsj] * G[j*Gsj];
				lam += lams[l];
			}

			/* Compute */
			bias = ( X[i + j*m] - lam ) / (double)k;
			for (l = 0; l < k; ++l)
				Y[i + l*m + j*m*k] = lams[l] + bias;
		}
}

SEXP
prism_cond_l2_R(SEXP X_R, SEXP Z_R, SEXP W_R, SEXP G_R)
{
	int m, n, mZ, k, nZ, kW, nW, nG;
	SEXP Y_R;
	double *scratch;

	/* Check inputs */
	if (!(isReal(X_R) && ndims(X_R) <= 2))
		error("'%s' must be a numeric matrix", "X");
	if (!(isReal(X_R) && ndims(Z_R) <= 3))
		error("'%s' must be a numeric array", "Z");
	if (!(isReal(W_R) && ndims(W_R) <= 2))
		error("'%s' must be a numeric matrix", "W");
	if (!(isReal(G_R) && ndims(G_R) <= 2))
		error("'%s' must be a numeric matrix", "G");

	/* Get dimensions */
	m = nrows(X_R);
	n = ncols(X_R);
	mZ = nrows(Z_R);
	k = ncols(Z_R);
	nZ = n3rdDim(Z_R);
	kW = nrows(W_R);
	nW = ncols(W_R);
	nG = length(G_R);

	/* Check dimensions */
	if (!((mZ == m) && (nZ == n || nZ == 1)))
		error("'%s' must be a %d-by-%d-by-%d array", "Z", m, k, n);
	if (!((kW == k || kW == 1) && (nW == n || nW == 1)))
		error("'%s' must be a %d-by-%d matrix", "W", k, n);
	if (!((nG == n || nG == 1) && (nrows(G_R) == nG || ncols(G_R) == nG)))
		error("'%s' must be a %d-vector", "G", n);

	/* Create output */
	Y_R = PROTECT(alloc3DArray(REALSXP, m, k, n));
	scratch = (double *)R_alloc(sizeof(*scratch), k);

	/* Compute */
	prism_cond_l2(scratch, REAL(Y_R), REAL(X_R), REAL(Z_R), REAL(W_R), REAL(G_R),
		(size_t)m, (size_t)k, (size_t)n,
		(size_t)(nZ == n), (size_t)(kW == k), (size_t)(nW == n), (size_t)(nG == n));

	UNPROTECT(1);
	return Y_R;
}
