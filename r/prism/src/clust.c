
#include <Rinternals.h>
#include <libprism.h>
#include <string.h>

static
double
map_label(size_t label, size_t n)
{
	if (label < n)
		return -(double)(label + 1);
	else
		return (double)(label-n + 1);
}

static
void
convert_labels(double *Z, size_t n1, size_t n)
{
	size_t k;

	for (k = 0; k < n1; ++k)
	{
		Z[k + 0*n1] = map_label((size_t)Z[k + 0*n1], n);
		Z[k + 1*n1] = map_label((size_t)Z[k + 1*n1], n);
	}
}

SEXP
prism_clust_poi_R(SEXP X_R, SEXP G_R)
{
	int m, n, n1;
	SEXP Z_R;
	double *scratch;

	/* Check arguments */
	if (!(isReal(X_R)))
		error("'%s' must be a numeric matrix", "X");
	if (!(isReal(G_R) && length(G_R) == ncols(X_R)))
		error("'%s' must be a compatible numeric vector", "G");

	/* Get dimensions */
	m = nrows(X_R);
	n = ncols(X_R);
	n1 = n > 0 ? n - 1 : 0;

	/* Create ouput */
	Z_R = PROTECT(allocMatrix(REALSXP, n1, 3));
	/* Allocate scratch */
	scratch = (double *)R_alloc(prism_clust_poi_scratch(m, n), sizeof(*scratch));

	/* Compute */
	prism_clust_poi(REAL(Z_R), scratch, REAL(X_R), REAL(G_R), (size_t)m, (size_t)n);
	/* Rewrite labels for R */
	convert_labels(REAL(Z_R), n1, n);

	UNPROTECT(1);
	return Z_R;
}

SEXP
prism_clust_spoi_R(SEXP X_R, SEXP G_R)
{
	int m, n, n1;
	SEXP Z_R;
	double *scratch;

	/* Check arguments */
	if (!(isReal(X_R)))
		error("'%s' must be a numeric matrix", "X");
	if (!(isReal(G_R) && length(G_R) == ncols(X_R)))
		error("'%s' must be a compatible numeric vector", "G");

	/* Get dimensions */
	m = nrows(X_R);
	n = ncols(X_R);
	n1 = n > 0 ? n - 1 : 0;

	/* Create ouput */
	Z_R = PROTECT(allocMatrix(REALSXP, n1, 3));
	/* Allocate scratch */
	scratch = (double *)R_alloc(prism_clust_spoi_scratch(m, n), sizeof(*scratch));

	/* Compute */
	prism_clust_spoi(REAL(Z_R), scratch, REAL(X_R), REAL(G_R), (size_t)m, (size_t)n);
	/* Rewrite labels for R */
	convert_labels(REAL(Z_R), n1, n);

	UNPROTECT(1);
	return Z_R;
}

SEXP
prism_clust_l2_R(SEXP X_R, SEXP G_R)
{
	int m, n, n1;
	SEXP Z_R;
	double *scratch;

	/* Check arguments */
	if (!(isReal(X_R)))
		error("'%s' must be a numeric matrix", "X");
	if (!(isReal(G_R) && length(G_R) == ncols(X_R)))
		error("'%s' must be a compatible numeric vector", "G");

	/* Get dimensions */
	m = nrows(X_R);
	n = ncols(X_R);
	n1 = n > 0 ? n - 1 : 0;

	/* Create ouput */
	Z_R = PROTECT(allocMatrix(REALSXP, n1, 3));
	/* Allocate scratch */
	scratch = (double *)R_alloc(prism_clust_l2_scratch(m, n), sizeof(*scratch));

	/* Compute */
	prism_clust_l2(REAL(Z_R), scratch, REAL(X_R), REAL(G_R), (size_t)m, (size_t)n);
	/* Rewrite labels for R */
	convert_labels(REAL(Z_R), n1, n);

	UNPROTECT(1);
	return Z_R;
}

static
int
check_label(double value, int n, int k)
{
	int label;

	/* Get label & check that it's an integer */
	label = (int)value;
	if (!(label == value))
		return 0;

	/* Check that it's a valid node, or a branch */
	if (label < 0)
	{
		if (!(1 <= -label && -label <= n))
			return 0;
	}
	else
		if (!(1 <= label && label <= k))  /* NB. k 0-based */
			return 0;

	return 1;
}

static
int
check_tree(const double *Z, int n1, int n)
{
	int k;

	/* Loop through */
	for (k = 0; k < n1; ++k)
	{
		if (!check_label(Z[k + 0*n1], n, k))
			return 0;
		if (!check_label(Z[k + 1*n1], n, k))
			return 0;
	}

	return 1;
}

SEXP
prism_clust_select_R(SEXP Z_R, SEXP m_R, SEXP n_R, SEXP complexity_R, SEXP method_R, SEXP pvalue_R)
{
	int m, n, n1;
	double complexity, pvalue, penalty;
	const char *method;
	SEXP k_R;

	/* Check arguments */
	if (!(isReal(Z_R)))
		error("'%s' must be a numeric matrix", "Z");
	if (!(isInteger(m_R) && length(m_R) == 1))
		error("'%s' must be an integer scalar", "m");
	if (!(isInteger(n_R) && length(n_R) == 1))
		error("'%s' must be an integer scalar", "n");
	if (!(isInteger(complexity_R) && length(complexity_R) == 1))
		error("'%s' must be an integer scalar", "complexity");
	if (!(isString(method_R) && length(method_R) == 1))
		error("'%s' must be a string", "method");
	if (!(isReal(pvalue_R) && length(pvalue_R) == 1))
		error("'%s' must be a numeric scalar", "p.value");

	/* Get dimensions */
	m = *INTEGER(m_R);
	n = *INTEGER(n_R);
	n1 = n > 0 ? n - 1 : 0;

	/* Check tree structure */
	if (!(nrows(Z_R) == n1 && ncols(Z_R) == 3))
		error("'%s' is of invalid dimension", "Z");
	if (!(check_tree(REAL(Z_R), n1, n)))
		error("'%s' has invalid data", "Z");

	/* Get parameters */
	complexity = (double)*INTEGER(complexity_R);
	method = CHAR(STRING_ELT(method_R, 0));
	pvalue = *REAL(pvalue_R);

	/* Check parameters */
	if      (strcmp(method, "bic") == 0)
		penalty = prism_clust_bic_penalty((size_t)(complexity * m), (size_t)n);
	else if (strcmp(method, "aic") == 0)
		penalty = prism_clust_aic_penalty((size_t)(complexity * m), (size_t)n);
	else if (strcmp(method, "lr") == 0)
		penalty = prism_clust_lr_penalty((size_t)(complexity * m), (size_t)n, pvalue);
	else
		error("'%s' must be 'bic', 'aic', or 'lr'", "method");

	/* Create output */
	k_R = PROTECT(allocVector(INTSXP, 1));

	/* Select model */
	*INTEGER(k_R) = (int)prism_clust_select(REAL(Z_R), (size_t)m, (size_t)n, complexity * penalty);

	UNPROTECT(1);
	return k_R;
}
