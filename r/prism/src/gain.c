
#include <Rinternals.h>
#include <libprism.h>

SEXP
prism_gain_poi_R(SEXP X_R)
{
	int m, n;
	SEXP u_R, v_R, out_R;

	/* Check arguments */
	if (!(isReal(X_R)))
		error("'%s' must be a numeric matrix", "X");

	/* Get dimensions */
	m = nrows(X_R);
	n = ncols(X_R);

	/* Create outputs */
	u_R = PROTECT(allocVector(REALSXP, m));
	v_R = PROTECT(allocVector(REALSXP, n));

	/* Compute */
	prism_gain_poi(REAL(u_R), REAL(v_R), REAL(X_R), (size_t)m, (size_t)n);

	/* Assemble outputs */
	out_R = PROTECT(allocList(2));
	SETCAR(out_R, u_R);
	SET_TAG(out_R, install("u"));
	SETCAR(CDR(out_R), v_R);
	SET_TAG(CDR(out_R), install("v"));

	UNPROTECT(3);
	return out_R;
}

SEXP
prism_gain_spoi_R(SEXP X_R)
{
	int m, n;
	SEXP u_R, v_R, out_R;

	/* Check arguments */
	if (!(isReal(X_R)))
		error("'%s' must be a numeric matrix", "X");

	/* Get dimensions */
	m = nrows(X_R);
	n = ncols(X_R);

	/* Create outputs */
	u_R = PROTECT(allocVector(REALSXP, m));
	v_R = PROTECT(allocVector(REALSXP, n));

	/* Compute */
	prism_gain_spoi(REAL(u_R), REAL(v_R), REAL(X_R), (size_t)m, (size_t)n);

	/* Assemble outputs */
	out_R = PROTECT(allocList(2));
	SETCAR(out_R, u_R);
	SET_TAG(out_R, install("u"));
	SETCAR(CDR(out_R), v_R);
	SET_TAG(CDR(out_R), install("v"));

	UNPROTECT(3);
	return out_R;
}

SEXP
prism_gain_l2_R(SEXP X_R)
{
	int m, n;
	SEXP u_R, v_R, out_R;

	/* Check arguments */
	if (!(isReal(X_R)))
		error("'%s' must be a numeric matrix", "X");

	/* Get dimensions */
	m = nrows(X_R);
	n = ncols(X_R);

	/* Create outputs */
	u_R = PROTECT(allocVector(REALSXP, m));
	v_R = PROTECT(allocVector(REALSXP, n));

	/* Compute */
	prism_gain_l2(REAL(u_R), REAL(v_R), REAL(X_R), (size_t)m, (size_t)n);

	/* Assemble outputs */
	out_R = PROTECT(allocList(2));
	SETCAR(out_R, u_R);
	SET_TAG(out_R, install("u"));
	SETCAR(CDR(out_R), v_R);
	SET_TAG(CDR(out_R), install("v"));

	UNPROTECT(3);
	return out_R;
}

SEXP
prism_gain_trimmed_poi_R(SEXP X_R, SEXP alpha_R)
{
	int m, n;
	SEXP u_R, v_R, out_R;

	/* Check arguments */
	if (!(isReal(X_R)))
		error("'%s' must be a numeric matrix", "X");
	if (!(isReal(alpha_R) && length(alpha_R) == 1))
		error("'%s' must be a numeric scalar", "alpha"); 

	/* Get dimensions */
	m = nrows(X_R);
	n = ncols(X_R);

	/* Create outputs */
	u_R = PROTECT(allocVector(REALSXP, m));
	v_R = PROTECT(allocVector(REALSXP, n));

	/* Compute */
	prism_gain_trimmed_poi(REAL(u_R), REAL(v_R), REAL(X_R), (size_t)m, (size_t)n, *REAL(alpha_R));

	/* Assemble outputs */
	out_R = PROTECT(allocList(2));
	SETCAR(out_R, u_R);
	SET_TAG(out_R, install("u"));
	SETCAR(CDR(out_R), v_R);
	SET_TAG(CDR(out_R), install("v"));

	UNPROTECT(3);
	return out_R;
}

SEXP
prism_gain_trimmed_spoi_R(SEXP X_R, SEXP alpha_R)
{
	int m, n;
	SEXP u_R, v_R, out_R;

	/* Check arguments */
	if (!(isReal(X_R)))
		error("'%s' must be a numeric matrix", "X");
	if (!(isReal(alpha_R) && length(alpha_R) == 1))
		error("'%s' must be a numeric scalar", "alpha"); 

	/* Get dimensions */
	m = nrows(X_R);
	n = ncols(X_R);

	/* Create outputs */
	u_R = PROTECT(allocVector(REALSXP, m));
	v_R = PROTECT(allocVector(REALSXP, n));

	/* Compute */
	prism_gain_trimmed_spoi(REAL(u_R), REAL(v_R), REAL(X_R), (size_t)m, (size_t)n, *REAL(alpha_R));

	/* Assemble outputs */
	out_R = PROTECT(allocList(2));
	SETCAR(out_R, u_R);
	SET_TAG(out_R, install("u"));
	SETCAR(CDR(out_R), v_R);
	SET_TAG(CDR(out_R), install("v"));

	UNPROTECT(3);
	return out_R;
}

SEXP
prism_gain_trimmed_l2_R(SEXP X_R, SEXP alpha_R)
{
	int m, n;
	SEXP u_R, v_R, out_R;

	/* Check arguments */
	if (!(isReal(X_R)))
		error("'%s' must be a numeric matrix", "X");
	if (!(isReal(alpha_R) && length(alpha_R) == 1))
		error("'%s' must be a numeric scalar", "alpha"); 

	/* Get dimensions */
	m = nrows(X_R);
	n = ncols(X_R);

	/* Create outputs */
	u_R = PROTECT(allocVector(REALSXP, m));
	v_R = PROTECT(allocVector(REALSXP, n));

	/* Compute */
	prism_gain_trimmed_l2(REAL(u_R), REAL(v_R), REAL(X_R), (size_t)m, (size_t)n, *REAL(alpha_R));

	/* Assemble outputs */
	out_R = PROTECT(allocList(2));
	SETCAR(out_R, u_R);
	SET_TAG(out_R, install("u"));
	SETCAR(CDR(out_R), v_R);
	SET_TAG(CDR(out_R), install("v"));

	UNPROTECT(3);
	return out_R;
}
