
#include <Rinternals.h>
#include <libprism.h>

SEXP
prism_decom_poi_R(SEXP t_X_R, SEXP t_Y_R, SEXP WY_R, SEXP max_iters_R, SEXP min_delta_R)
{
	int m, r, n, k;
	SEXP t_Z_R, WX_R, out_R;

	/* Check arguments */
	if (!(isReal(t_X_R)))
		error("'%s' must be a numeric matrix", "X");
	if (!(isReal(t_Y_R) && ncols(t_Y_R) == ncols(t_X_R)))
		error("'%s' must be a compatible numeric vector", "Y");
	if (!(isReal(WY_R) && ncols(WY_R) == nrows(t_Y_R)))
		error("'%s' must be a compatible numeric vector", "W.Y");
	if (!(isInteger(max_iters_R) && length(max_iters_R) == 1))
		error("'%s' must be an integer scalar", "max.iters");
	if (!(isReal(min_delta_R) && length(min_delta_R) == 1))
		error("'%s' must be an integer scalar", "min.delta");

	/* Get dimensions */
	m = ncols(t_X_R);
	r = nrows(t_X_R);
	n = nrows(t_Y_R);
	k = nrows(WY_R);

	/* Create ouput */
	t_Z_R = PROTECT(allocMatrix(REALSXP, k, m));
	WX_R = PROTECT(allocMatrix(REALSXP, k, r));

	/* Compute */
	prism_decom_poi(REAL(t_Z_R), REAL(WX_R), REAL(t_X_R), REAL(t_Y_R), REAL(WY_R), (size_t)m, (size_t)k, (size_t)r, (size_t)n, (size_t)*INTEGER(max_iters_R), *REAL(min_delta_R));

	/* Assemble outputs */
	out_R = PROTECT(allocList(2));
	SETCAR(out_R, t_Z_R);
	SET_TAG(out_R, install("t_Z"));
	SETCAR(CDR(out_R), WX_R);
	SET_TAG(CDR(out_R), install("W.X"));

	UNPROTECT(3);
	return out_R;
}

SEXP
prism_decom_spoi_R(SEXP t_X_R, SEXP t_Y_R, SEXP WY_R, SEXP max_iters_R, SEXP min_delta_R)
{
	int m, r, n, k;
	SEXP t_Z_R, t_T_R, WX_R, out_R;

	/* Check arguments */
	if (!(isReal(t_X_R)))
		error("'%s' must be a numeric matrix", "X");
	if (!(isReal(t_Y_R) && ncols(t_Y_R) == ncols(t_X_R)))
		error("'%s' must be a compatible numeric vector", "Y");
	if (!(isReal(WY_R) && ncols(WY_R) == nrows(t_Y_R)))
		error("'%s' must be a compatible numeric vector", "W.Y");
	if (!(isInteger(max_iters_R) && length(max_iters_R) == 1))
		error("'%s' must be an integer scalar", "max.iters");
	if (!(isReal(min_delta_R) && length(min_delta_R) == 1))
		error("'%s' must be an integer scalar", "min.delta");

	/* Get dimensions */
	m = ncols(t_X_R);
	r = nrows(t_X_R);
	n = nrows(t_Y_R);
	k = nrows(WY_R);

	/* Create ouput */
	t_Z_R = PROTECT(allocMatrix(REALSXP, k, m));
	t_T_R = PROTECT(allocMatrix(REALSXP, k, m));
	WX_R = PROTECT(allocMatrix(REALSXP, k, r));

	/* Compute */
	prism_decom_spoi(REAL(t_Z_R), REAL(t_T_R), REAL(WX_R), REAL(t_X_R), REAL(t_Y_R), REAL(WY_R), (size_t)m, (size_t)k, (size_t)r, (size_t)n, (size_t)*INTEGER(max_iters_R), *REAL(min_delta_R));

	/* Assemble outputs */
	out_R = PROTECT(allocList(3));
	SETCAR(out_R, t_Z_R);
	SET_TAG(out_R, install("t_Z"));
	SETCAR(CDR(out_R), t_T_R);
	SET_TAG(CDR(out_R), install("t_T"));
	SETCAR(CDR(CDR(out_R)), WX_R);
	SET_TAG(CDR(CDR(out_R)), install("W.X"));

	UNPROTECT(4);
	return out_R;
}

SEXP
prism_decom_l2_R(SEXP t_X_R, SEXP t_Y_R, SEXP WY_R, SEXP max_iters_R, SEXP min_delta_R)
{
	int m, r, n, k;
	SEXP t_Z_R, WX_R, out_R;

	/* Check arguments */
	if (!(isReal(t_X_R)))
		error("'%s' must be a numeric matrix", "X");
	if (!(isReal(t_Y_R) && ncols(t_Y_R) == ncols(t_X_R)))
		error("'%s' must be a compatible numeric vector", "Y");
	if (!(isReal(WY_R) && ncols(WY_R) == nrows(t_Y_R)))
		error("'%s' must be a compatible numeric vector", "W.Y");
	if (!(isInteger(max_iters_R) && length(max_iters_R) == 1))
		error("'%s' must be an integer scalar", "max.iters");
	if (!(isReal(min_delta_R) && length(min_delta_R) == 1))
		error("'%s' must be an integer scalar", "min.delta");

	/* Get dimensions */
	m = ncols(t_X_R);
	r = nrows(t_X_R);
	n = nrows(t_Y_R);
	k = nrows(WY_R);

	/* Create ouput */
	t_Z_R = PROTECT(allocMatrix(REALSXP, k, m));
	WX_R = PROTECT(allocMatrix(REALSXP, k, r));

	/* Compute */
	prism_decom_l2(REAL(t_Z_R), REAL(WX_R), REAL(t_X_R), REAL(t_Y_R), REAL(WY_R), (size_t)m, (size_t)k, (size_t)r, (size_t)n, (size_t)*INTEGER(max_iters_R), *REAL(min_delta_R));

	/* Assemble outputs */
	out_R = PROTECT(allocList(2));
	SETCAR(out_R, t_Z_R);
	SET_TAG(out_R, install("t_Z"));
	SETCAR(CDR(out_R), WX_R);
	SET_TAG(CDR(out_R), install("W.X"));

	UNPROTECT(3);
	return out_R;
}
