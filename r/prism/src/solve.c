
#include <Rinternals.h>
#include <string.h>
#include <libprism.h>

SEXP
prism_solve_poi_R(SEXP t_A_R, SEXP B_R)
{
	int m, n, r, j;
	SEXP X_R;

	/* Check arguments */
	if (!(isReal(t_A_R)))
		error("'%s' must be a numeric matrix", "A");
	if (!(isReal(B_R) && nrows(B_R) == ncols(t_A_R)))
		error("'%s' must be a compatible numeric matrix", "B");

	/* Get dimensions */
	m = ncols(t_A_R);
	n = nrows(t_A_R);
	r = ncols(B_R);

	/* Create ouput */
	X_R = PROTECT(allocMatrix(REALSXP, n, r));

	/* Solve */
	for (j = 0; j < r; ++j)
		prism_solve_poi(&REAL(X_R)[j*n], REAL(t_A_R), &REAL(B_R)[j*m], m, n);

	UNPROTECT(1);
	return X_R;
}

SEXP
prism_solve_l2_R(SEXP t_A_R, SEXP B_R)
{
	int m, n, r, j;
	SEXP X_R;

	/* Check arguments */
	if (!(isReal(t_A_R)))
		error("'%s' must be a numeric matrix", "A");
	if (!(isReal(B_R) && nrows(B_R) == ncols(t_A_R)))
		error("'%s' must be a compatible numeric matrix", "B");

	/* Get dimensions */
	m = ncols(t_A_R);
	n = nrows(t_A_R);
	r = ncols(B_R);

	/* Create ouput */
	X_R = PROTECT(allocMatrix(REALSXP, n, r));

	/* Solve */
	for (j = 0; j < r; ++j)
		prism_solve_l2(&REAL(X_R)[j*n], REAL(t_A_R), &REAL(B_R)[j*m], m, n);

	UNPROTECT(1);
	return X_R;
}

SEXP
prism_solve_poi_upd_R(SEXP t_A_R, SEXP B_R, SEXP X_R)
{
	int m, n, r;
	SEXP Y_R;

	/* Check arguments */
	if (!(isReal(t_A_R)))
		error("'%s' must be a numeric matrix", "A");
	if (!(isReal(B_R) && nrows(B_R) == ncols(t_A_R)))
		error("'%s' must be a compatible numeric matrix", "B");
	if (!(isReal(X_R) && nrows(X_R) == nrows(t_A_R) && ncols(X_R) == ncols(B_R)))
		error("'%s' must be a compatible numeric matrix", "X");

	/* Get dimensions */
	m = ncols(t_A_R);
	n = nrows(t_A_R);
	r = ncols(B_R);

	/* Create ouput */
	Y_R = PROTECT(allocMatrix(REALSXP, n, r));

	/* Solve */
	memcpy(REAL(Y_R), REAL(X_R), n*r * sizeof(*REAL(Y_R)));
	prism_solve_poi_mat_upd(REAL(Y_R), REAL(t_A_R), REAL(B_R), m, n, r);

	UNPROTECT(1);
	return Y_R;
}

SEXP
prism_solve_l2_upd_R(SEXP t_A_R, SEXP B_R, SEXP X_R)
{
	int m, n, r;
	SEXP Y_R;

	/* Check arguments */
	if (!(isReal(t_A_R)))
		error("'%s' must be a numeric matrix", "A");
	if (!(isReal(B_R) && nrows(B_R) == ncols(t_A_R)))
		error("'%s' must be a compatible numeric matrix", "B");
	if (!(isReal(X_R) && nrows(X_R) == nrows(t_A_R) && ncols(X_R) == ncols(B_R)))
		error("'%s' must be a compatible numeric matrix", "X");

	/* Get dimensions */
	m = ncols(t_A_R);
	n = nrows(t_A_R);
	r = ncols(B_R);

	/* Create ouput */
	Y_R = PROTECT(allocMatrix(REALSXP, n, r));

	/* Solve */
	memcpy(REAL(Y_R), REAL(X_R), n*r * sizeof(*REAL(Y_R)));
	prism_solve_l2_mat_upd(REAL(Y_R), REAL(t_A_R), REAL(B_R), m, n, r);

	UNPROTECT(1);
	return Y_R;
}

SEXP
prism_solve_poi_con_R(SEXP t_A_R, SEXP B_R)
{
	int m, n, r;
	SEXP X_R;

	/* Check arguments */
	if (!(isReal(t_A_R)))
		error("'%s' must be a numeric matrix", "A");
	if (!(isReal(B_R) && nrows(B_R) == ncols(t_A_R)))
		error("'%s' must be a compatible numeric matrix", "B");

	/* Get dimensions */
	m = ncols(t_A_R);
	n = nrows(t_A_R);
	r = ncols(B_R);

	/* Create ouput */
	X_R = PROTECT(allocMatrix(REALSXP, n, r));

	/* Solve */
	prism_solve_poi_mat(REAL(X_R), REAL(t_A_R), REAL(B_R), m, n, r);
	prism_solve_poi_con_mat(REAL(X_R), REAL(t_A_R), m, n, r);

	UNPROTECT(1);
	return X_R;
}

SEXP
prism_solve_l2_con_R(SEXP t_A_R, SEXP B_R)
{
	int m, n, r;
	SEXP X_R;

	/* Check arguments */
	if (!(isReal(t_A_R)))
		error("'%s' must be a numeric matrix", "A");
	if (!(isReal(B_R) && nrows(B_R) == ncols(t_A_R)))
		error("'%s' must be a compatible numeric matrix", "B");

	/* Get dimensions */
	m = ncols(t_A_R);
	n = nrows(t_A_R);
	r = ncols(B_R);

	/* Create ouput */
	X_R = PROTECT(allocMatrix(REALSXP, n, r));

	/* Solve */
	prism_solve_l2_mat(REAL(X_R), REAL(t_A_R), REAL(B_R), m, n, r);
	prism_solve_l2_con_mat(REAL(X_R), REAL(t_A_R), m, n, r);

	UNPROTECT(1);
	return X_R;
}
