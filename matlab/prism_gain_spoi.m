
% PRISM_GAIN_SPOI Estimate gains using a scaled Poisson model.
%   [u, v]= PRISM_GAIN_SPOI(X)

function [u, v]= prism_gain_spoi(X)
	%%
	% get dimensions
	[m, n]= size(X);
	
	% run estimator
	[u, v, ~]= calllib(prism_load, 'prism_gain_spoi', ...
		nan(m, 1), nan(n, 1), X, m, n);
