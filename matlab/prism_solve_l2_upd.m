
% PRISM_SOLVE_L2_UPD Run a single step of normal model inversion.
%   x= PRISM_SOLVE_L2_UPD(A, b, x)

function x= prism_solve_l2_upd(A, b, x)
	%%
	% apply defaults
	if nargin < 3 || isempty(x)
		x= (1:size(A, 2)).' * ones(size(b, 2), 'like', A);
	end
	
	%%
	% get dimensions
	[m, n]= size(A);
	[~, r]= size(b);
	
	% check dimensions
	assert(isequal(size(b), [m, r]), ...
		'The input B must be %d-by-%d.', m, r);
	assert(isequal(size(x), [n, r]), ...
		'The input X must be %d-by-%d.', n, r);
	
	%%
	% solve
	[x, ~, ~]= calllib(prism_load, 'prism_solve_l2_mat_upd', x, A', b, m, n, r);
