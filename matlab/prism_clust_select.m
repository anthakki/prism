
% PRISM_CLUST_SELECT Select number of clusters.
%   k= PRISM_CLUST_SELECT(tree, m, n, penalty)

function k= prism_clust_select(Z, m, n, penalty, delta, p_value)
	%%
	% apply defaults
	if nargin < 3 || isempty(n)
		n= size(Z, 1) + 1;
	end
	if nargin < 4 || isempty(penalty)
		penalty= 'bic';
	end
	if nargin < 5 || isempty(delta)
		delta= 1;
	end
	if nargin < 6 || isempty(p_value)
		p_value= 0.05;
	end
	
	%%
	% translate penalty
	switch penalty
		case 'bic'
			penalty= calllib(prism_load, 'prism_clust_bic_penalty', m, n);
		case 'aic'
			penalty= calllib(prism_load, 'prism_clust_aic_penalty', m, n);
		case 'lr'
			penalty= calllib(prism_load, 'prism_clust_lr_penalty', m, n, p_value);
	end
	
	%%
	% select model order
	k= calllib(prism_load, 'prism_clust_select', Z, m, n, penalty * delta);
