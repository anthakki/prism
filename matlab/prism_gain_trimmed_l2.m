
% PRISM_GAIN_TRIMMED_L2 Estimate gains using a trimmed Normal model.
%   [u, v]= PRISM_GAIN_TRIMMED_L2(X, alpha)

function [u, v]= prism_gain_trimmed_l2(X, alpha)
	%%
	% apply defaults
	if nargin < 2 || isempty(alpha)
		alpha= 1.;
	end
	
	%%
	% get dimensions
	[m, n]= size(X);
	
	% run estimator
	[u, v, ~]= calllib(prism_load, 'prism_gain_trimmed_l2', ...
		nan(m, 1), nan(n, 1), X, m, n, alpha);
