
% PRISM_DECOM_SPOI Partially blind decomposition using a least-squares model.
%   [Z, WX, GX, T]= PRISM_DECOM_SPOI(X, Y, WY, GY)

function [Z, WX, GX, T]= prism_decom_spoi(X, Y, WY, GY, max_iters, min_delta)
	%%
	% apply defaults
	if nargin < 4 || isempty(GY)
		GY= ones(1, size(WY, 2));
	end
	if nargin < 5 || isempty(max_iters)
		max_iters= 1000;
	end
	if nargin < 6 || isempty(min_delta)
		min_delta= 1e-10;
	end
	
	%%
	% get dimensions
	[m, r]= size(X);
	[~, n]= size(Y);
	[k, ~]= size(WY);
	
	% check dimensions
	assert(isequal(size(Y), [m, n]), ...
		'The input Y must be %d-by-%d', m, n);
	assert(isequal(size(WY), [k, n]), ...
		'The input WY must be %d-by-%d', k, n);
	assert(isequal(size(GY), [1, n]), ...
		'The input GY must be %d-by-%d', 1, n);
	
	%%
	% decompose
	[Z, T, WX, ~, ~, ~]= calllib(prism_load, 'prism_decom_spoi', ...
		nan(k, m), nan(k, m), nan(k, r), X', Y', WY * diag(GY), m, k, r, n, ...
		max_iters, min_delta);
	Z= Z';   % Z is returned transposed
	T= T';
	
	%%
	% split weights
	if nargout >= 3
		GX= sum( WX, 1 );
		WX= WX ./ (( GX + (GX == 0)*1e-100 ));
	end
