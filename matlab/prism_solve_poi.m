
% PRISM_SOLVE_POI Invert a linear Poisson model.
%   x= PRISM_SOLVE_POI(A, b)

function x= prism_solve_poi(A, b)
	%%
	% get dimensions
	[m, n]= size(A);
	[~, r]= size(b);
	
	% check dimensions
	assert(isequal(size(b), [m, r]), ...
		'The input B must be %d-by-%d.', m, r);
	
	%%
	% solve
	x= nan(n, r);
	[x, ~, ~]= calllib(prism_load, 'prism_solve_poi_mat', x, A', b, m, n, r);
