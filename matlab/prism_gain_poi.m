
% PRISM_GAIN_POI Estimate gains using a Poisson model.
%   [u, v]= PRISM_GAIN_POI(X)

function [u, v]= prism_gain_poi(X)
	%%
	% get dimensions
	[m, n]= size(X);
	
	% run estimator
	[u, v, ~]= calllib(prism_load, 'prism_gain_poi', ...
		nan(m, 1), nan(n, 1), X, m, n);
