
% PRISM_GAIN_TRIMMED_SPOI Estimate gains using a trimmed scaled Poisson model.
%   [u, v]= PRISM_GAIN_TRIMMED_SPOI(X, alpha)

function [u, v]= prism_gain_trimmed_spoi(X, alpha)
	%%
	% apply defaults
	if nargin < 2 || isempty(alpha)
		alpha= 1.;
	end
	
	%%
	% get dimensions
	[m, n]= size(X);
	
	% run estimator
	[u, v, ~]= calllib(prism_load, 'prism_gain_trimmed_spoi', ...
		nan(m, 1), nan(n, 1), X, m, n, alpha);
