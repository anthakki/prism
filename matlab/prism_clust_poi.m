
% PRISM_CLUST_POI Hierarchical clustering using a Poisson model.
%   tree= PRISM_CLUST_POI(X)

function Z= prism_clust_poi(X, v)
	%%
	% apply defaults
	if nargin < 2 || isempty(v)
		v= [];
	end
	
	%%
	% get dimensions
	[m, n]= size(X);
	
	% generate default gains
	if isempty(v)
		v= ones(n, 1);
	end
	
	% compute intermediate dimensions
	n1= max( n-1, 0 );
	s= calllib(prism_load, 'prism_clust_poi_scratch', m, n);
	
	% run estimator
	[Z, ~, ~, ~]= calllib(prism_load, 'prism_clust_poi', ...
		nan(n1, 3), nan(s, 1), X, v, m, n);
	
	% convert indices to MATLAB
	Z(:, 1:2)= Z(:, 1:2) + 1;
