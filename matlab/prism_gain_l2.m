
% PRISM_GAIN_L2 Estimate gains using a least squares model.
%   [u, v]= PRISM_GAIN_L2(X)

function [u, v]= prism_gain_l2(X)
	%%
	% get dimensions
	[m, n]= size(X);
	
	% run estimator
	[u, v, ~]= calllib(prism_load, 'prism_gain_l2', ...
		nan(m, 1), nan(n, 1), X, m, n);
