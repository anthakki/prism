
% PRISM_SOLVE_L2_CON Apply unity constraint on a least-squares inverse.
%   x= PRISM_SOLVE_L2_CON(A, x)

function x= prism_solve_l2_con(A, x)
	%%
	% get dimensions
	[m, n]= size(A);
	[~, r]= size(x);
	
	% check dimensions
	assert(isequal(size(x), [n, r]), ...
		'The input X must be %d-by-%d.', n, r);
	
	%%
	% solve
	[x, ~]= calllib(prism_load, 'prism_solve_l2_con_mat', x, A', m, n, r);
