
% PRISM_SOLVE_POI_CON Apply unity constraint on a Poisson inverse.
%   x= PRISM_SOLVE_POI_CON(A, x)

function x= prism_solve_poi_con(A, x)
	%%
	% get dimensions
	[m, n]= size(A);
	[~, r]= size(x);
	
	% check dimensions
	assert(isequal(size(x), [n, r]), ...
		'The input X must be %d-by-%d.', n, r);
	
	%%
	% solve
	[x, ~]= calllib(prism_load, 'prism_solve_poi_con_mat', x, A', m, n, r);
