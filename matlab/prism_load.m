
% PRISM_LOAD Load the prism C routines into MATLAB.
%   soname= PRISM_LOAD
%   soname= PRISM_LOAD('-clear', path)

function soname= prism_load(path, varargin)
	%%
	% apply defaults
	if nargin < 1 || isempty(path)
		path= '';
	end
	
	%%
	% shuffle options
	args= {path, varargin{:}};
	
	% handle options	
	clear= false;
	if ischar(args{1}) && strcmpi(args{1}, '-clear')
		clear= true;
		args= args(2:end);
	end
	
	% pop arguments
	path= args{1};
	
	%%
	% get soname
	soname= regexprep(mfilename, '^([^_]+)(.*)$', 'lib$1');
	
	%%
	% clear?
	is_loaded= libisloaded(soname);
	if is_loaded && clear
		unloadlibrary(soname);
		is_loaded= false;
	end
	
	% load the library
	if ~is_loaded
		loadlibrary(fullfile(path, soname), ...
			fullfile(path, regexprep(soname, '(\.([^.]+))?$', '.h')));
	end
