
% PRISM_CLUST_SPOI Hierarchical clustering using a scaled Poisson model.
%   tree= PRISM_CLUST_SPOI(X)

function Z= prism_clust_spoi(X, v)
	%%
	% apply defaults
	if nargin < 2 || isempty(v)
		v= [];
	end
	
	%%
	% get dimensions
	[m, n]= size(X);
	
	% generate default gains
	if isempty(v)
		v= ones(n, 1);
	end
	
	% compute intermediate dimensions
	n1= max( n-1, 0 );
	s= calllib(prism_load, 'prism_clust_spoi_scratch', m, n);
	
	% run estimator
	[Z, ~, ~, ~]= calllib(prism_load, 'prism_clust_spoi', ...
		nan(n1, 3), nan(s, 1), X, v, m, n);
	
	% convert indices to MATLAB
	Z(:, 1:2)= Z(:, 1:2) + 1;
