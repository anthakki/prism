
% PRISM_COND_POI Condition counts for a Poisson model.
%   Y= PRISM_COND_POI(X, Z, W, G)

function Y= prism_cond_poi(X, Z, W, G)
	%%
	% apply defaults
	if nargin < 3 || isempty(W)
		W= 1;
	end
	if nargin < 4 || isempty(G)
		G= 1;
	end
	
	%%
	% get dimensions
	[m, n]= size(X);
	[~, k]= size(Z);
	
	% expand arrays
	W= W .* ones(k, n);
	G= G .* ones(1, n);
		
	% check dimensions
	assert(isequal(size(X), [m, n]), ...
		'The input X must be %d-by-%d', m, n);
	assert(size(Z, 1) == m && size(Z, 2) == k && (size(Z, 3) == 1 || size(Z, 3) == n), ...
		'The input Z must be %d-by-%d', m, k);
	assert(isequal(size(W), [k, n]), ...
		'The input W must be %d-by-%d', k, n);
	assert(isequal(size(G), [1, n]), ...
		'The input G must be %d-by-%d', 1, n);
	
	%%
	% condition
	lams= Z .* reshape( W, [1, k, n] ) .* reshape( G, [1, 1, n] );
	coefs= lams ./ sum( lams, 2 );
	coefs(~(lams > 0))= 0;
	Y= coefs .* reshape( X, [m, 1, n] );
