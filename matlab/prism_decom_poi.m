
% PRISM_DECOM_POI Partially blind decomposition using a Poisson model.
%   [Z, WX, GX]= PRISM_DECOM_POI(X, Y, WY, GY)

function [Z, WX, GX]= prism_decom_poi(X, Y, WY, GY, max_iters, min_delta)
	%%
	% apply defaults
	if nargin < 4 || isempty(GY)
		GY= ones(1, size(WY, 2));
	end
	if nargin < 5 || isempty(max_iters)
		max_iters= 1000;
	end
	if nargin < 6 || isempty(min_delta)
		min_delta= 1e-10;
	end
	
	%%
	% get dimensions
	[m, r]= size(X);
	[~, n]= size(Y);
	[k, ~]= size(WY);
	
	% check dimensions
	assert(isequal(size(Y), [m, n]), ...
		'The input Y must be %d-by-%d', m, n);
	assert(isequal(size(WY), [k, n]), ...
		'The input WY must be %d-by-%d', k, n);
	assert(isequal(size(GY), [1, n]), ...
		'The input GY must be %d-by-%d', 1, n);
	
	%%
	% decompose
	[Z, WX, ~, ~, ~]= calllib(prism_load, 'prism_decom_poi', ...
		nan(k, m), nan(k, r), X', Y', WY * diag(GY), m, k, r, n, ...
		max_iters, min_delta);
	Z= Z';   % Z is returned transposed
	
	%%
	% split weights
	if nargout >= 3
		GX= sum( WX, 1 );
		WX= WX ./ (( GX + (GX == 0)*1e-100 ));
	end
