% PRISM-GAIN(1) Version 0.9 | prism documentation

NAME 
====

**prism-gain** - Estimate scale factors for single-component RNA-seq data

SYNOPSIS
========

| **prism-gain** \[_options_\] _expr.tsv_ [**-G** _gains.tsv_]

DESCRIPTION
===========

The program takes a set of RNA-seq profiles and estimates batch factors both
on the genes and on the samples, by doing a rank-1 factorization of the
expression matrix. This is useful, as the samples may vary in total amount of
sequenced material, amplification factor etc., which need to be controlled for
in a differential expression analysis.

The expression matrix _expr.tsv_ is an _m_-by-_n_ matrix, where _m_ denotes the
number of genes (rows) and _n_ the number of samples (columns), and the sample
specific batch factors _gains.tsv_ is a 1-by-_n_ matrix. Generally, the values
must be counts (or scaled counts) for the results to be meaningful, but this
depends on the method. All files are assumed to be tab separated text.

Options:
--------

-m _model_

: Specifies the model to be used. Possible values are **poi** for Poisson,
**spoi** for scaled Poisson (default), and **l2** for normal (least-squares or
PCA).

-m sum

: The gain is the sum of all read counts. Equivalent to **-m poi -a 1**.

-m pca

: The gain is the major principal component coefficient. Equivalent to **-m l2
-a 1**.

-a _trim_

: A fraction of _trim_ genes per each sample are assumed to be unperturbed
(default: 1). These genes need not be the same in each sample and are
automatically discovered. This is useful if the data feature systematic
changes, such as being distinct cell types, as different cell types likely
have different global expression level. 

-n _unity_

: Specifies that the gains are normalized to average _unity_. By default the
factors are not normalized, and are whatever is natural to the underlying
method. However, absolute scale factors cannot be in practice estimated, so a
canonical scaling is often used. The common expression profile is scaled with
the inverse.

-H

: Specifies that the data has row and column headers. These are passed through ot the output files.

-M

: Specifies that zeros in the data are to be treated as missing data. This can be used to combat drop-out, especially with trimming.

-G _gains.tsv_

: Filename to output the sample gains.

SEE ALSO
========

**prism-clust**, **prism-decom**

AUTHOR
======

Antti Hakkinen.
