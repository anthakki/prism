% PRISM-CLUST(1) Version 0.9 | prism documentation

NAME 
====

**prism-clust** - Cluster single-component RNA-seq data

SYNOPSIS
========

| **prism-clust** \[_options_\] _expr.tsv_ [**-g** _gains.tsv_] [**-T** _tree.tsv_]

| **prism-clust** \[_options_\] **-t** _tree.tsv_ [**-W** _labels.tsv_]

DESCRIPTION
===========

The program takes a set of RNA-seq profiles and agglomeratively constructs a
hierarchical clustering tree, in order to cluster single-cell phenotypes. The
tree can be then cut either traditional or by statistical means in order to
obtain a clustering of the data into different phenotypes. 

Usually the clustering is done in two steps as illustrated in the synopsis:
first constructs the clustering tree _tree.tsv_ (which is computationally
expensive), and the second cuts the tree into a specified clustering, but
these can be done in a single invocation.

The expression matrix _expr.tsv_ is an _m_-by-_n_ matrix, where _m_ denotes the
number of genes (rows) and _n_ the number of samples (columns). Generally, the
values must be counts (or scaled counts) for the results to be meaningful, but
this depends on the method. The sample scaling factors should be provided using
the **-g** option using a 1-by-_n_ matrix, as estimated using **prism-gain** or
by alternative means.

The format of the clustering tree _tree.tsv_ is unspecified, generally of size
_O(n)_. The labeling is specified by the _n_-by-_k_ adjacency matrix
_labels.tsv_. All files are assumed to be tab separated text.

Options:
--------

-m _model_

: Specifies the model to be used. Possible values are **poi** for Poisson,
**spoi** for scaled Poisson (default), and **l2** for normal (least-squares).

-k _clusts_

: Cut the clustering tree into _clusts_ clusters. This overrides any setting by **-s** or **-p**.

-s bic, -s aic

: Cut the clustering tree using Bayesian (**bic**) or Akaike Information Criterion (**aic**).

-p _pvalue_

: Cut the clustering tree using a likelihood ratio test with a significance level of _pvalue_. 

-H

: Specifies that the data has row and column headers.

-g _gains.tsv_

: Specifies the single-cell gains (default: all ones).

-t _tree.tsv_

: Specifies a previously constructed tree for cutting.

-T _tree.tsv_

: Filename to output the clustering tree.

-W _labels.tsv_

: Filename to output the labeling.

SEE ALSO
========

**prism-gain**, **prism-decom**

AUTHOR
======

Antti Hakkinen.
