% PRISM-DECOM(1) Version 0.9 | prism documentation

NAME 
====

**prism-decom** - Decompose mixture RNA-seq data using a soft single-cell reference

SYNOPSIS
========

| **prism-decom** \[_options_\] _bulk_expr.tsv_ _sc_expr.tsv_ [-g _sc_gains.tsv_] -w _sc_weights.tsv_

DESCRIPTION
===========

The program takes a set of mixture (e.g. bulk) RNA-seq profiles and decomposes
each using a single-cell population reference. Both the mixture profile and the
single-cell reference are treated as data and are subject to the statistical
model, which allows the estimated profiles to adapt to unmatched data.

The bulk expression matrix _bulk_expr.tsv_ is an _m_-by-_r_ matrix, where _m_
denotes the number of genes (rows) and _n_ the number of bulk samples (columns)
and single-cell expression matrix _sc_expr.tsv_ is an _m_-by-_n_ matrix, where
_n_ denotes the number of single-cell samples. The rows must be matching and
regardless if the labels are provided this is not checked.  Generally, the
values must be counts (or scaled counts) for the results to be meaningful, but
this depends on the method. The bulk RNA samples are assumed to be independent
but settings with replicates can be implemented using the R or MATLAB
interfaces.

The sample specific batch factors for the single cell data should be provided
in a 1-by-_n_ matrix using the **-g** option. These gains be estimated using
**prism-gain**. Further, the labeling for the single-cell samples must be
provided in a _n_-by-_k_ adjacency matrix using the **-w** option. The labels
can be estimated using **prism-clust**. All files are assumed to be tab
separated text.

The decomposition reveals the _m_-by-_k_*_r_ sample spefic cell type specific
expression profiles _decom_expr.tsv_, with the _k_ cell types of a single bulk
in adjacent columns, the 1-by-_r_ bulk sample scaling factors _bulk_gains.tsv_,
and the _r_-by-_k_ convex bulk composition factors _bulk_weights.tsv_.

Options:
--------

-m _model_

: Specifies the model to be used. Possible values are **poi** for Poisson,
**spoi** for scaled Poisson (default), and **l2** for normal (least-squares).

-b _bulk_bias_

: Bulk weight bias (default: .5). A value near 0. or 1. weighs in the single
cell reference or the bulk data respectively, .5 corresponding to natural
weighting. This can be useful if the read depths vary drastically, and the
model is overfit to either dataset.

-b auto

: As above, but a value is automatically selected to weight the data equally.
This is not natural weighting if the read depths differ.

-H

: Specifies that the data has row and column headers.

-g _sc_gains.tsv_

: Specifies the single-cell gains (default: all ones).

-w _sc_weights.tsv_

: Specifies the single-cell labels.

-Z _decom_bulk_expr.tsv_

: Filename to output the decomposed bulk data.

-G _bulk_gains.tsv_

: Filename to output the bulk gains.

-W _bulk_weights.tsv_

: Filename to output the bulk compositions.

-k _max_iters_

: Maximum number of EM iterations (default: 1,000)

-t _min_delta_

: Minimum objective tolerance (default: 1e-10)

-d _density_

: Density to switch from sparse to dense matrix routines (default: 0.5)

SEE ALSO
========

**prism-gain**, **prism-clust**

AUTHOR
======

Antti Hakkinen.
